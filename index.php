<?php
ob_start();
/* loading config files */
require_once 'System/Engine/Config/config.php';

/* loading routing collection */
$loader = include 'vendor/autoload.php';
$collection = new System\Engine\Router\Collection();
require_once 'System/Engine/Config/routing_collection.php';

/* set url from collection */
$url = new System\Engine\Router\Router($collection, PROTOCOL . $_SERVER["SERVER_NAME"] . (PORT ? ':' . $_SERVER["SERVER_PORT"] : '') . $_SERVER["REQUEST_URI"]);

/* load page */
try {
    if (!$route_action = $url->exist()) {
        $url = new System\Engine\Router\Router($collection, PROTOCOL . $_SERVER["SERVER_NAME"] . (PORT ? ':' . $_SERVER["SERVER_PORT"] : '') . '/' . URL_ERROR_404);
        $route_action = $url->exist();
    }
    $action = new $route_action->namespace;
    $data = $route_action->data ? $route_action->data : NULL;
    $action->{$route_action->method}($data);
} catch (Exception $exc) {
    echo ERROR;
}

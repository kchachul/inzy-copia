<?php

namespace System\Engine\Router;

class Router
{
    private $url;
    private $collection;

    public function __construct(Collection $collection, $url)
    {
        $this->collection = $collection;
        $url = explode('?', $url);
        $url = explode(HTTP_SERVER, $url[0]);
        $this->url = $url[1];
    }

    public function exist()
    {
        if ($route = $this->collection->get($this->url)) {
            return $route;
        } else {
            return null;
        }
    }

}

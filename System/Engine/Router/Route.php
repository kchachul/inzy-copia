<?php

namespace System\Engine\Router;

class Route
{
    public $class;
    public $method;
    public $namespace;
    public $data;

    public function __construct($class, $method, $namespace, $data = NULL)
    {
        $this->class = $class;
        $this->method = $method;
        $this->namespace = $namespace;
        if ($data) {
            $this->data = $data;
        }
    }
}

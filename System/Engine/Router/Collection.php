<?php

namespace System\Engine\Router;

class Collection {

    protected $items;

    /**
     * Add object rout to collection
     * @param string $name
     * @param Route $item
     */
    public function add($name, $item) {
        $this->items[$name] = $item;
    }

    /**
     * get collection object
     *
     * @param $name
     * @return null
     */
    public function get($name) {
        if(array_key_exists($name, $this->items)) {
            return $this->items[$name];
        } else {
            return null;
        }
    }

    /**
     * get all objects collection
     * @return array array
     */
    public function getAll() {
        return $this->items;
    }

}

<?php

namespace System\Engine\Helpers;
//use bb\Sha3\Sha3;

abstract class MainHelper extends Validator
{
    /**
     * Mapping
     *
     * @param $unmapedData
     * @return array|null
     */
    public function mapFields($unmapedData, $fields)
    {

        if (isset($unmapedData)) {
            $mapedData = array();

            foreach ($unmapedData as $test) {
                $mapedTest = array();

                foreach ($test as $key => $value) {
                    $mapedTest[$fields[$key]['name']] = $value;
                }

                $mapedData[] = $mapedTest;
            }

            return $mapedData;
        } else {
            return null;
        }
    }


    public function unmapFields($mapedData, $fields, $searchBy)
    {
        if (isset($mapedData)) {
            $unmapedData = array();

            foreach ($mapedData as $key => $value) {
                $mainKey = array_search($key, array_column($fields, $searchBy));
                $unmapedData[array_keys($fields)[$mainKey]] = $value;
            }

            return $unmapedData;
        } else {
            return null;
        }
    }

    protected function generateHash($length)
    {
        $char = "abcdefghijklmnopqrstuvwxyz0123456789";
        $char = str_shuffle($char);
        for ($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i++) {
            $rand .= $char{mt_rand(0, $l)};
        }
        return $rand;
    }


    /**
     * hash js in php
     *
     * @param $text
     * @return string
     */
    protected function sameJsKeyStretching($text)
    {
        $iterations = 25;
        $hash = $text;

        for ($i = 0; $i < $iterations; $i++) {
            $hash = hash('sha3-512', $hash);
        }

        return $hash;
    }


    protected function generateUrl($name)
    {
        return HTTP_SERVER . $name;
    }

    protected function checkData($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


    /**
     * @param $message
     * @param $title
     * @param $email
     * @return bool
     */
    protected function sendEmail($message, $title, $email)
    {

        $header = "Reply-to: " . ADDRESS_FROM . " <" . ADDRESS_FROM . ">" . PHP_EOL;
        $header .= "From: " . ADDRESS_FROM . " <" . ADDRESS_FROM . ">" . PHP_EOL;
        $header .= "MIME-Version: 1.0" . PHP_EOL;
        $header .= "Content-type: text/html; charset=utf-8" . PHP_EOL;

        try {
            mail($email, $title, $message, $header);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    protected function generateUserHash($userName)
    {
        return md5($_SERVER['REMOTE_ADDR'] + preg_replace('/\s+/', '', $userName));
    }

    protected function getAddrIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    protected function parseToJson($data)
    {
        return json_encode($data);
    }

    /**
     * get default JSON callback
     * @return array
     */
    protected function defaultJSONCallback()
    {
        return array('status' => false, 'data' => null);
    }


    protected function getGradge($percents)
    {
        if ($percents >= TEST_GRADGE_PERCENT_5) {
            return 'bdb';
        } else if ($percents >= TEST_GRADGE_PERCENT_4) {
            return 'db';
        } else if ($percents >= TEST_GRADGE_PERCENT_3) {
            return 'dst';
        } else if ($percents >= TEST_GRADGE_PERCENT_2) {
            return 'dop';
        } else {
            return 'ndst';
        }
    }


    /**
     * shuffle array
     * @param $my_array
     * @return mixed
     */
    public function shuffleAssoc($my_array)
    {
        $keys = array_keys($my_array);
        shuffle($keys);

        foreach ($keys as $key) {
            $new[] = $my_array[$key];
        }

        $my_array = $new;
        return $my_array;
    }
}
<?php

namespace System\Engine\Helpers;

abstract class Validator
{
    /**
     * clear text
     * @param $data
     * @return string
     */
    protected function clearText($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    /**
     * @param $data
     * @return bool
     */
    protected function allIsset($data)
    {
        $status = true;

        if (!empty($data)) {

            foreach ($data as $param) {
                if (!isset($_POST[$param])) {
                    $status = false;
                    break;
                }
            }

            return $status;
        } else {
            return false;
        }
    }

    /**
     * username
     * @param $username
     * @return false|int
     */
    protected function validUsername($username)
    {
        return preg_match('/^[a-zA-Z0-9]{3,25}$/', $username);
    }

    /**
     * email
     * @param $email
     * @return mixed
     */
    protected function validEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }


    protected function validPassword($password)
    {
        return preg_match('/^[a-zA-Z0-9]{128}$/', $password);

    }

    /**
     * lesson title - question collection
     * @param $title
     * @return false|int
     */
    protected function LessonTitle($title){
        return preg_match('/^[\s\p{L}0-9 ]{3,50}$/u', $title); // regexp js
    }

    /**
     * lesson description - questions collection
     * @param $description
     * @return false|int
     */
    protected function LessonDescription($description){
        return preg_match('/^[\s\p{L}0-9 ]{3,200}$/u', $description); // regexp js
    }


    /**
     * test title - test creator
     * @param $title
     * @return false|int
     */
    protected function testShortTitle($title){
        return preg_match('/^[\s\p{L}0-9 ]{3,100}$/u', $title); // regexp js
    }


    /**
     * test description - test creator
     * @param $description
     * @return false|int
     */
    protected function testShortDescription($description){
        return preg_match('/^[\s\p{L}0-9 ]{3,200}$/u', $description); // regexp js
    }


    /**
     * test id
     * @param $testId
     * @return int
     */
    protected function testId($testId){
        return intval($this->clearText($testId));
    }



    /**
     * test id
     * @param $studentId
     * @return int
     */
    protected function studentId($studentId){
        return intval($this->clearText($studentId));
    }


    //  zmienic funkcje na validNAZWA

    /**
     * question id
     * @param $questionId
     * @return int
     */
    protected function questionId($questionId){
        return intval($this->clearText($questionId));
    }


    /**
     * answer id
     * @param $answerId
     * @return int
     */
    protected function answerId($answerId){
        return intval($this->clearText($answerId));
    }

    /**
     * lesson id
     * @param $lessonId
     * @return int
     */
    protected function lessonId($lessonId){
        return intval($this->clearText($lessonId));
    }


}
<?php

namespace System\Engine;

abstract class Controller extends Helpers\MainHelper
{
    protected $model;
    protected $view;

    public function __construct()
    {
        $className = get_class($this);
        $this->model = str_replace("Controller", "Model", $className);
        $this->view = str_replace("Controller", "View", $className);
    }

    public function redirect($url)
    {
        header("location: " . $url);
    }

}

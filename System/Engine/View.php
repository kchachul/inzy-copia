<?php

namespace System\Engine;

abstract class View
{
    protected function renderHTML($name, $path = '', $isHtml = NULL)
    {
        $path = DIR_TEMPLATE . $path . $name . '.html.php';
        try {
            if (is_file($path)) {
                if (!$isHtml) {
                    require $path;
                } else {
                    return file_get_contents($path);
                }
            } else {
                throw new \Exception('Can not open template ' . $name . ' in: ' . $path);
            }
        } catch (\Exception $e) {
            echo $e->getMessage() . '<br />
                File: ' . $e->getFile() . '<br />
                Code line: ' . $e->getLine() . '<br />
                Trace: ' . $e->getTraceAsString();
            exit;
        }
    }

    /**
     * JSON
     * @param $data
     */
    public function renderJSON($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    /**
     * pdf
     * @param $data
     */
    public function renderPdf($data)
    {
        echo $data;
        exit;
    }

    /**
     * head
     */
    protected function getHead()
    {
        $this->renderHTML('head', 'components/');
    }

    /**
     * header
     */
    protected function getHeader()
    {
        $this->renderHTML('header', 'components/');
    }

    /**
     * footer
     */
    protected function getFooter()
    {
        $this->renderHTML('footer', 'components/');
    }

}

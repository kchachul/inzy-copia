<?php
require_once 'emails_temp.php';
require_once 'config_debug.php';

// ******************************* //
//                                 //
//  MAIN CONFIG                    //
//                                 //
// ******************************* //

/* server settings */
define('PROTOCOL', !IS_DEBUG ? 'https://' : DEBUG_PROTOCOL);
define('PORT', !IS_DEBUG ? '' : DEBUG_PORT);
define('HTTP_URL', PROTOCOL . (!IS_DEBUG ? 'moja-ojczyzna.pl' : DEBUG_URL) . PORT . '/');
define('HTTP_SERVER', HTTP_URL);

/* template dir */
define('DIR_TEMPLATE', 'template/default/');
define('DIR_LIBS', 'libs/');
define('DIR_TEMPLATE_IMAGES', HTTP_SERVER . DIR_TEMPLATE . 'assets/images/pages/');

/* active page - slug */
define('ACTIVE_PAGE', end(explode('/', $_SERVER['REQUEST_URI'])));

/* email - system info */
define('ADDRESS_FROM', "kontakt@moja-ojczyzna.pl");

/* cookie and session */
define('COOKIE_NAME', 'system_cookie');
define('SESSION_NAME', 'system_session');
define('SESSION_ID', 'user_id');
define('SESSION_NAME_IP', 'system_session_ip');
define('SESSION_TIME_LIFE', time() + 3600);

/* social media urls */
define('SOCIAL_MEDIA_FACEBOOK', 'https://www.facebook.com/Moja-Ojczyznapl-888484684833234 ');
define('SOCIAL_MEDIA_INSTAGRAM', 'https://www.instagram.com/moja_ojczyzna_pl/');

/* default title */
define('WEBSITE_TITLE', 'Moja ojczyzna. Historia Polski dla najmłodszych.');

/* szablon bledu zamienic na obrazek */
define('ERROR', '<div style="text-align:center;padding-top:100px;color:#336699;text-transform: uppercase;"><h1>ERROR</h1><h2>Przepraszamy, nastąpił nieoczekiwany błąd aplikacji!</h2></div>');


// ******************************* //
//                                 //
//  DATABASE CONFIG                //
//                                 //
// ******************************* //

/* database connection */
define('DATABASE_NAME', 'XXXX');
define('DATABASE_USER', 'YYYY');
define('DATABASE_HOST', !IS_DEBUG ? 'localhost' : 'XXXX');
define('DATABASE_PASSWORD', 'XXXXX');

/* database tables name */
define('DATABASE_TAB_LESSON', 'lesson');
define('DATABASE_TAB_SCENE', 'scene');
define('DATABASE_TAB_ANSWER', 'answer');
define('DATABASE_TAB_ANSWERED', 'answered');
define('DATABASE_TAB_ARCHIVAL', 'archival');
define('DATABASE_TAB_LOG', 'log');
define('DATABASE_TAB_QUESTION', 'question');
define('DATABASE_TAB_REQUEST', 'request');
define('DATABASE_TAB_STUDENT', 'student');
define('DATABASE_TAB_TEST', 'test');
define('DATABASE_TAB_USER', 'user');


// ******************************* //
//                                 //
//  PAGES CONFIG                   //
//                                 //
// ******************************* //

/* menu - page urls */
define('URL_HOMEPAGE', '');
define('URL_ACTIVE_TESTS', 'panel');
define('URL_TEST_CREATOR', 'test_creator');
define('URL_ARCHIVAL_TESTS', 'archival_tests');
define('URL_QUESTIONS_COLLECTION', 'questions_collection');
define('URL_USER_PANEL', 'user_panel');
define('URL_TEST', 'test');
define('URL_TEST_PDF', 'pdf');
define('URL_LESSONS', 'lekcje');
define('URL_LESSON', 'lekcja');
define('URL_ERROR_404', 'error404');

/* menu - page labels */
define('MENU_TITLE_PANEL', 'aktywne testy');
define('MENU_TITLE_TEST_CREATOR', 'utwórz test');
define('MENU_TITLE_ARCHIVAL_TESTS', 'archiwum');
define('MENU_TITLE_QUESTIONS_COLLECTION', 'baza pytań');
define('MENU_TITLE_USER_PANEL', 'panel użykownika');

/* pages titles */
define("HOMEPAGE_TITLE_HEADER", "Odkrywaj karty historii");
define("TEST_TITLE_HEADER", "Sprawdzaj swoje umiejętnosci");
define("LESSONS_TITLE_HEADER", "Nauka");
define("ACTIVE_TESTS_TITLE_HEADER", "Twoje aktywne testy");
define("ARCHIVAL_TESTS_TITLE_HEADER", "Twoje archiwalne testy");
define("TEST_CREATOR_TITLE_HEADER", "Tworzenie testu");
define("QUEST_COLLECTIONS_TITLE_HEADER", "Lista dostępnych pytań");
define("USER_SETTINGS_TITLE_HEADER", "Ustawienia użytkownika");

/* footer */
define("FOOTER_COPYRIGHT_TEXT1", "ⓒ ".date('Y')." -  moja-ojczyzna.pl");
define("FOOTER_COPYRIGHT_TEXT2", "<strong>Projekt wykonany w ramach obrony pracy inżynierskiej:</strong><br> Jakub Chachuła, Marcin Tillak");


// *******************************
//
//  SYSTEM CONFIG
//
// *******************************


// -------------------------------
//  ROUTING
// -------------------------------
define('ACT_LOGIN', 'login');
define('ACT_LOGOUT', 'logout');
define('ACT_REGISTRATION', 'registration');
define('ACT_ADD_LESSON', 'add_lesson');
define('ACT_LOST_PASSWORD', 'lost_password');
define('ACT_RMV_LESSON', 'remove_lesson');
define('ACT_ADD_QUESTION', 'add_question');
define('ACT_RMV_QUESTION', 'remove_question');
define('ACT_ADD_TEST', 'add_test');
define('ACT_GET_ALL_LESSONS', 'get_all_lessons');
define('ACT_GET_ACTIVE_TEST', 'get_active_tests');
define('ACT_REMOVE_ACTIVE_TEST', 'remove_test');
define('ACT_START_TEST', 'start_test');
define('ACT_STOP_TEST', 'stop_test');
define('ACT_UPDATE_TEST', 'update_test');
define('ACT_CHANGE_EMAIL', 'change_email');
define('ACT_CHANGE_PASSWD', 'change_password');
define('ACT_ADD_STUDENT', 'add_student');
define('ACT_REMOVE_STUDENT', 'remove_active_student');
define('ACT_CLEAR_STUDENT_DATA', 'clear_student');
define('ACT_LOAD_STUDENT_QUEST', 'load_student_question');
define('ACT_ADD_STUDENT_ANSWER', 'add_student_answer');


// -------------------------------
//  LOGS TYPES
// -------------------------------
define('LOG_TYPE_LOGIN_FAIL', 1);
define('LOG_TYPE_LOGIN_SUCCESS', 2);
define('LOG_TYPE_LOST_PASSWD_FAIL', 3);
define('LOG_TYPE_LOST_PASSWD_SUCCESS', 4);
define('LOG_TYPE_CHANGE_EMAIL_FAIL', 5);
define('LOG_TYPE_CHANGE_EMAIL_SUCCESS', 6);
define('LOG_TYPE_CHANGE_PASSWORD_FAIL', 7);
define('LOG_TYPE_CHANGE_PASSWORD_SUCCESS', 8);
define('LOG_TYPE_REGISTRATION_FAIL', 9);
define('LOG_TYPE_REGISTRATION_SUCCESS', 10);
define('LOG_TYPE_USER_LOGOUT', 11);

// -------------------------------
//  LOGS TEXT
// -------------------------------
define('LOG_USERNAME_EXIST', 'Użytkownik o takej nazwie już istnieje!');
define('LOG_EMAIL_EXIST', 'Użytkownik o takim emailu już istnieje!');
define('LOG_EMAIL_FAIL', 'Nie moge wyslac wiadomosci na podany adres email!');
define('LOG_REGISTRATION_FAIL', 'Przepraszamy, coś poszło nie tak!');


// -------------------------------
//  REQUESTS CONFIG
// -------------------------------

// change email
define('REQUEST_EMAIL_TYPE', 1);
define('REQUEST_EMAIL_TOKEN', 'token_email');
define('REQUEST_EMAIL_TOKEN_CORRECT', 'Adres email został zmieniony');
define('REQUEST_EMAIL_TITLE', 'Żądanie zmiany adresu email');
define('REQUEST_EMAIL_INFO', $changeEmailTemplate);
define('REQUEST_EMAIL_BTN', 'Kliknij aby potwierdzić zmianę adresu email');

// change password
define('REQUEST_PASSWD_TYPE', 2);
define('REQUEST_PASSWD_TOKEN', 'token_password');
define('REQUEST_PASSWD_TOKEN_CORRECT', 'Hasło został zmienione');
define('REQUEST_PASSWD_TITLE', 'Żądanie zmiany hasła');
define('REQUEST_PASSWD_INFO', $changePasswordTemplate);
define('REQUEST_PASSWD_BTN', 'Kliknij aby potwierdzić zmianę adresu email');

// registration
define('REQUEST_REGISTR_TYPE', 3);
define('REQUEST_REGISTR_TOKEN', 'token_registration');
define('REQUEST_REGISTR_TOKEN_CORRECT', 'Użytkownik został zarejestrowany');
define('REQUEST_REGISTR_TITLE', 'Potwierdzenie rejestracji');


// szablon maila rejestracji
define('REQUEST_REGISTR_INFO', $registrationEmailTemplate);
define('REQUEST_REGISTR_MAIL_CORRECT', 'Rejestracja zostala potwierdzona! Zaloguj się do serwisu! ');

// lost password
define('REQUEST_LOST_PASSWD_TYPE', 4);
define('REQUEST_LOST_PASSWD_TOKEN', 'token_lpassword');
define('REQUEST_LOST_PASSWD_TOKEN_CORRECT', 'Hasło został zmienione');
define('REQUEST_LOST_PASSWD_TITLE', 'Żądanie zmiany hasła');
define('REQUEST_LOST_PASSWD_INFO', $lostPasswordTemplate);
define('REQUEST_LOST_PASSWD_INFO2', '<p>To nie ty? zignoruj tego maila!</p>');
define('REQUEST_LOST_PASSWD_BTN', 'Kliknij aby potwierdzić zmianę hasła');


// -------------------------------
//  LOGIN
// -------------------------------
define('POST_LOGIN_USER_LOGIN', 'username');
define('POST_LOGIN_USER_PASSWORD', 'passwd');


// -------------------------------
//  REGISTRATION
// -------------------------------
define('POST_REGISTRATION_USER_LOGIN', 'username');
define('POST_REGISTRATION_USER_PASSWORD', 'passwd');
define('POST_REGISTRATION_USER_EMAIL', 'email');
define('REGISTRATION_SUCCESS_TEXT', 'Sprawdź adres emial aby dokonczyc rejestracje!');
define('REGISTRATION_HASH_LENGTH', 128);

// -------------------------------
//  LOST PASSWORD
// -------------------------------
define('POST_LOST_PASSWORD_USER_LOGIN', 'username');
define('POST_LOST_PASSWORD_USER_EMAIL', 'email');
define('LOST_PASSWORD_HASH_LENGTH', 200);
define('LOST_PASSWORD_NEW_PASS_LENGTH', 10);


// -------------------------------
//   TEST
// -------------------------------
define('TEST_HASH_LENGTH', 6);

define('TEST_GRADGE_PERCENT_5',90);
define('TEST_GRADGE_PERCENT_4',75);
define('TEST_GRADGE_PERCENT_3',51);
define('TEST_GRADGE_PERCENT_2',30);


// -------------------------------
//  ACTIVE TEST
// -------------------------------
define('COOKIE_TEST_ID', 'activetest');
define('COOKIE_STUDENT_ID', 'student');
define('COOKIE_STUDENT_TEST', 'student_name');
define('COOKIE_TEST_LIFE', time() + (3600 * 3)); // 3 h
define('STUDENT_TEST_HASH_LENGTH', 30);


// -------------------------------
//  PDF
// -------------------------------
define('URL_TEST_PDF_GET', 'test_code');


// -------------------------------
//  USER SETTINGS
// -------------------------------
define('POST_USER_EMAIL', 'email');
define('POST_USER_PASSWORD', 'password');
define('POST_USER_NEW_PASSWORD', 'new_password');
define('CHANGE_EMAIL_HASH_LENGTH', 200);
define('CHANGE_PASSWORD_HASH_LENGTH', 200);

// *******************************
//
//  TEMPLATE TEXT
//
// *******************************

// -------------------------------
//  HOMEPAGE
// -------------------------------
define('TMP_HOMEPAGE_TITLE', 'Moja ojczyzna');
define('TMP_HOMEPAGE_SUBTITLE', 'Historia Polski dla najmłodszych');
define('TMP_HOMEPAGE_SUBTITLE2', 'Koniec z nudnym przyswajaniem wiedzy. Razem ze swoim dzieckiem odkrywaj historię naszej ojczyzny na nowo!');
define('TMP_HOMEPAGE_TEACHER_TITLE', 'Jesteś nauczycielem lub rodzicem?');
define('TMP_HOMEPAGE_TEACHER_SUBTITLE', 'Rozwój dzieci jest dla Ciebie czyms ważnym? W nauce dziecka cenisz sobie niekonwencjonalne i innowacyjne rozwiązania? Jeśli na każde z naszych pytań odpowiedziałeś "tak", to znaczy, że jesteś w dobrym miejscu. Zapraszamy do skorzystania z naszej platformy edukacyjnej, która da Tobie możliwość nauczania dziecka w sposób, który go zaskoczy!');
define('TMP_HOMEPAGE_QUOTATION', '"Naród, który traci pamięć przestaje być Narodem – staje się jedynie zbiorem ludzi, czasowo zajmujących dane terytorium."');
define('TMP_HOMEPAGE_QUOTATION_SUBTITLE', 'Edukacja<br> historyczna');
define('TMP_HOMEPAGE_SECTION2_TITLE', 'Sprawdź wiedzę swoich uczniów!');
define('TMP_HOMEPAGE_SECTION2_TEXT', 'Zestaw gotowych sprawdzianów lub własnych testów, dzięki którym szybko zweryfikujesz wiedzę swoich uczniów.');
define('TMP_HOMEPAGE_SECTION3_TITLE', 'Interaktywny sprawdzian');
define('TMP_HOMEPAGE_SECTION3_TEXT', 'Spokojnie, wszystkim się zajmiemy za Ciebie! Stwórz swój własny test w oparciu o materiały z naszej strony.');

define('TMP_HOMEPAGE_SECTION3_STEP1_TITLE', 'Podgląd na żywo');
define('TMP_HOMEPAGE_SECTION3_STEP1_TEXT', 'Śledź na żywo postępy każdego ze swoich podopiecznych.');
define('TMP_HOMEPAGE_SECTION3_STEP2_TITLE', 'Sugerowana ocena');
define('TMP_HOMEPAGE_SECTION3_STEP2_TEXT', 'Znamy progi procentowe obowiązujące w kanonie nauczania. Wystawimy sugerowaną ocenę za Ciebie.');
define('TMP_HOMEPAGE_SECTION3_STEP3_TITLE', 'Gotowe wydruki');
define('TMP_HOMEPAGE_SECTION3_STEP3_TEXT', 'Wydrukuj swój przeprowadzony test, aby móc dołączyć go do dokumentacji.');

define('TMP_HOMEPAGE_TEST_PANEL_LABEL', 'PANEL TESTÓW');
define('TMP_HOMEPAGE_TEACHER_PANEL_LABEL', 'PANEL NAUCZYCIELA');

// -------------------------------
//  ACTIVE PAGES
// -------------------------------

define('TMP_ACTIVE_TESTS_TITLE', 'Twoje aktywne testy');
define('TMP_ACTIVE_TESTS_SUBTITLE', 'Monitoruj na bieżąco postępy swoich uczniów podczas trwania testu');
define('TMP_ACTIVE_TESTS_NO_ACTIVE_TITLE', 'Brak aktywnych testów w systemie');
define('TMP_ACTIVE_TESTS_NO_ACTIVE_SUBTITLE', 'Utwórz nowy test, aby monitorować na bieżąco postępy swoich uczniów');

// -------------------------------
//  ARCHIVAL TESTS
// -------------------------------

define('TMP_ARCHIVAL_TESTS_TITLE', 'Archiwalne testy');
define('TMP_ARCHIVAL_TESTS_SUBTITLE', 'Pobierz i wydrukuj niezbędne dokumenty');
define('TMP_ARCHIVAL_TESTS_NO_TESTS_TITLE', 'Lista archiwalnych testów');
define('TMP_ARCHIVAL_TESTS_NO_TESTS_SUBTITLE', 'Utwórz nowy test, aby monitorować na bieżąco postępy swoich uczniów');


// -------------------------------
//  QUESTIONS COLLECTION
// -------------------------------
define('TMP_QUESTIONS_COLLECTION_TITLE', 'Pytania dostępne w systemie');
define('TMP_QUESTIONS_COLLECTION_SUBTITLE', 'Skorzystaj z naszego gotowego zestawu pytań lub dodaj własne');

define('TMP_QUESTIONS_COLLECTION_ADD_CARD', 'Nowa karta');
define('TMP_QUESTIONS_COLLECTION_ADD_LESSON', 'dodaj lekcję');
define('TMP_QUESTIONS_COLLECTION_ADD_LESSON_MODAL', 'Dodawanie nowej lekcji');


// -------------------------------
//  USER PANEL
// -------------------------------

define('TMP_USER_PANEL_TITLE', 'Panel użytkownika');
define('TMP_USER_PANEL_SUBTITLE', 'Zmień swój adres e-mail lub hasło');
define('TMP_USER_PANEL_SUBTITLE_1', 'Podstawowe dane');
define('TMP_USER_PANEL_SUBTITLE_2', 'Zmiana adresu email');
define('TMP_USER_PANEL_SUBTITLE_3', 'Zmiana hasła');
define('TMP_USER_PANEL_FIELD_1', 'Login');
define('TMP_USER_PANEL_FIELD_2', 'adres email');
define('TMP_USER_PANEL_FIELD_3', 'nowy adres email');
define('TMP_USER_PANEL_FIELD_3_PLACEHOLDER', 'mojnowyadres@address.pl');
define('TMP_USER_PANEL_BTN_1', 'zmień email');
define('TMP_USER_PANEL_BTN_1_INFO_BOX', 'Potwierdzenie zmiany adresu email zostanie wysyłane na Twój stary adres');
define('TMP_USER_PANEL_FIELD_4', 'aktualne hasło');
define('TMP_USER_PANEL_FIELD_4_PLACEHOLDER', 'aktualne hasło');
define('TMP_USER_PANEL_FIELD_5', 'nowe hasło');
define('TMP_USER_PANEL_FIELD_5_PLACEHOLDER', 'nowe hasło');
define('TMP_USER_PANEL_FIELD_6', 'powtórz hasło');
define('TMP_USER_PANEL_FIELD_6_PLACEHOLDER', 'powtórz nowe hasło');
define('TMP_USER_PANEL_BTN_2', 'zmień hasło');
define('TMP_USER_PANEL_BTN_2_INFO_BOX', 'Potwierdzenie zmiany hasła zostanie wysyłane na Twój adres');

// -------------------------------
//  TEST CREATOR
// -------------------------------

define('TMP_TEST_CREATOR_TITLE', 'Utwórz test dla swoich uczniów');
define('TMP_TEST_CREATOR_SUBTITLE', 'Pamietaj o aktywowaniu testu po jego utworzeniu w panelu "aktywne testy".');
define('TMP_TEST_CREATOR_TYPE_INFO', 'Wyświetlanie <u>podpowiedzi obrazkowych</u> polecamy najmłodszym uczniom.');
define('TMP_TEST_CREATOR_INFO_BOX', 'Uwaga! Możesz mieć tylko jeden aktywny test! Aby utworzyć nowy najpierw zakończ bądź usuń poprzedni!');

define('', '');






<?php

/* pages */
$collection->add(URL_HOMEPAGE, new System\Engine\Router\Route('Page', 'homepage', '\System\Controller\Page'));
$collection->add(URL_ACTIVE_TESTS, new System\Engine\Router\Route('Page', 'activeTests', '\System\Controller\Page'));
$collection->add(URL_TEST_CREATOR, new System\Engine\Router\Route('Page', 'testCreator', '\System\Controller\Page'));
$collection->add(URL_ARCHIVAL_TESTS, new System\Engine\Router\Route('Page', 'archivalTests', '\System\Controller\Page'));
$collection->add(URL_QUESTIONS_COLLECTION, new System\Engine\Router\Route('Page', 'questionsCollection', '\System\Controller\Page'));
$collection->add(URL_USER_PANEL, new System\Engine\Router\Route('Page', 'userPanel', '\System\Controller\Page'));
$collection->add(URL_TEST, new System\Engine\Router\Route('Page', 'test', '\System\Controller\Page'));
$collection->add(URL_TEST_PDF, new System\Engine\Router\Route('Page', 'resultsPdf', '\System\Controller\Page'));
$collection->add(URL_LESSONS, new System\Engine\Router\Route('Page', 'lessons', '\System\Controller\Page'));
$collection->add(URL_ERROR_404, new System\Engine\Router\Route('Page', 'error404', '\System\Controller\Page'));

/* actions */
$collection->add(ACT_LOGIN, new System\Engine\Router\Route('User', 'login', '\System\Controller\User'));
$collection->add(ACT_LOGOUT, new System\Engine\Router\Route('User', 'logout', '\System\Controller\User'));
$collection->add(ACT_REGISTRATION, new System\Engine\Router\Route('User', 'registration', '\System\Controller\User'));
$collection->add(ACT_LOST_PASSWORD, new System\Engine\Router\Route('User', 'lostPassword', '\System\Controller\User'));
$collection->add(ACT_ADD_LESSON, new System\Engine\Router\Route('Lesson', 'add', '\System\Controller\Lesson'));
$collection->add(ACT_RMV_LESSON, new System\Engine\Router\Route('Lesson', 'remove', '\System\Controller\Lesson'));
$collection->add(ACT_ADD_QUESTION, new System\Engine\Router\Route('Question', 'add', '\System\Controller\Question'));
$collection->add(ACT_RMV_QUESTION, new System\Engine\Router\Route('Question', 'remove', '\System\Controller\Question'));
$collection->add(ACT_ADD_TEST, new System\Engine\Router\Route('Test', 'add', '\System\Controller\Test'));
$collection->add(ACT_GET_ALL_LESSONS, new System\Engine\Router\Route('Test', 'getAllFullLessonsForTestCreator', '\System\Controller\Lesson'));
$collection->add(ACT_GET_ACTIVE_TEST, new System\Engine\Router\Route('Test', 'getAllActiveByTeacherId', '\System\Controller\Test'));
$collection->add(ACT_REMOVE_ACTIVE_TEST, new System\Engine\Router\Route('Test', 'remove', '\System\Controller\Test'));
$collection->add(ACT_START_TEST, new System\Engine\Router\Route('Test', 'start', '\System\Controller\Test'));
$collection->add(ACT_STOP_TEST, new System\Engine\Router\Route('Test', 'stop', '\System\Controller\Test'));
$collection->add(ACT_UPDATE_TEST, new System\Engine\Router\Route('Test', 'update', '\System\Controller\Test'));
$collection->add(ACT_CHANGE_EMAIL, new System\Engine\Router\Route('User', 'changeEmail', '\System\Controller\User'));
$collection->add(ACT_CHANGE_PASSWD, new System\Engine\Router\Route('User', 'changePassword', '\System\Controller\User'));
$collection->add(ACT_ADD_STUDENT, new System\Engine\Router\Route('Student', 'add', '\System\Controller\Student'));
$collection->add(ACT_REMOVE_STUDENT, new System\Engine\Router\Route('Student', 'removeActive', '\System\Controller\Student'));
$collection->add(ACT_LOAD_STUDENT_QUEST, new System\Engine\Router\Route('Answered', 'getOneJSON', '\System\Controller\Answered'));
$collection->add(ACT_CLEAR_STUDENT_DATA, new System\Engine\Router\Route('Student', 'clearData', '\System\Controller\Student'));
$collection->add(ACT_ADD_STUDENT_ANSWER, new System\Engine\Router\Route('Answered', 'update', '\System\Controller\Answered'));

/* lssons */
$collection->add(URL_LESSONS . '/' . URL_LESSON . '1', new System\Engine\Router\Route('Page', 'lesson', '\System\Controller\Page', '1'));
$collection->add(URL_LESSONS . '/' . URL_LESSON . '2', new System\Engine\Router\Route('Page', 'lesson', '\System\Controller\Page', '2'));
$collection->add(URL_LESSONS . '/' . URL_LESSON . '3', new System\Engine\Router\Route('Page', 'lesson', '\System\Controller\Page', '3'));
$collection->add(URL_LESSONS . '/' . URL_LESSON . '4', new System\Engine\Router\Route('Page', 'lesson', '\System\Controller\Page', '4'));
$collection->add(URL_LESSONS . '/' . URL_LESSON . '5', new System\Engine\Router\Route('Page', 'lesson', '\System\Controller\Page', '5'));
$collection->add(URL_LESSONS . '/' . URL_LESSON . '6', new System\Engine\Router\Route('Page', 'lesson', '\System\Controller\Page', '6'));
$collection->add(URL_LESSONS . '/' . URL_LESSON . '7', new System\Engine\Router\Route('Page', 'lesson', '\System\Controller\Page', '7'));
$collection->add(URL_LESSONS . '/' . URL_LESSON . '8', new System\Engine\Router\Route('Page', 'lesson', '\System\Controller\Page', '8'));
$collection->add(URL_LESSONS . '/' . URL_LESSON . '9', new System\Engine\Router\Route('Page', 'lesson', '\System\Controller\Page', '9'));
$collection->add(URL_LESSONS . '/' . URL_LESSON . '10', new System\Engine\Router\Route('Page', 'lesson', '\System\Controller\Page', '10'));
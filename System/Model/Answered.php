<?php

namespace System\Model;

use \PDO;

class Answered extends \System\Engine\Model
{
    private $tableName = DATABASE_TAB_ANSWERED;

    /**
     * [STUDENT ANSWERS]
     * id
     * student_id
     * test_id
     * question_id
     * answer_id
     * date
     * is_correct
     *
     */

    private $fields = array(
        'id' => array('name' => 'uid'),
        'student_id' => array('name' => 'studentId'),
        'test_id' => array('name' => 'testId'),
        'question_id' => array('name' => 'questionId'),
        'answer_id' => array('name' => 'answerId'),
        'date' => array('name' => 'dateAnswered'),
        'is_correct' => array('name' => 'isCorrect')
    );


    /**
     * get all answered id
     * @param $testId
     * @param $studentId
     * @param null $date
     * @param bool $limit
     * @param bool $answered
     * @return array|null
     */
    public function getAllAnsweredId($testId, $studentId, $date = null, $limit = false, $onlyAnswered = false)
    {
        $query = $this->pdo->prepare("SELECT question_id, answer_id, is_correct, date from $this->tableName WHERE test_id = :test_id AND student_id = :student_id AND date <= NOW() " . ($onlyAnswered ? ' AND answered=1 ' : '') . ($limit ? ' AND answered=0 ' : '') . ($date ? "AND date > '" . $date . "' " : "") . "  ORDER BY date ASC " . ($limit ? 'LIMIT 1' : ''));
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->bindValue(':student_id', $studentId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }


    /**
     * get all by test id
     * @param $testId
     * @return array|null
     */
    public function getAllByTestId($testId)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE test_id = :test_id AND answered = 1");
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : null;
    }

    /**
     * get all by student id
     * @param $studentId
     * @param $testId
     * @return array|null
     */
    public function getAllByStudentId($studentId, $testId)
    {
        $query = $this->pdo->prepare("SELECT question_id as `q`,answer_id as `a`,is_correct as `c` from $this->tableName WHERE student_id = :student_id AND test_id = :test_id ORDER BY date DESC");
        $query->bindValue(':student_id', $studentId, PDO::PARAM_INT);
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : null;
    }

    /**
     * add new
     * @param $studentId
     * @param $testId
     * @param $questionId
     * @return bool|null
     */
    public function addNew($studentId, $testId, $questionId)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (student_id, test_id, question_id, answer_id, is_correct, answered) VALUES (:student_id, :test_id, :question_id, 0, false, false)");
        $query->bindValue(':student_id', $studentId, PDO::PARAM_INT);
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->bindValue(':question_id', $questionId, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : null;
    }

    /**
     * update
     * @param $answerId
     * @param $studentId
     * @param $testId
     * @param $questionId
     * @param $isCorrect
     * @return bool
     */
    public function update($answerId, $studentId, $testId, $questionId, $isCorrect)
    {
        $query = $this->pdo->prepare("UPDATE $this->tableName SET answer_id=:answer_id, is_correct=:is_correct, answered=TRUE, date=NOW() WHERE student_id=:student_id AND test_id=:test_id AND question_id=:question_id");
        $query->bindValue(':answer_id', $answerId, PDO::PARAM_INT);
        $query->bindValue(':student_id', $studentId, PDO::PARAM_INT);
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->bindValue(':question_id', $questionId, PDO::PARAM_INT);
        $query->bindValue(':is_correct', $isCorrect, PDO::PARAM_BOOL);
        $query->execute();
        return $query->rowCount() ? true : false;
    }
}
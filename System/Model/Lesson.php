<?php

namespace System\Model;

use \PDO;

class Lesson extends \System\Engine\Model
{
    private $tableName = DATABASE_TAB_LESSON;

    /**
     * id
     * title
     * description
     * teacher_id
     */

    private $fields = array(
        'id' => array('name' => 'uid'),
        'title' => array('name' => 'title'),
        'description' => array('name' => 'description'),
        'teacher_id' => array('name' => 'teacherId'),
    );

    /**
     * get all
     * @return array|null
     */
    public function getAll() {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE teacher_id = 0 ORDER BY id DESC ");
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : null;
    }


    /**
     * main and teacher lessons
     * @param $teacherId
     * @return array|null
     */
    public function getDefaultsAndTeacher($teacherId)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE teacher_id = 0 OR teacher_id = :teacher_id ORDER BY id DESC ");
        $query->bindValue(':teacher_id', $teacherId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : null;
    }


    /**
     * add
     * @param $title
     * @param $description
     * @param $teacherId
     * @return array|bool
     */
    public function add($title, $description, $teacherId)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (title,description,teacher_id) VALUES (:title, :description,:teacher_id)");
        $query->bindValue(':title', $title, PDO::PARAM_STR);
        $query->bindValue(':description', $description, PDO::PARAM_STR);
        $query->bindValue(':teacher_id', $teacherId, PDO::PARAM_INT);
        $query->execute();
        $lessonId = (int)$this->pdo->lastInsertId();

        return $query->rowCount() ? array('id' => $lessonId) : false;
    }

    /**
     * remove lesson by lessonID
     *
     * @param $lessonId
     * @param $teacherId
     * @return bool
     */
    public function remove($lessonId, $teacherId)
    {
        $query = $this->pdo->prepare("DELETE FROM $this->tableName WHERE  id = :id AND teacher_id = :teacher_id AND teacher_id > 0");
        $query->bindValue(':id', $lessonId, PDO::PARAM_INT);
        $query->bindValue(':teacher_id', $teacherId, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }

}
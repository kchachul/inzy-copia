<?php

namespace System\Model;

use \PDO;

class User extends \System\Engine\Model
{
    private $tableName = DATABASE_TAB_USER;

    /**
     * [USER]
     * id
     * login
     * email
     * password
     * ks
     * confirm
     * created
     * verified
     */

    private $fields = array(
        'id' => array('name' => 'uid'),
        'login' => array('name' => 'username'),
        'email' => array('name' => 'addressEmail'),
        'password' => array('name' => 'passwd'),
        'ks' => array('name' => 'key'),
        'confirm' => array('name' => 'confKey'),
        'created' => array('name' => 'dateCreated'),
        'verified' => array('name' => 'isVerified')
    );


    /**
     * key stretching iterations by login
     * @param $login
     * @return array|null
     */
    public function giveKeyStretchingIterations($login)
    {
        $query = $this->pdo->prepare("SELECT ks from $this->tableName WHERE login=:login LIMIT 1");
        $query->bindValue(':login', $login, PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);
        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }


    /**
     * key streaching iterations by user id
     * @param $userId
     * @return null
     */
    public function giveKeyStretchingIterationsByUserId($userId)
    {
        $query = $this->pdo->prepare("SELECT ks from $this->tableName WHERE id=:id LIMIT 1");
        $query->bindValue(':id', $userId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);
        return isset($data) ? $data[0]['ks'] : null;
    }


    /**
     * check - user exist
     * @param $login
     * @param $password
     * @return array|null
     */
    public function userExist($login, $password)
    {
        $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE login=:login AND password=:password AND verified=1 LIMIT 1");
        $query->bindValue(':login', $login, PDO::PARAM_STR);
        $query->bindValue(':password', $password, PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);
        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }

    /**
     * check - user exist by id
     * @param $userId
     * @param $password
     * @return bool
     */
    public function userExistById($userId, $password)
    {
        $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE id=:id AND password=:password AND verified=1 LIMIT 1");
        $query->bindValue(':id', $userId, PDO::PARAM_INT);
        $query->bindValue(':password', $password, PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) && $data[0] ? true : false;
    }

    /**
     * check - login exist
     * @param $login
     * @return bool
     */
    public function loginExist($login)
    {
        $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE login=:login AND verified=1 LIMIT 1");
        $query->bindValue(':login', $login, PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) && $data[0] ? true : false;
    }

    /**
     * check - email exist
     * @param $email
     * @return bool
     */
    public function emailExist($email)
    {
        $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE email=:email AND verified=1 LIMIT 1");
        $query->bindValue(':email', $email, PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) && $data[0] ? true : false;
    }


    /**
     * get user data
     * @param $id
     * @return null
     */
    public function getUserData($id)
    {
        $query = $this->pdo->prepare("SELECT login,email from $this->tableName WHERE id=:id LIMIT 1");
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);
        return isset($data) ? $data[0] : null;
    }


    /**
     * update new email
     * @param $email
     * @param $userID
     * @return bool
     */
    public function updateEmail($email, $userID)
    {
        $query = $this->pdo->prepare("UPDATE $this->tableName SET email = :email WHERE id = :user_id");
        $query->bindValue(':email', $email, PDO::PARAM_STR);
        $query->bindValue(':user_id', $userID, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }

    /**
     * update new password
     * @param $password
     * @param $userID
     * @return bool
     */
    public function updatePassword($password, $userID)
    {
        $query = $this->pdo->prepare("UPDATE $this->tableName SET password = :password WHERE id = :user_id");
        $query->bindValue(':password', $password, PDO::PARAM_STR);
        $query->bindValue(':user_id', $userID, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }

    /**
     * update lost password
     * @param $userID
     * @param $iterations
     * @param $password
     * @return bool
     */
    public function updateLostPassword($userID, $iterations, $password)
    {
        $query = $this->pdo->prepare("UPDATE $this->tableName SET password = :password, ks = :ks  WHERE id = :user_id");
        $query->bindValue(':password', $password, PDO::PARAM_STR);
        $query->bindValue(':ks', $iterations, PDO::PARAM_INT);
        $query->bindValue(':user_id', $userID, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }


    /**
     * user verified
     * @param $userID
     * @return bool
     */
    public function updateRegistration($userID)
    {
        $query = $this->pdo->prepare("UPDATE $this->tableName SET verified = 1 WHERE id = :user_id");
        $query->bindValue(':user_id', $userID, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }


    /**
     * add
     * @param $login
     * @param $password
     * @param $email
     * @param $ks
     * @param $token
     * @return bool|int
     */
    public function add($login, $password, $email, $ks, $token)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (login, email, password, ks, confirm, created, verified) VALUES ( :login, :email, :password, :ks, :confirm, NOW(), false)");
        $query->bindValue(':login', $login, PDO::PARAM_STR);
        $query->bindValue(':email', $email, PDO::PARAM_STR);
        $query->bindValue(':password', $password, PDO::PARAM_STR);
        $query->bindValue(':ks', $ks, PDO::PARAM_INT);
        $query->bindValue(':confirm', $token, PDO::PARAM_STR);
        $query->execute();
        $userId = (int)$this->pdo->lastInsertId();

        return $query->rowCount() ? $userId : false;
    }


    public function userExistByLoginAndEmail($login, $emial){
            $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE login=:login AND email=:email LIMIT 1");
            $query->bindValue(':login', $login, PDO::PARAM_STR);
            $query->bindValue(':email', $emial, PDO::PARAM_STR);
            $query->execute();
            $data = $query->fetchAll(\PDO::FETCH_ASSOC);

            return isset($data) && $data[0] ? $data[0]['id'] : false;
    }



}
<?php

namespace System\Model;

use \PDO;
use System\Engine\Helpers\MainHelper;

class Log extends \System\Engine\Model
{
    private $tableName = DATABASE_TAB_LOG;

    /**
     * add
     * @param $type
     * @param $userId
     * @param $data
     */
    public function add($type, $userId, $data)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (date, ip, type, user_id, data) VALUES (NOW(), :ip, :type, :user_id, :data)");
        $query->bindValue(':ip', MainHelper::getAddrIp(), PDO::PARAM_STR);
        $query->bindValue(':type', $type, PDO::PARAM_INT);
        $query->bindValue(':user_id', $userId, PDO::PARAM_INT);
        $query->bindValue(':data', MainHelper::parseToJson($data), PDO::PARAM_STR);
        $query->execute();
    }
}
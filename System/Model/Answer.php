<?php

namespace System\Model;

use \PDO;

class Answer extends \System\Engine\Model
{
    private $tableName = DATABASE_TAB_ANSWER;

    /**
     * id
     * title
     * is_correct
     * question_id
     */
    private $fields = array(
        'id' => array('name' => 'uid'),
        'title' => array('name' => 'title'),
        'is_correct' => array('name' => 'correct'),
        'question_id' => array('name' => 'questionId')
    );


    /**
     * get all by question id
     *
     * @param $questionId
     * @return array|null
     */
    public function getAllByQuestionId($questionId)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE question_id = :question_id");
        $query->bindValue(':question_id', $questionId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }

    /**
     * check answer is correct
     * @param $answerId
     * @return bool
     */
    public function isCorrect($answerId)
    {
        $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE id = :id AND is_correct = TRUE LIMIT 1");
        $query->bindValue(':id', $answerId, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }

    /**
     * add new answer
     *
     * @param $answer
     * @return bool|null
     */
    public function add($answer, $questionId)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (title, is_correct, question_id) VALUES (:title, :is_correct, :question_id)");
        $query->bindValue(':title', $answer['title'], PDO::PARAM_STR);
        $query->bindValue(':is_correct', ($answer['isCorrect'] == 1), PDO::PARAM_INT);
        $query->bindValue(':question_id', $questionId, PDO::PARAM_INT);
        $result = $query->execute();

        return $result ? $result : null;
    }

    /**
     * remove answer
     *
     * @param $answerId
     * @return bool
     */
    public function remove($answerId)
    {
        $query = $this->pdo->prepare("DELETE FROM $this->tableName WHERE id = :answer_id");
        $query->bindValue(':answer_id', $answerId, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }
}



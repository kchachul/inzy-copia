<?php

namespace System\Model;

use \PDO;

class Student extends \System\Engine\Model
{
    private $tableName = DATABASE_TAB_STUDENT;

    /**
     * [STUDENT]
     * id
     * join_date
     * name
     * test_id
     *
     */

    private $fields = array(
        'id' => array('name' => 'uid'),
        'join_date' => array('name' => 'joinDate'),
        'name' => array('name' => 'name'),
        'test_id' => array('name' => 'testId'),
        'hash' => array('name' => 'token'),
        'points' => array('name' => 'uPoints'),
        'answered' => array('name' => 'answeredCount')
    );


    /**
     * select all students by test id
     *
     * @param $testId
     * @return array|null
     */
    public function getAllByTestId($testId)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE test_id = :test_id");
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : false;
    }


    /**
     * add new
     * @param $name
     * @param $testId
     * @param $userHash
     * @return bool|int
     */
    public function add($name, $testId, $userHash)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (join_date,name, test_id, hash) VALUES (NOW(), :name, :test_id, :hash)");
        $query->bindValue(':name', $name, PDO::PARAM_STR);
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->bindValue(':hash', $userHash, PDO::PARAM_STR);
        $query->execute();
        $studentId = (int)$this->pdo->lastInsertId();

        return $query->rowCount() ? $studentId : false;
    }


    /**
     * student exist
     * @param $testId
     * @param $studentId
     * @param $studentHash
     * @return bool
     */
    public function exist($testId, $studentId)
    {
        $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE id = :id AND test_id = :test_id LIMIT 1");
        $query->bindValue(':id', $studentId, PDO::PARAM_INT);
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? true : false;
    }



    /**
     * student with the same name exists
     * @param $studentName
     * @param $testId
     * @return bool
     */
    public function nameExist($studentName, $testId)
    {
        $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE test_id = :test_id AND name = :name LIMIT 1");
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->bindValue(':name', $studentName, PDO::PARAM_STR);
        $query->execute();

        return $query->rowCount()  ? true : false;
    }


    /**
     * get student data
     * @param $studentId
     * @return array|bool
     */
    public function getData($studentId)
    {
        $query = $this->pdo->prepare("SELECT name, answered from $this->tableName WHERE id = :id LIMIT 1");
        $query->bindValue(':id', $studentId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : false;
    }


    /**
     * get student data by hash
     * @param $studentId
     * @return array|bool
     */
    public function getDataByHash($studentHash)
    {
        $query = $this->pdo->prepare("SELECT name, answered from $this->tableName WHERE hash = :hash LIMIT 1");
        $query->bindValue(':hash', $studentHash, PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : false;
    }


    /**
     * @param $lastStudentId
     * @param $testID
     * @return array|bool|null
     */
    public function getNewStudents($lastStudentId, $testID)
    {
        $query = $this->pdo->prepare("SELECT id, name, answered, points, join_date from $this->tableName WHERE id > :id AND test_id = :test_id");
        $query->bindValue(':id', $lastStudentId, PDO::PARAM_INT);
        $query->bindValue(':test_id', $testID, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : false;
    }


    /**
     * @param $studentHash
     * @return array|bool|null
     */
    public function getStudentIdFromHash($studentHash)
    {
        $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE hash = :hash");
        $query->bindValue(':hash', $studentHash, PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : false;
    }


    /**
     * remove
     * @param $studentId
     * @return bool
     */
    public function remove($studentId)
    {
        $query = $this->pdo->prepare("DELETE FROM $this->tableName WHERE id = :student_id ");
        $query->bindValue(':student_id', $studentId, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }

}
<?php

namespace System\Model;

use \PDO;

class Archival extends \System\Engine\Model
{
    private $tableName = DATABASE_TAB_ARCHIVAL;

    /**
     * add
     * @param $student
     * @param $answered
     * @param $testId
     * @return bool
     */
    public function add($student, $answered, $testId)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (student_id, test_id, name, data, points, join_date, answered_count) VALUES (:student_id, :test_id, :name, :data, :points, :join_date, :answered_count)");
        $query->bindValue(':student_id', $student['uid'], PDO::PARAM_INT);
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->bindValue(':name', $student['name'], PDO::PARAM_STR);
        $query->bindValue(':data', $answered, PDO::PARAM_STR);
        $query->bindValue(':points', $student['uPoints'], PDO::PARAM_INT);
        $query->bindValue(':join_date', $student['joinDate'], PDO::PARAM_STR);
        $query->bindValue(':answered_count', $student['answeredCount'], PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }


    /**
     * get all by test id
     * @param $testId
     * @return array|null
     */
    public function getAllByTestId($testId){
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE test_id = :test_id");
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : null;
    }

}
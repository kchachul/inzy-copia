<?php

namespace System\Model;

use \PDO;

class Scene extends \System\Engine\Model
{

    private $tableName = DATABASE_TAB_SCENE;

    /**
     * id
     * title
     * description
     * lesson_id
     */

    private $fields = array(
        'id' => array('name' => 'uid'),
        'title' => array('name' => 'title'),
        'description' => array('name' => 'description'),
        'lesson_id' => array('name' => 'lessonId')
    );


    /**
     * get all by lesson id
     * @param $lessonId
     * @return array|null
     */
    public function getAllByLessonId($lessonId)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE lesson_id = :lesson_id");
        $query->bindValue(':lesson_id', $lessonId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : null;
    }


    /**
     * select scene id by lesson id
     *
     * @param $lessonId
     * @return array|null
     */
     public function getIdByLessonId($lessonId)
     {
         $query = $this->pdo->prepare("SELECT id from $this->tableName  WHERE lesson_id = :lesson_id LIMIT 1");
         $query->bindValue(':lesson_id', $lessonId, PDO::PARAM_INT);
         $query->execute();
         $data = $query->fetchAll(\PDO::FETCH_ASSOC);

         return isset($data) ? $this->mapFields($data, $this->fields)[0] : null;
     }


    /**
     * @param $title
     * @param $description
     * @return array|null
     */
    public function add($title, $description, $lessonId)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (title,description,lesson_id) VALUES (:title, :description,:lesson_id)");
        $query->bindValue(':title', $title, PDO::PARAM_STR);
        $query->bindValue(':description', $description, PDO::PARAM_STR);
        $query->bindValue(':lesson_id', $lessonId, PDO::PARAM_INT);
        $query->execute();
        $sceneId = (int)$this->pdo->lastInsertId();

        return $query->rowCount() ? array('id' => $sceneId) : false;
    }

}
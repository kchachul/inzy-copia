<?php

namespace System\Model;

use \PDO;

class Request extends \System\Engine\Model
{
    private $tableName = DATABASE_TAB_REQUEST;

    public function checkRequestToken($token, $type)
    {
        $query = $this->pdo->prepare("UPDATE $this->tableName SET verified=:verified WHERE verified=0 AND token=:token AND type=:type");
        $query->bindValue(':verified', true, PDO::PARAM_BOOL);
        $query->bindValue(':token', $token, PDO::PARAM_STR);
        $query->bindValue(':type', $type, PDO::PARAM_INT);
        $query->execute();
        return $query->rowCount() ? true : false;
    }

    public function selectDataFromUpdated($token)
    {
        $query = $this->pdo->prepare("SELECT user_id, data from $this->tableName WHERE verified = 1 AND token=:token LIMIT 1 ");
        $query->bindValue(':token', $token, PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);
        return isset($data) ? $data[0] : null;
    }


    /**
     * password request
     * @param $userId
     * @param $type
     * @param $token
     * @param $data
     * @return bool
     */
    public function passwordRequest($userId, $type, $token, $data)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (user_id, date, type, token, verified, data) VALUES ( :user_id, NOW(), :type, :token, :verified, :data)");
        $query->bindValue(':user_id', $userId, PDO::PARAM_INT);
        $query->bindValue(':type', $type, PDO::PARAM_INT);
        $query->bindValue(':token', $token, PDO::PARAM_STR);
        $query->bindValue(':verified', false, PDO::PARAM_BOOL);
        $query->bindValue(':data', $data, PDO::PARAM_STR);
        $query->execute();

        return $query->rowCount() ? true : false;
    }


    /**
     * email request
     * @param $userId
     * @param $type
     * @param $token
     * @param $email
     * @return bool
     */
    public function setNewEmailRequest($userId, $type, $token, $email)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (user_id, date, type, token, verified, data) VALUES ( :user_id, NOW(), :type, :token, :verified, :data)");
        $query->bindValue(':user_id', $userId, PDO::PARAM_INT);
        $query->bindValue(':type', $type, PDO::PARAM_INT);
        $query->bindValue(':token', $token, PDO::PARAM_STR);
        $query->bindValue(':verified', false, PDO::PARAM_BOOL);
        $query->bindValue(':data', $email, PDO::PARAM_STR);
        $query->execute();
        return $query->rowCount() ? true : false;
    }


    /**
     * registration request
     * @param $userId
     * @param $type
     * @param $token
     * @return bool
     */
    public function setNewRegistrationRequest($userId, $type, $token)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (user_id, date, type, token, verified) VALUES ( :user_id, NOW(), :type, :token, :verified)");
        $query->bindValue(':user_id', $userId, PDO::PARAM_INT);
        $query->bindValue(':type', $type, PDO::PARAM_INT);
        $query->bindValue(':token', $token, PDO::PARAM_STR);
        $query->bindValue(':verified', false, PDO::PARAM_BOOL);
        $query->execute();
        return $query->rowCount() ? true : false;
    }


}
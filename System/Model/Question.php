<?php

namespace System\Model;

use \PDO;

class Question extends \System\Engine\Model
{
    private $tableName = DATABASE_TAB_QUESTION;

    /**
     * id
     * title
     * image
     * test_id
     * scene_id
     */

    private $fields = array(
        'id' => array('name' => 'uid'),
        'title' => array('name' => 'title'),
        'image' => array('name' => 'image'),
        'test_id' => array('name' => 'testId'),
        'scene_id' => array('name' => 'sceneId')
    );


    /**
     * get all questions
     *
     * @return array|null
     */
    public function getAll()
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName");
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }

    /**
     * get all questions by test Id
     *
     * @param $testId
     * @return array|null
     */
    public function getAllByTestId($testId)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE test_id = :test_id");
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }


    /**
     * get one with answers
     * @param $questionId
     * @return array|null
     */
    public function getOneWithAnswers($questionId)
    {
        $query = $this->pdo->prepare("SELECT q.title, a.title as answer, a.id as answerId, a.is_correct FROM $this->tableName AS q INNER JOIN answer AS a ON (a.question_id = q.id) WHERE q.id =:question_id");
        $query->bindValue(':question_id', $questionId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : null;
    }


    /**
     * get all questions by scene id
     *
     * @param $sceneId
     * @return array|null
     */
    public
    function getAllBySceneId($sceneId)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE scene_id = :scene_id");
        $query->bindValue(':scene_id', $sceneId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : null;
    }


    /**
     * get One question by id
     *
     * @param $questionId
     * @return array|null
     */
    public
    function getOneById($questionId)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE id = :question_id");
        $query->bindValue(':question_id', $questionId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }

    /**
     * add new question
     *
     * @param $question
     * @return bool|null
     */
    public
    function add($question)
    {
        $query = $this->pdo->prepare("INSERT INTO $this->tableName (title, image,test_id,scene_id) VALUES (:title, :image,:test_id,:scene_id)");
        $query->bindValue(':title', $question['title'], PDO::PARAM_STR);
        $query->bindValue(':image', $question['image'], PDO::PARAM_STR);
        $query->bindValue(':test_id', $question['test_id'], PDO::PARAM_INT);
        $query->bindValue(':scene_id', $question['scene_id'], PDO::PARAM_INT);
        $result = $query->execute();
        $questionId = (int)$this->pdo->lastInsertId();

        return $query->rowCount() ? array('id' => $questionId) : false;
    }


    /**
     * remove new question
     *
     * @param $questionId
     * @return bool
     */
    public
    function remove($questionId)
    {
        $query = $this->pdo->prepare("DELETE FROM $this->tableName WHERE id = :question_id ");
        $query->bindValue(':question_id', $questionId, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }

    /**
     * get question by id
     *
     * @param $id
     * @return bool
     */
    public
    function getQuestionByID($id)
    {
        $query = $this->pdo->prepare("SELECT title from $this->tableName WHERE id=:id LIMIT 1");
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->execute();
        $items = $query->fetchAll(\PDO::FETCH_ASSOC);

        if (isset($items)) {
            return $items[0]['title'];
        } else {
            return false;
        }
    }

}
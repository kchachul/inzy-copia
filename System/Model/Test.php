<?php

namespace System\Model;

use \PDO;

class Test extends \System\Engine\Model
{
    private $tableName = DATABASE_TAB_TEST;

    /**
     * id
     * date_created
     * date_end
     * teacher_id
     * title
     * description
     * hash
     * type
     * start_slots
     * free_slots
     * active_students
     * time
     * is_active
     * is_archival
     * questions
     */

    private $fields = array(
        'id' => array('name' => 'uid'),
        'date_created' => array('name' => 'dateCreated'),
        'date_end' => array('name' => 'endDate'),
        'teacher_id' => array('name' => 'teacherId'),
        'title' => array('name' => 'shortTitle'),
        'description' => array('name' => 'shortDescription'),
        'hash' => array('name' => 'hashName'),
        'type' => array('name' => 'testType'),
        'start_slots' => array('name' => 'startSlots'),
        'free_slots' => array('name' => 'freeSlots'),
        'active_students' => array('name' => 'activeStudents'),
        'time' => array('name' => 'timeDuration'),
        'is_active' => array('name' => 'isActive'),
        'is_archival' => array('name' => 'isArchival'),
        'questions' => array('name' => 'questionsIds'),
        'questions_count' => array('name' => 'questionsCount')
    );


    /**
     * get all tests without NAME MAPPING
     *
     * @return array|null
     */
    public function getAll()
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName");
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }


    /**
     * Get one test
     *
     * @param $id
     * @return array|null
     */
    public function getOne($id)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE id=:id LIMIT 1");
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }


    /**
     * Get one by student
     *
     * @param $hash
     * @return array|null
     */
    public function getOneByStudent($hash)
    {
        $query = $this->pdo->prepare("SELECT date_end,title,description, type from $this->tableName WHERE date_end > NOW() AND hash = :hash AND is_active = 1 AND is_archival = 0 LIMIT 1");
        $query->bindValue(':hash', $hash, PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }


    public function getOneByHash($hash)
    {
        $query = $this->pdo->prepare("SELECT id,date_end,title,description, type, questions from $this->tableName WHERE date_end > NOW() AND hash = :hash AND is_active = 1 AND is_archival = 0 LIMIT 1");
        $query->bindValue(':hash', $hash, PDO::PARAM_STR);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }


    /**
     * get all tests by teacher ID
     *
     * @return array|null
     */
    public function getAllByTeacherId($teacherID)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE teacher_id=:teacher_id AND is_archival = false");
        $query->bindValue(':teacher_id', $teacherID, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }

    /**
     * get all tests by teacher ID
     *
     * @return array|null
     */
    public function getAllEndedByTeacherId($teacherID)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE teacher_id=:teacher_id AND is_archival = false AND date_end < NOW()");
        $query->bindValue(':teacher_id', $teacherID, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $data : null;
    }


    /**
     * get all tests by teacher ID
     *
     * @return array|null
     */
    public function getAllAndArchivalByTeacherId($teacherID)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE teacher_id=:teacher_id AND is_archival = true ORDER BY id DESC");
        $query->bindValue(':teacher_id', $teacherID, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }


    /**
     * get all tests by teacher ID
     *
     * @return array|null
     */
    public function getOneArchivalByTestId($testId, $teacherID)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE teacher_id=:teacher_id AND id=:test_id AND is_archival = true LIMIT 1");
        $query->bindValue(':teacher_id', $teacherID, PDO::PARAM_INT);
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields) : null;
    }


    /**
     * get all active tests by teacher ID
     *
     * @return array|null
     */
    public function getAllActiveByTeacherId($teacherID)
    {
        $query = $this->pdo->prepare("SELECT * from $this->tableName WHERE teacher_id=:teacher_id AND is_archival = 0 AND (date_end > NOW() OR date_end = '0000-00-00 00:00:00') ORDER BY date_created DESC LIMIT 1");
        $query->bindValue(':teacher_id', $teacherID, PDO::PARAM_INT);
        $query->execute();
        $items = $query->fetchAll(\PDO::FETCH_ASSOC);


        if ($items) {
            $data = array();

            foreach ($items as $activeTest) {
                $data[] = array(
                    'id' => (int)$activeTest['id'],
                    'date_created' => $activeTest['date_created'],
                    'hash' => $activeTest['hash'],
                    'title' => $activeTest['title'],
                    'description' => $activeTest['description'],
                    'date_end' => $activeTest['date_end'],
                    'time' => (int)$activeTest['time'],
                    'is_active' => (bool)$activeTest['is_active'],
                    'type' => (int)$activeTest['type'],
                    'free_slots' => (int)$activeTest['free_slots'],
                    'start_slots' => (int)$activeTest['start_slots'],
                    'active_students' => (int)$activeTest['active_students'],
                    'questions' => unserialize($activeTest['questions']) // json_decode($activeTest['questions'])
                );
            }
            return $this->mapFields($data, $this->fields);
        } else {
            return null;
        }

    }


    /**
     * add new test
     *
     * @param $test
     * @return bool
     */
    public function add($test)
    {
        $unmapedTest = $this->unmapFields($test, $this->fields, 'name');

        $query = $this->pdo->prepare("INSERT INTO $this->tableName (date_created,title, description,teacher_id,hash,type,start_slots,free_slots,time,questions,questions_count) VALUES (NOW(), :title, :description,:teacher_id,:hash,:type,:start_slots,:free_slots,:time,:questions,:questions_count)");
        $query->bindValue(':title', $unmapedTest['title'], PDO::PARAM_STR);
        $query->bindValue(':description', $unmapedTest['description'], PDO::PARAM_STR);
        $query->bindValue(':type', $unmapedTest['type'], PDO::PARAM_INT);
        $query->bindValue(':start_slots', $unmapedTest['start_slots'], PDO::PARAM_INT);
        $query->bindValue(':free_slots', $unmapedTest['start_slots'], PDO::PARAM_INT);

        $query->bindValue(':teacher_id', $unmapedTest['teacher_id'], PDO::PARAM_INT);
        $query->bindValue(':hash', $unmapedTest['hash'], PDO::PARAM_STR);
        $query->bindValue(':time', $unmapedTest['time'], PDO::PARAM_INT);
        $query->bindValue(':questions', $unmapedTest['questions'], PDO::PARAM_STR);
        $query->bindValue(':questions_count', $unmapedTest['questions_count'], PDO::PARAM_STR);
        $query->execute();

        return $query->rowCount() ? true : false;
    }


    /**
     * remove test
     *
     * @param $testId
     * @param $teacherId
     * @return bool|null
     */
    public function remove($testId, $teacherId)
    {
        $query = $this->pdo->prepare("DELETE FROM $this->tableName WHERE id = :id_test AND teacher_id = :teacher_id AND teacher_id != 0");
        $query->bindValue(':id_test', $testId, PDO::PARAM_INT);
        $query->bindValue(':teacher_id', $teacherId, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }


    /**
     * starting test = set active
     *
     * @param $testId
     * @param $teacherId
     * @param $time
     * @return bool
     */
    public function start($testId, $teacherId, $time)
    {
        $nTime = new \DateTime();
        $nTime->add(new \DateInterval('PT' . $time . 'M'));
        $newDataTime = $nTime->format('Y-m-d H:i:s');

        $query = $this->pdo->prepare("UPDATE $this->tableName SET date_end = :new_date, is_active = 1 WHERE id = :test_id AND teacher_id = :teacher_id AND is_active = 0 LIMIT 1");
        $query->bindValue(':new_date', $newDataTime, PDO::PARAM_INT);
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->bindValue(':teacher_id', $teacherId, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }


    /**
     * get time - duration of the test
     * @param $teacherId
     * @param $testId
     * @return |null
     */
    public function getTime($teacherId, $testId)
    {
        $query = $this->pdo->prepare("SELECT time from $this->tableName WHERE teacher_id=:teacher_id AND id=:id LIMIT 1");
        $query->bindValue(':teacher_id', $teacherId, PDO::PARAM_INT);
        $query->bindValue(':id', $testId, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll(\PDO::FETCH_ASSOC);

        return isset($data) ? $this->mapFields($data, $this->fields)[0] : null;
    }


    /**
     * stoping test = set disactive
     *
     * @param $testId
     * @param $teacherId
     * @return bool
     */
    public function stop($testId, $teacherId)
    {
        $query = $this->pdo->prepare("UPDATE $this->tableName SET is_active = 0, is_archival = 1 WHERE id = :test_id AND teacher_id = :teacher_id AND is_active = 1 LIMIT 1");
        $query->bindValue(':test_id', $testId, PDO::PARAM_INT);
        $query->bindValue(':teacher_id', $teacherId, PDO::PARAM_INT);
        $query->execute();

        return $query->rowCount() ? true : false;
    }

    /**
     * check = hash is unique
     *
     * @param $hash
     * @return bool
     */
    public function checkHash($hash)
    {
        $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE hash=:hash LIMIT 1");
        $query->bindValue(':hash', $hash, PDO::PARAM_STR);
        $query->execute();
        $query->fetchAll(\PDO::FETCH_ASSOC);

        return $query->rowCount() ? true : false;
    }


    public function existByTeacherId($teacherId)
    {
        $query = $this->pdo->prepare("SELECT id from $this->tableName WHERE is_archival=:is_archival AND teacher_id=:teacher_id LIMIT 1");
        $query->bindValue(':is_archival', false, PDO::PARAM_BOOL);
        $query->bindValue(':teacher_id', $teacherId, PDO::PARAM_INT);
        $query->execute();
        $query->fetchAll(\PDO::FETCH_ASSOC);

        return $query->rowCount() ? true : false;
    }

}
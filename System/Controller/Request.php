<?php

namespace System\Controller;

/**
 * Class Request
 * @package System\Controller
 */
class Request extends \System\Engine\Controller
{
    /**
     * Request constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new $this->model;
    }


    /**
     * Sprawdzanie czy jakis klucz wystepuje
     * @param string $type - typ akcji
     * @return bool
     */
    public function checkRequestToken($type)
    {
        switch ($type) {
            case 'change_email':
                return $this->changeEmail();
                break;
            case 'change_password':
                return $this->changePassword();
                break;
            case 'registration':
                return $this->registration();
                break;
            case 'lost_password':
                return $this->lostPassword();
                break;
        }
        return false;
    }


    /**
     * Sprawdzenie czy klucz ma prawidlowy format i dlugosc
     * @param $tokenName
     * @return bool
     */
    private function isCorrectToken($tokenName)
    {
        if (isset($_GET[$tokenName])) {
            return strlen($this->checkData($_GET[$tokenName])) >= REGISTRATION_HASH_LENGTH;
        } else {
            return false;
        }
    }

    /**
     * Sprawdzanie zadania: zmiany adresu email
     * @return bool
     */
    private function changeEmail()
    {
        if ($this->isCorrectToken(REQUEST_EMAIL_TOKEN)) {
            $token = $this->checkData($_GET[REQUEST_EMAIL_TOKEN]);
            $type = REQUEST_EMAIL_TYPE;
            if ($token) {
                if ($this->model->checkRequestToken($token, $type)) {
                    if ($data = $this->model->selectDataFromUpdated($token)) {
                        $objUser = new User();
                        return $objUser->updateEmail($data['data'], $data['user_id']);
                    }
                }
            }
        }
        return false;
    }

    /**
     * Sprawdzanie zadania: zmiany hasla
     * @return bool
     */
    private function changePassword()
    {
        if ($this->isCorrectToken(REQUEST_PASSWD_TOKEN)) {
            $token = $this->checkData($_GET[REQUEST_PASSWD_TOKEN]);
            $type = REQUEST_PASSWD_TYPE;
            if ($token) {
                if ($this->model->checkRequestToken($token, $type)) {
                    if ($data = $this->model->selectDataFromUpdated($token)) {
                        $objUser = new User();
                        return $objUser->updatePassword($data['data'], $data['user_id']);
                    }
                }
            }
        }
        return false;
    }


    /**
     * Sprawdzanie zadania: rejestracji
     * @return bool
     */
    private function registration()
    {
        if ($this->isCorrectToken(REQUEST_REGISTR_TOKEN)) {
            $token = $this->checkData($_GET[REQUEST_REGISTR_TOKEN]);
            $type = REQUEST_REGISTR_TYPE;
            if ($token) {
                if ($this->model->checkRequestToken($token, $type)) {
                    if ($data = $this->model->selectDataFromUpdated($token)) {
                        $objUser = new User();
                        return $objUser->updateRegistration($data['user_id']);
                    }
                }
            }
        }
        return false;
    }

    /**
     * Sprawdzanie zadania: zapomnianego hasla
     * @return bool
     */
    private function lostPassword()
    {
        if ($this->isCorrectToken(REQUEST_LOST_PASSWD_TOKEN)) {
            $token = $this->checkData($_GET[REQUEST_LOST_PASSWD_TOKEN]);
            $type = REQUEST_LOST_PASSWD_TYPE;
            if ($token) {
                if ($this->model->checkRequestToken($token, $type)) {
                    if ($data = $this->model->selectDataFromUpdated($token)) {

                        $dataUns = unserialize($data['data']);
                        $iterations = $dataUns['iterations'];
                        $newPassword = $dataUns['passwd'];

                        $objUser = new User();
                        return $objUser->updateLostPassword($data['user_id'], $iterations, $newPassword);
                    }
                }
            }
        }
        return false;
    }


    /**
     * Dodanie zadania: zapomniane haslo
     * @param integer $userId - id uzytkownika
     * @param $type
     * @param $token
     * @param $data
     * @return mixed
     */
    public function setLostPasswordRequest($userId, $type, $token, $data)
    {
        return $this->model->passwordRequest($userId, $type, $token, $data);
    }


    /**
     * Dodanie zadania: set new password request
     * @param integer $userId - id uzytkownika
     * @param $type
     * @param $token
     * @param $newPassword
     * @return mixed
     */
    public function setNewPasswordRequest($userId, $type, $token, $newPassword)
    {
        return $this->model->passwordRequest($userId, $type, $token, $newPassword);
    }


    /**
     * Dodanie zadania: zmiana adresu email
     * @param integer $userId - id uzytkownika
     * @param $type
     * @param $token
     * @param $email
     * @return mixed
     */
    public function setNewEmailRequest($userId, $type, $token, $email)
    {
        return $this->model->setNewEmailRequest($userId, $type, $token, $email);
    }


    /**
     * Dodanie zadania: rejestracja uzytkownika
     * @param integer $userId - id uzytkownika
     * @param $type
     * @param $token
     * @return mixed
     */
    public function setNewRegistrationRequest($userId, $type, $token)
    {
        return $this->model->setNewRegistrationRequest($userId, $type, $token);
    }


}
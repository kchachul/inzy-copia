<?php

namespace System\Controller;

/**
 * Class Log
 * @package System\Controller
 */
class Log extends \System\Engine\Controller
{
    /**
     * Log constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new $this->model;
    }

    /**
     * Dodawanie nowej wartosci do loga
     * @param $type - typ
     * @param integer $userId - id uzytkownika
     * @param array $data - dane
     * @return mixed
     */
    public function add($type, $userId = 0, $data = array())
    {
        return $this->model->add($type, $userId, $data);
    }
}
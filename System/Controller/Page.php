<?php

namespace System\Controller;

/**
 * Class Page
 * @package System\Controller
 */
class Page extends \System\Engine\Controller
{
    /**
     * @var bool
     */
    private $isOnline;
    /**
     * @var integer
     */
    private $userId;

    /**
     * @var string
     */
    private $userName;

    /**
     * @var string
     */
    private $userEmail;

    /**
     * @var Session
     */
    private $sessionObiect;

    /**
     * Page constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->view = new $this->view;
        $this->model = new $this->model;

        @session_start();
        $this->sessionObiect = new Session();

        $this->isOnline = $this->sessionObiect->getStatus();
        $this->view->isOnline = $this->isOnline;

        if ($this->isOnline) {
            $this->userId = $this->sessionObiect->getUserId();
            $userObj = new User();
            $userData = $userObj->getUserData($this->userId);
            $this->userName = $userData['login'];
            $this->userEmail = $userData['email'];
            $this->view->userName = $this->userName;
            $this->view->userEmail = $this->userEmail;
        }
    }

    /**
     * Widok strony glownej
     */
    public function homepage()
    {
        $this->view->infoBox = $this->getInfoBoxText(new Request());
        $lessonObj = new Lesson;
        $this->view->lessons = $lessonObj->getAllAndScenes();
        $this->view->pageTitle = HOMEPAGE_TITLE_HEADER;
        $this->view->homepage();
    }

    /**
     * Widok strony: aktywne testy
     */
    public function activeTests()
    {
        $this->redirectOffline();
        $this->view->pageTitle = ACTIVE_TESTS_TITLE_HEADER;
        $this->view->activeTests();
    }

    /**
     * Widok strony: Utworz test
     */
    public function testCreator()
    {
        $this->redirectOffline();
        $lessonObj = new Lesson();
        $this->view->Lessons = $lessonObj->getAllFullForTeacher();

        $testObj = new Test();
        $testObj->checkIsStopped($this->userId);
        $this->view->haveAcviveTest = $testObj->existByTeacherId();
        $this->view->pageTitle = TEST_CREATOR_TITLE_HEADER;
        $this->view->testCreator();
    }

    /**
     * Widok strony: archiwum
     */
    public function archivalTests()
    {
        $testArchival = new Archival();
        $this->view->Tests = $testArchival->getAllArchivalByTeacherId($this->userId);
        $this->redirectOffline();
        $this->view->pageTitle = ARCHIVAL_TESTS_TITLE_HEADER;
        $this->view->archivalTests();
    }

    /**
     * Widok strony: baza pytan
     */
    public function questionsCollection()
    {
        $this->redirectOffline();
        $lessonObj = new Lesson();
        $this->view->Lessons = $lessonObj->getAllFullForTeacher();

        $this->view->pageTitle = QUEST_COLLECTIONS_TITLE_HEADER;
        $this->view->questionsCollection();
    }

    /**
     * Widok strony: Ustawienia konta
     */
    public function userPanel()
    {
        $this->redirectOffline();
        $this->view->pageTitle = USER_SETTINGS_TITLE_HEADER;
        $this->view->userPanel();
    }

    /**
     * Widok strony: test
     */
    public function test()
    {
        $lessonObj = new Lesson;
        $this->view->lessons = $lessonObj->getAllAndScenes();

        $studentOnj = new Student();
        $testData = $studentOnj->selvesTest();

        if ($this->view->testStatus = $testData['status']) {
            if (!$this->view->isOver = $testData['isOver']) {
                $this->view->question = $testData['question'];
                $this->view->student = $testData['student'];
                $this->view->test = $testData['test'];
            }
        }

        $this->view->pageTitle = TEST_TITLE_HEADER;
        $this->view->test();
    }

    /**
     * Widok strony: Wyniki w archiwum [plik pdf]
     */
    public function resultsPdf()
    {
        $this->redirectOffline();
        $testObj = new Test();
        $this->view->Results = $testObj->getArchivalByTestID();
        $this->view->resultsPdf();
    }


    /**
     * Widok strony: lekcje
     */
    public function lessons()
    {
        $lessonObj = new Lesson;
        $this->view->lessons = $lessonObj->getAllAndScenes();
        $this->view->pageTitle = LESSONS_TITLE_HEADER;
        $this->view->lessons();
    }

    /**
     * Widok strony: lekcja
     * @param integer $sceneId - id sceny
     */
    public function lesson($sceneId)
    {
        $this->view->lesson($sceneId);
    }


    /**
     * Widok strony 404
     */
    public function error404()
    {
        $this->view->error404();
    }


    /**
     * -------- metody pomocnicze ------------
     */


    /**
     * Przekierowanie jezeli uzytkownik nie jest zalogowany
     * @param bool $url
     */
    private function redirectOffline($url = false)
    {
        if (!$this->isOnline) {
            if ($url) {
                $this->redirect($this->generateUrl($url));
            } else {
                $this->redirect($this->generateUrl(URL_HOMEPAGE));
            }
        }
    }


    /**
     * Zwracanie tekstu do infoBoxa
     * @param Request $requestObiect
     * @return string
     */
    private function getInfoBoxText(Request $requestObiect)
    {
        if ($requestObiect->checkRequestToken('change_email')) {
            return REQUEST_EMAIL_TOKEN_CORRECT;
        } else if ($requestObiect->checkRequestToken('change_password')) {
            return REQUEST_PASSWD_TOKEN_CORRECT;
        } else if ($requestObiect->checkRequestToken('registration')) {
            return REQUEST_REGISTR_MAIL_CORRECT;
        } else if ($requestObiect->checkRequestToken('lost_password')) {
            return REQUEST_LOST_PASSWD_TOKEN_CORRECT;
        } else {
            return '';
        }
    }


}
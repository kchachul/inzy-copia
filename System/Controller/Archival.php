<?php

namespace System\Controller;
/**
 * Class Archival
 * @package System\Controller
 */
class Archival extends \System\Engine\Controller
{
    /**
     * Archival constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new $this->model;
    }

    /**
     * Dodawanie testu do archiwum
     * @param $student
     * @param $answered
     * @param integer $testId - id testu
     * @return mixed
     */
    public function add($student, $answered, $testId)
    {
        return $this->model->add($student, $answered, $testId);
    }

    /**
     * Zwracanie wszystkich archiwalnych testow
     * @param integer $testId - id testu
     * @return mixed
     */
    public function getAllByTestId($testId)
    {
        return $this->model->getAllByTestId($testId);
    }


    /**
     * Zwracanie wszystkich archiwalnych testow
     * @param integer $teacherId - id nauczyciela
     * @return mixed
     */
    public function getAllArchivalByTeacherId($teacherId)
    {
        $objTest = new Test();
        $objTest->checkIsStopped($teacherId);

        $archivalTests = $objTest->getAllAndArchivalByTeacherId($teacherId);

        $objQuestion = new Question();

        //  poprawic to
        foreach ($archivalTests as $key => $test) {

            foreach (unserialize($test['questionsIds']) as $questionId) {
                $archivalTests[$key]['questions'][] = $objQuestion->getOneWithAnswers($questionId);
            }

            $archivalTests[$key]['students'] = $this->model->getAllByTestId($test['uid']);

            foreach ($archivalTests[$key]['students'] as $key2 => $student) {
                foreach (json_decode($student['data']) as $key3 => $question) {

                    $searchedQuestion = $question->q;
                    $searchedAnswer = $question->a;
                    $questionSearch = reset(array_filter($archivalTests[$key]['questions'], function ($e) use (&$searchedQuestion) {
                        return $e['questionId'] == $searchedQuestion;
                    }));

                    $answerSearch = reset(array_filter($questionSearch['answers'], function ($e) use (&$searchedAnswer) {
                        return $e['id'] == $searchedAnswer;
                    }));


                    $archivalTests[$key]['students'][$key2]['questions'][] = array(
                        'question' => $questionSearch['question'],
                        'answer' => $answerSearch['title'],
                        'isCorrect' => $answerSearch['isCorrect']
                    );
                }
                $studentPercents = number_format(($student['points'] * 100) / $test['questionsCount'], 2, '.', '');
                $archivalTests[$key]['students'][$key2]['percents'] = $studentPercents;
                $archivalTests[$key]['students'][$key2]['gradge'] = $this->getGradge($studentPercents);
            }

        }

        return $archivalTests;
    }





}
<?php

namespace System\Controller;

/**
 * Class Scene
 * @package System\Controller
 */
class Scene extends \System\Engine\Controller
{
    /**
     * Scene constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new $this->model;
    }

    /**
     * Zwracanie scen
     * @param integer $lessonId - id lekcji
     * @return mixed
     */
    public function getAllByLessonId($lessonId)
    {
        return $this->model->getAllByLessonId($lessonId);
    }

    /**
     * Zwracanie id sceny
     * @param integer $lessonId - id lekcji
     * @return integer
     */
    public function getIdByLessonId($lessonId)
    {
        return $this->model->getIdByLessonId($lessonId);
    }

    /**
     * Dodawanie nowej sceny
     * @param string $title - tytul
     * @param string $description - opis
     * @param integer $lessonId - id lekcji
     * @return mixed
     */
    public function add($title, $description, $lessonId)
    {
        return $this->model->add($title, $description, $lessonId);
    }
}
<?php

namespace System\Controller;

class Test extends \System\Engine\Controller
{
    private $isOnline;
    private $userId;

    public function __construct()
    {
        parent::__construct();
        $this->view = new $this->view;
        $this->model = new $this->model;
        @session_start();
        $sessionObiect = new Session();

        $this->isOnline = $sessionObiect->getStatus();

        if ($this->isOnline) {
            $this->userId = $sessionObiect->getUserId();
        }
    }

    /**
     * zwraca wszystkie testy
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->getAll();
    }

    /**
     * zwraca test o ID
     * @param $id
     * @return mixed
     */
    public function getOne($id)
    {
        return $this->model->getOne($id);
    }

    /**
     * zwraca wszystkie testy po ID nauczyciela
     * @param $teacherId
     * @return mixed
     */
    public function getAllByTeacherId($teacherId)
    {
        return $this->model->getAllByTeacherId($teacherId);
    }


    /**
     * zwraca wszystkie testy (rowniez archiwalne) po ID nauczyciela
     * @param $teacherId
     * @return mixed
     */
    public function getAllAndArchivalByTeacherId($teacherId)
    {
        return $this->model->getAllAndArchivalByTeacherId($teacherId);
    }


    /**
     * zwraca wszystkie aktywne testy
     * po ID zalogowanego nauczyciela
     * @return bool
     */
    public function getAllActiveByTeacherId()
    {
        $callback = $this->defaultJSONCallback();
        $teacherId = $this->userId;

        // wyciaganie tylko 1 - moze w przyszlosci 2
        if ($activeTests = $this->model->getAllActiveByTeacherId($teacherId)) {
            $questionObj = new Question();
            $answersObj = new Answer();
            $studentObj = new Student();
            $answeredObj = new Answered();

            $activeTests[0]['nowDate'] = date('Y-m-d H:i:s');
            $activeTests[0]['students'] = $studentObj->getAllByTestId($activeTests[0]['uid']);
            $activeTests[0]['questions'] = $this->getQuestionsAndAnswers($activeTests[0]['questionsIds'], $questionObj, $answersObj);

            foreach ($activeTests[0]['students'] as $key => $student) {
                $activeTests[0]['students'][$key]['answered'] = $answeredObj->getAllAnsweredId($activeTests[0]['uid'], (int)$student['uid']);
            }

            unset($activeTests[0]['questionsIds']);
            $callback['status'] = true;
            $callback['data'] = $activeTests;
        }

        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }


    /**
     * zwraca wszystkie pytania i odpowiedzi
     * @param $questionsId
     * @param $questionObj
     * @param $answersObj
     * @return array
     */
    private function getQuestionsAndAnswers($questionsId, Question $questionObj, Answer $answersObj)
    {
        $questAndAnswers = [];

        foreach ($questionsId as $key => $questionId) {
            $questAndAnswers[$key]['id'] = $questionId;
            $questAndAnswers[$key]['data'] = $questionObj->getOneById($questionId)[0];
            $questAndAnswers[$key]['answers'] = $answersObj->getAllByQuestionId($questionId);
        }
        return $questAndAnswers;
    }


    /**
     * get one test by hash
     * @param $hash
     * @return mixed
     */
    public function getOneByHash($hash)
    {
        return $this->model->getOneByHash($hash);
    }

    /**
     * check exist - by teacher id
     * @return mixed
     */
    public function existByTeacherId()
    {
        return $this->model->existByTeacherId($this->userId);
    }


    /**
     * dodawanie nowego testu
     * @return bool
     */
    public function add()
    {
        $callback = $this->defaultJSONCallback();
        if (!$this->existByTeacherId()) {

            /*  tu musi byc jakas ujednolicona walidacja */
            if ($this->allIsset(array('shortTitle', 'shortDescription', 'testType', 'startSlots', 'timeDuration'))) {
                if ($this->testShortTitle($_POST['shortTitle']) && $this->testShortDescription($_POST['shortDescription'])) {

                    $questionsList = json_decode($_POST['questionsList']);
                    $questionsListSerialized = serialize($questionsList);

                    $test = array(
                        'shortTitle' => $_POST['shortTitle'],
                        'shortDescription' => $_POST['shortDescription'],
                        'testType' => (int)$_POST['testType'],
                        'startSlots' => $this->clearText($_POST['startSlots']),
                        'timeDuration' => $this->clearText($_POST['timeDuration']),
                        'teacherId' => $this->userId,
                        'hashName' => $this->generateUniqueHash(),
                        'questionsIds' => $questionsListSerialized,
                        'questionsCount' => count($questionsList)
                    );


                    if ($callbackStatus = $this->model->add($test)) {
                        $callback['status'] = true;
                        $callback['data'] = true;
                    }

                }
            }
        }

        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }

    /**
     * usuwanie testu
     * @return bool
     */
    public function remove()
    {
        $callback = $this->defaultJSONCallback();
        $teacherId = $this->userId;
        if ($this->allIsset(array('testId'))) {
            $testId = $this->testId($_POST['testId']);

            if ($callbackStatus = $this->model->remove($testId, $teacherId)) {
                $callback['status'] = true;
                $callback['data'] = true;
            }
        }
        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }

    /**
     * rozpoczynanie testu
     * @return bool
     */
    public function start()
    {
        $callback = $this->defaultJSONCallback();
        $teacherId = $this->userId;
        if ($this->allIsset(array('testId'))) {
            $testId = $this->testId($_POST['testId']);
            $time = $this->getTime($teacherId, $testId)['timeDuration'];

            if ($callbackStatus = $this->model->start($testId, $teacherId, $time)) {
                $callback['status'] = true;
                $callback['data'] = true;
            }
        }
        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }


    /**
     * zatrzymywanie aktywnego testu
     * @return bool
     */
    public function stop()
    {
        $callback = $this->defaultJSONCallback();
        if ($this->allIsset(array('testId'))) {
            $testId = $this->testId($_POST['testId']);
            $teacherId = $this->userId;
            $callback['status'] = $this->setStoped($testId, $teacherId);
        }
        $this->view->renderJSON(array("status" => $callback['status']));
        return false;
    }

    /**
     * zatrzymywanie testu
     * @param $testId
     * @param $teacherId
     * @return bool
     */
    private function setStoped($testId, $teacherId)
    {
        $callback = false;

        if ($this->model->stop($testId, $teacherId)) {
            $studentObj = new Student();
            $allStudents = $studentObj->getAllByTestId($testId);

            foreach ($allStudents as $key => $student) {
                $answeredObj = new Answered(); //  czy tego nie mozna bez mapowania? (np id zamiast uid)
                $answered = json_encode($answeredObj->getAllByStudentId($student['uid'], $testId));
                $archivalObj = new Archival();
                if ($archivalObj->add($student, $answered, $testId)) {
                    $studentObj->remove($student['uid']);
                }
            }
            $callback = true;
        }

        return $callback;
    }


    /**
     * zwracanie czasu testu
     * @param $teacherId
     * @param $testId
     * @return mixed
     */
    private function getTime($teacherId, $testId)
    {
        return $this->model->getTime($teacherId, $testId);
    }


    /**
     * generowanie unikalnego KODU testu
     * @return string
     */
    public function generateUniqueHash()
    {
        $hash = $this->generateHash(TEST_HASH_LENGTH);
        if (!$this->model->checkHash($hash)) {
            return $hash;
        } else {
            $this->generateUniqueHash();
        }
    }

    /**
     * aktualizowanie testu
     * @return bool
     */
    public function update()
    {
        $callback = $this->defaultJSONCallback();

        if ($this->allIsset(array('students', 'testId'))) {
            //  zwalidowac
            $studentsData = json_decode($_POST['students']);
            $testId = $this->testId($_POST['testId']);

            $lastStudentIdFromData = array_values(array_slice($studentsData, -1))[0]->studentId;
            $lastStudentId = $lastStudentIdFromData ? $lastStudentIdFromData : 0;
            $studentObj = new Student();

            foreach ($studentsData as $key => $student) {
                $answeredObj = new Answered();
                $answered = $answeredObj->getNewAnsweredId($testId, $student->studentId, $student->lastAnsweredData, false, true);
                $student->answered = count($answered) ? $answered : 0;
                unset($student->lastAnsweredData); // rmv
            }

            $newStudent = $studentObj->getNewStudents($lastStudentId, $testId);
            $callback['status'] = true;
            $callback['data'] = array('new' => $newStudent, 'old' => $studentsData);
        }

        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }


    /**
     * zwracanie archiwalnych testow ??
     * @return mixed
     */
    public function getArchivalByTestID()
    {
        $testId = $_GET[URL_TEST_PDF_GET];
        $teacherId = $this->userId;

        $archivalTests = $this->model->getOneArchivalByTestId($testId, $teacherId);
        $objArchival = new Archival();
        $objQuestion = new Question();

        //  poprawic to
        foreach ($archivalTests as $key => $test) {

            foreach (unserialize($test['questionsIds']) as $questionId) {
                $archivalTests[$key]['questions'][] = $objQuestion->getOneWithAnswers($questionId);
            }

            $archivalTests[$key]['students'] = $objArchival->getAllByTestId($test['uid']);

            foreach ($archivalTests[$key]['students'] as $key2 => $student) {
                foreach (json_decode($student['data']) as $key3 => $question) {

                    $searchedQuestion = $question->q;
                    $searchedAnswer = $question->a;
                    $questionSearch = reset(array_filter($archivalTests[$key]['questions'], function ($e) use (&$searchedQuestion) {
                        return $e['questionId'] == $searchedQuestion;
                    }));

                    $answerSearch = reset(array_filter($questionSearch['answers'], function ($e) use (&$searchedAnswer) {
                        return $e['id'] == $searchedAnswer;
                    }));


                    $archivalTests[$key]['students'][$key2]['questions'][] = array(
                        'question' => $questionSearch['question'],
                        'answer' => $answerSearch['title'],
                        'isCorrect' => $answerSearch['isCorrect']
                    );
                }
            }

        }

        return $archivalTests;
    }


    /**
     * zwracanie wszystkich zakonczonych testow
     * @param $teacherID
     * @return mixed
     */
    public function getAllEndedByTeacherId($teacherID)
    {
        return $this->model->getAllEndedByTeacherId($teacherID);
    }


    public function checkIsStopped($teacherID)
    {
        if ($ended = $this->getAllEndedByTeacherId($teacherID)) {
            foreach ($ended as $key => $test) {
                $this->setStoped($test['id'], $teacherID);
            }
        }
    }
}
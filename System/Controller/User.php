<?php

namespace System\Controller;

class User extends \System\Engine\Controller
{
    private $userId = false;
    private $logObject;

    public function __construct()
    {
        parent::__construct();
        @session_start();
        $this->view = new $this->view;
        $this->model = new $this->model;
        $sessionObj = new Session();
        $this->logObject = new Log();
        $this->userId = $sessionObj->getUserId();
    }

    /**
     * logowanie do systemu
     * @return bool
     */
    public function login()
    {
        $callback = $this->defaultJSONCallback();
        $logType = LOG_TYPE_LOGIN_FAIL;
        $logData = null;

        if ($this->allIsset(array(POST_LOGIN_USER_LOGIN, POST_LOGIN_USER_PASSWORD)) &&
            $this->validUsername($this->clearText($_POST[POST_LOGIN_USER_LOGIN]))) {
            $login = $this->clearText($_POST[POST_LOGIN_USER_LOGIN]);
            $passwd = $this->clearText($_POST[POST_LOGIN_USER_PASSWORD]);
            $logData = array('login' => $login);

            if ($iterations = $this->model->giveKeyStretchingIterations($login)[0]['key']) {
                if ($passwordKs = $this->keyStretching($passwd, $iterations)) {
                    if ($userId = $this->model->userExist($login, $passwordKs)[0]['uid']) {
                        $seesjonObj = new Session();
                        $callback['data'] = $seesjonObj->createSession($userId);
                        $callback['status'] = true;
                        $logType = LOG_TYPE_LOGIN_SUCCESS;
                    }
                }
            }
        }

        $this->logObject->add($logType, 0, $logData);
        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }

    /**
     * odzyskowanie hasla
     * @return bool
     */
    public function lostPassword()
    {
        $callback = $this->defaultJSONCallback();
        $logType = LOG_TYPE_LOST_PASSWD_FAIL;
        $logData = null;

        if ($this->allIsset(array(POST_LOST_PASSWORD_USER_EMAIL, POST_LOST_PASSWORD_USER_LOGIN)) &&
            $this->validUsername($_POST[POST_LOST_PASSWORD_USER_LOGIN]) &&
            $this->validEmail($_POST[POST_LOST_PASSWORD_USER_EMAIL])) {
            $login = $this->clearText($_POST[POST_LOST_PASSWORD_USER_LOGIN]);
            $email = $this->clearText($_POST[POST_LOST_PASSWORD_USER_EMAIL]);
            $logData = array('login' => $login, 'email' => $email);

            if ($userId = $this->model->userExistByLoginAndEmail($login, $email)) {
                $token = $this->generateHash(LOST_PASSWORD_HASH_LENGTH);
                $password = $this->generateHash(LOST_PASSWORD_NEW_PASS_LENGTH);
                $iterations = rand(10000, 999999);
                $newPassword = $this->KeyStretching($this->sameJsKeyStretching($password), $iterations);
                $type = REQUEST_LOST_PASSWD_TYPE;
                $data = serialize(array('iterations' => $iterations, 'passwd' => $newPassword));

                $requestObj = new Request();
                if ($requestObj->setLostPasswordRequest($userId, $type, $token, $data)) {
                    $url = HTTP_SERVER . URL_HOMEPAGE . '?' . REQUEST_LOST_PASSWD_TOKEN . '=' . $token;
                    $message = preg_replace(array('/{url}/', '/{password}/'), array($url, $password), REQUEST_LOST_PASSWD_INFO);

                    if ($this->sendEmail($message, REQUEST_LOST_PASSWD_TITLE, $email)) {
                        $callback['status'] = true;
                        $logType = LOG_TYPE_LOST_PASSWD_SUCCESS;
                    }
                }

            }
        }

        $this->logObject->add($logType, 0, $logData);
        $this->view->renderJSON(array("status" => $callback['status']));
        return false;
    }


    /**
     * zmiana adresu email
     * @return bool
     */
    public function changeEmail()
    {
        $callback = $this->defaultJSONCallback();
        $logType = LOG_TYPE_CHANGE_EMAIL_FAIL;
        $logData = null;

        if ($this->allIsset(array(POST_USER_EMAIL)) && $this->validEmail($_POST[POST_USER_EMAIL])) {
            $email = $this->clearText($_POST[POST_USER_EMAIL]);
            $token = $this->generateHash(CHANGE_EMAIL_HASH_LENGTH);
            $userId = $this->userId;
            $userEmail = $this->model->getUserData($userId)['email'];
            $type = REQUEST_EMAIL_TYPE;
            $logData = array('email' => $email);

            $requestObject = new Request();
            if ($requestObject->setNewEmailRequest($userId, $type, $token, $email)) {
                $url = HTTP_SERVER . URL_HOMEPAGE . '?' . REQUEST_EMAIL_TOKEN . '=' . $token;
                $message = preg_replace(array('/{url}/'), array($url), REQUEST_EMAIL_INFO);

                if ($this->sendEmail($message, REQUEST_EMAIL_TITLE, $userEmail)) {
                    $callback['status'] = true;
                    $logType = LOG_TYPE_CHANGE_EMAIL_SUCCESS;
                }
            }
        }

        $this->logObject->add($logType, $this->userId, $logData);
        $this->view->renderJSON(array("status" => $callback['status']));
        return false;
    }



    /**
     * zmiana hasla
     * @return bool
     */
    public function changePassword()
    {
        $callback = $this->defaultJSONCallback();
        $logType = LOG_TYPE_CHANGE_PASSWORD_FAIL;
        $logData = null;

        if ($this->allIsset(array(POST_USER_PASSWORD, POST_USER_NEW_PASSWORD))
            && $this->validPassword($this->clearText($_POST[POST_USER_PASSWORD]))
            && $this->validPassword($this->clearText($_POST[POST_USER_NEW_PASSWORD]))) {

            $password = $this->clearText($_POST[POST_USER_PASSWORD]);
            $newPassword = $this->clearText($_POST[POST_USER_NEW_PASSWORD]);
            $token = $this->generateHash(CHANGE_PASSWORD_HASH_LENGTH);
            $userId = $this->userId;
            $userEmail = $this->model->getUserData($userId)['email'];
            $type = REQUEST_PASSWD_TYPE;
            $logData = array('emailTo' => $userEmail);

            if ($iterations = $this->model->giveKeyStretchingIterationsByUserId($userId)) {
                if ($passwordKs = $this->keyStretching($password, $iterations)) {
                    if ($this->model->userExistById($userId, $passwordKs)) {
                        $requestObject = new Request();
                        if ($requestObject->setNewPasswordRequest($userId, $type, $token, $newPassword)) {

                            $url = HTTP_SERVER . URL_HOMEPAGE . '?' . REQUEST_PASSWD_TOKEN . '=' . $token;
                            $message = preg_replace(array('/{url}/'), array($url), REQUEST_PASSWD_INFO);

                            if ($this->sendEmail($message, REQUEST_PASSWD_TITLE, $userEmail)) {
                                $callback['status'] = true;
                                $logType = LOG_TYPE_CHANGE_PASSWORD_SUCCESS;
                            }
                        }
                    }
                }
            }
        }

        $this->logObject->add($logType, $this->userId, $logData);
        $this->view->renderJSON(array("status" => $callback['status']));
        return false;
    }


    /**
     * rejestracja
     */
    public function registration()
    {
        $callback = $this->defaultJSONCallback();
        $logType = LOG_TYPE_REGISTRATION_FAIL;
        $logData = null;

        if ($this->allIsset(array(POST_REGISTRATION_USER_LOGIN, POST_REGISTRATION_USER_EMAIL, POST_REGISTRATION_USER_PASSWORD))
            && $this->validUsername($this->clearText($_POST[POST_REGISTRATION_USER_LOGIN]))
            && $this->validPassword($this->clearText($_POST[POST_REGISTRATION_USER_PASSWORD]))) {

            $login = $this->clearText($_POST[POST_REGISTRATION_USER_LOGIN]);
            $email = $this->validEmail($this->clearText($_POST[POST_REGISTRATION_USER_EMAIL]));
            $password = $this->clearText($_POST[POST_REGISTRATION_USER_PASSWORD]);
            $iterations = rand(10000, 999999);
            $newPassword = $this->KeyStretching($password, $iterations);
            $token = $this->generateHash(REGISTRATION_HASH_LENGTH);
            $logData = array('login' => $login, 'email' => $email);


            if ($this->model->loginExist($login)) {
                $callback['data'] = $logData['callback'] = LOG_USERNAME_EXIST;
            } else if ($this->model->emailExist($email)) {
                $callback['data'] = $logData['callback'] = LOG_EMAIL_EXIST;
            } else {
                if ($userId = $this->model->add($login, $newPassword, $email, $iterations, $token)) {
                    $type = REQUEST_REGISTR_TYPE;
                    $requestObject = new Request();
                    $callback['data'] = LOG_REGISTRATION_FAIL;

                    if ($requestObject->setNewRegistrationRequest($userId, $type, $token)) {
                        $url = HTTP_SERVER . URL_HOMEPAGE . '?' . REQUEST_REGISTR_TOKEN . '=' . $token;
                        $message = preg_replace(array('/{url}/'), array($url), REQUEST_REGISTR_INFO);

                        if ($this->sendEmail($message, REQUEST_REGISTR_TITLE, $email)) {
                            $callback['status'] = true;
                            $callback['data'] = REGISTRATION_SUCCESS_TEXT;
                            $logType = LOG_TYPE_REGISTRATION_SUCCESS;
                        } else {
                            $callback['data'] = LOG_EMAIL_FAIL;
                        }
                    }
                }
            }

        }

        $this->logObject->add($logType, 0, $logData);
        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }

    /**
     * wylogowywanie
     */
    public function logout()
    {
        $this->_isOnline = false;
        session_unset();
        session_destroy();
        $this->logObject->add(LOG_TYPE_USER_LOGOUT, $this->userId, null);
        $this->redirect($this->generateUrl(URL_HOMEPAGE));
    }


    /**
     * @param $text
     * @param $iterations
     * @return string
     */
    private function keyStretching($text, $iterations)
    {
        for ($i = 0; $i < $iterations; $i++) {
            $text = hash('sha512', $text);
        }
        return $text;
    }


    /**
     * @param $userId
     * @return mixed
     */
    public function getUserData($userId)
    {
        return $this->model->getUserData($userId);
    }


    /**
     * @param $email
     * @param $userID
     * @return mixed
     */
    public function updateEmail($email, $userID)
    {
        return $this->model->updateEmail($email, $userID);
    }


    /**
     * @param $userID
     * @return mixed
     */
    public function updateRegistration($userID)
    {
        return $this->model->updateRegistration($userID);
    }


    /**
     * @param $password
     * @param $userId
     * @return mixed
     */
    public function updatePassword($password, $userId)
    {
        if ($iterations = $this->model->giveKeyStretchingIterationsByUserId($userId)) {
            if ($passwordKs = $this->keyStretching($password, $iterations)) {
                return $this->model->updatePassword($passwordKs, $userId);
            }
        }
    }

    /**
     * @param $userId
     * @param $iterations
     * @param $password
     * @return mixed
     */
    public function updateLostPassword($userId, $iterations, $password)
    {
        return $this->model->updateLostPassword($userId, $iterations, $password);
    }

}
<?php

namespace System\Controller;

/**
 * Class Session
 * @package System\Controller
 */
class Session extends \System\Engine\Controller
{

    /**
     * @var bool
     */
    private $_isOnline = false;

    /**
     * Session constructor.
     */
    public function __construct()
    {
        parent::__construct();
        @session_start();
        $this->_isOnline = $this->checkStatus();
    }

    /**
     * Sprawdzanie czy uzytkownik jest zalogowany
     * @return bool
     */
    private function checkStatus()
    {
        if ($this->checkCookie() && $this->checkSession()) {
            return true;
        }
        return false;
    }


    /**
     * Sprawdzanie czy Cookie istnieje
     */
    private function checkCookie()
    {
        if (isset($_COOKIE[COOKIE_NAME])) {
            return true;
        }
        return false;
    }

    /**
     * Sprawdzanie czy sesja istnieje
     */
    private function checkSession()
    {
        if (isset($_SESSION[SESSION_NAME]) && ($_SESSION[SESSION_NAME_IP] == $_SERVER['REMOTE_ADDR'])) {
            return true;
        }
        return false;
    }

    /**
     * Tworzenie nowej sesji dla zalogowanego uzytkownika
     * @param $userId
     * @return bool
     */
    public function createSession($userId)
    {
        setcookie(COOKIE_NAME, true, SESSION_TIME_LIFE, '/');
        $_SESSION[SESSION_NAME] = true;
        $_SESSION[SESSION_ID] = $userId;
        $_SESSION[SESSION_NAME_IP] = $_SERVER['REMOTE_ADDR'];
        $this->_isOnline = true;
        return true;
    }


    /**
     * Zwracanie statusu uzytkownika
     * @return bool - zalogowany / niezalogowany
     */
    public function getStatus()
    {
        return $this->_isOnline;
    }

    /**
     * Zwracanie Id zalogowanego uzytkownika
     * @return mixed
     */
    public function getUserId()
    {
        return $_SESSION[SESSION_ID];
    }


}
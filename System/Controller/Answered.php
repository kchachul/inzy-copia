<?php

namespace System\Controller;

use System\Engine\Helpers\MainHelper;

/**
 * Class Answered
 * @package System\Controller
 */
class Answered extends \System\Engine\Controller
{
    /**
     * Answered constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->view = new $this->view;
        $this->model = new $this->model;
    }

    /**
     * Zwracanie paczki pytan z kolejki
     * @param integer $testId - id testu
     * @param integer $studentId - id studenta
     * @param null $date - data ostatniej odpowiedzi, beda wyciagane tylko nowsze
     * @param bool $onlyOne - true gdy wyciagamy tylko jedna
     * @return mixed
     */
    public function getAllAnsweredId($testId, $studentId, $date = null, $onlyOne = false)
    {
        return $this->model->getAllAnsweredId($testId, $studentId, $date, $onlyOne);
    }


    /**
     * Zwracanie paczki pytan z kolejki na ktore udzielono odpowiedzi
     * @param integer $testId - id testu
     * @return mixed
     */
    public function getAllByTestId($testId)
    {
        return $this->model->getAllByTestId($testId);
    }

    /**
     * Zwracanie paczki pytan z kolejki
     * @param integer $studentId - id studenta
     * @param integer $testId - id testu
     * @return mixed
     */
    public function getAllByStudentId($studentId, $testId)
    {
        return $this->model->getAllByStudentId($studentId, $testId);
    }


    /**
     * Zwracanie paczki pytan
     * @param integer $testId - id testu
     * @param integer $studentId - id studenta
     * @param $date - data ostatniej odpowiedzi, beda wyciagane tylko nowsze
     * @param $onlyOne - true gdy wyciagamy tylko jedna
     * @param $onlyAnswered - true gdy tylko te na ktore udzielono juz odpowiedzi
     * @return mixed
     */
    public function getNewAnsweredId($testId, $studentId, $date, $onlyOne, $onlyAnswered)
    {
        return $this->model->getAllAnsweredId($testId, $studentId, $date, $onlyOne, $onlyAnswered);
    }


    /**
     * Dodawanie nowego pytanai do klejki
     * @param integer $studentId - id studenta
     * @param integer $testId - id testu
     * @param integer $questionId - id pytania
     * @return mixed
     */
    public function addNew($studentId, $testId, $questionId)
    {
        return $this->model->addNew($studentId, $testId, $questionId);
    }

    /**
     * Aktualizacja odpowiedzi w pytaniu z kolejki
     * @return bool - czy udalo sie zaktualizowac
     */
    public function update()
    {
        $callback = $this->defaultJSONCallback();
        $studentObj = new Student();
        $studentData = $studentObj->getStudentDataFromCookies();
        $studentId = $studentObj->getStudentIdFromHash($studentData['studentId'])[0]['uid'];

        if ($studentObj->exist($studentData['testId'], $studentId)) {
            $answerId = $this->answerId($_POST['answerId']);
            $questionId = $this->questionId($_POST['questionId']);
            $answerObj = new Answer();
            $isCorrect = $answerObj->isCorrect($answerId);

            if ($this->model->update($answerId, $studentId, $studentData['testId'], $questionId, $isCorrect)) {
                $callback['status'] = true;
            }
        }

        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }

    /**
     * Zwracanie jednego pytania z kolejki
     * Generuje format JSON
     * @return bool
     */
    public function getOneJSON()
    {
        $callback = $this->defaultJSONCallback();

        if ($questionData = $this->getOne()) {
            $callback['status'] = true;
            $callback['data'] = $questionData;
        }
        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }


    /**
     * Zwracanie jednego pytania z kolejki
     * // JOIN REQUEST ??
     * @return array|bool
     */
    public function getOne()
    {
        $studentObj = new Student();
        if ($studentData = $studentObj->getStudentDataFromCookies()) {

            $objQuestion = new Question();
            $objAnswer = new Answer();
            $testId = $this->testId($studentData['testId']);
            $studentId = $this->studentId($studentObj->getStudentIdFromHash($studentData['studentId'])[0]['uid']);

            $question = $this->getAllAnsweredId($testId, $studentId, null, true);
            $questionData = $objQuestion->getOneById($this->questionId($question[0]['questionId']));
            $answers = $objAnswer->getAllByQuestionId($questionData[0]['uid']);
            $clearAnswers = $this->getAnswersCollection($answers);

            if (count($clearAnswers) > 0) {
                return array(
                    'title' => $questionData[0]['title'],
                    'id' => $this->questionId($questionData[0]['uid']),
                    'answers' => MainHelper::shuffleAssoc($clearAnswers),
                    'image' => $questionData[0]['image'] ? $questionData[0]['image'] : false);
            } else {
                return array('status' => true, 'data' => array('isOver' => true));
            }
        }
        return false;
    }


    /**
     * Zwracane pary [ id - pytanie ]
     * @param array $answers - tablica odpowiedzi
     * @return array
     */
    private function getAnswersCollection($answers)
    {
        $clearAnswers = [];
        foreach ($answers as &$answer) {
            $clearAnswers[] = array('id' => $this->answerId($answer['uid']), 'title' => $answer['title']);
        }
        return $clearAnswers;
    }

}
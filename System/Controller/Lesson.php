<?php

namespace System\Controller;

class Lesson extends \System\Engine\Controller
{
    private $userId;

    public function __construct()
    {
        parent::__construct();
        $this->view = new $this->view;
        $this->model = new $this->model;
        @session_start();
        $sessionObiect = new Session();
        $this->userId = $sessionObiect->getUserId();
    }

    /**
     * get all lessons and scenes
     * @return mixed
     */
    public function getAllAndScenes()
    {
        $allLessons = $this->model->getAll();
        $sceneObj = new Scene();

        foreach ($allLessons as $key => $lesson) {
            $allLessons[$key]['scenes'] = $sceneObj->getAllByLessonId($lesson['id']);
        }
        return $allLessons;
    }


    /**
     * get all lessons for teacher
     * @return mixed
     */
    public function getAllFullForTeacher()
    {
        $teacherId = $this->userId;
        $allLessons = $this->getDefaultsAndTeacher($teacherId);

        foreach ($allLessons as $key => $lesson) {
            $sceneObj = new Scene();
            $allLessons[$key]['scenes'] = $sceneObj->getAllByLessonId($lesson['id']);

            foreach ($allLessons[$key]['scenes'] as $key2 => $scene) {
                $questionObj = new Question();
                $allLessons[$key]['scenes'][$key2]['questions'] = $questionObj->getAllBySceneId($scene['id']);

                // answers
                foreach ($allLessons[$key]['scenes'][$key2]['questions'] as $key3 => $question) {
                    $answerObj = new Answer();
                    $allLessons[$key]['scenes'][$key2]['questions'][$key3]['answers'] = $answerObj->getAllByQuestionId($question['id']);
                }
            }
        }


        return $allLessons;
    }

    /**
     * add new lesson
     * @return bool
     */
    public function add()
    {
        $callback = $this->defaultJSONCallback();
        $teacherId = $this->userId;

        if ($this->allIsset(array('title', 'description')) &&
            $this->LessonTitle($_POST['title']) &&
            $this->LessonDescription($_POST['description'])) {

            $title = $_POST['title'];
            $description = $_POST['description'];

            if ($callbackData = $this->model->add($title, $description, $teacherId)) {
                $sceneObj = new Scene();

                if ($sceneData = $sceneObj->add('', '', $callbackData['id'])) {
                    $callback['status'] = true;
                    $callback['data'] = array('lessonId' => $callbackData['id'], 'sceneId' => $sceneData['id']);
                }
            }
        }

        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }


    /**
     * remove
     * @return bool
     */
    public function remove()
    {
        $callback = $this->defaultJSONCallback();
        $teacherId = $this->userId;

        if ($this->allIsset(array('lessonId'))) {
            $lessonId = $this->lessonId($_POST['lessonId']);

            if ($callbackData = $this->model->remove($lessonId, $teacherId)) {
                $callback['status'] = true;
                $callback['data'] = $callbackData['id'];
            }
        }

        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }


    /**
     * get all teacher lessons + defaults
     *
     * @param $teacherId
     * @return mixed
     */
    public function getDefaultsAndTeacher($teacherId)
    {
        return $this->model->getDefaultsAndTeacher($teacherId);
    }
}
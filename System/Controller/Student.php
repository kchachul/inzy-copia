<?php

namespace System\Controller;

class Student extends \System\Engine\Controller
{
    private $isOnline;

    public function __construct()
    {
        parent::__construct();
        $this->view = new $this->view;
        $this->model = new $this->model;
        @session_start();
        $sessionObiect = new Session();
        $this->isOnline = $sessionObiect->getStatus();
    }


    /**
     * zwracanie wszystkich uczniow
     * @param $testId
     * @return mixed
     */
    public function getAllByTestId($testId)
    {
        return $this->model->getAllByTestId($testId);
    }


    /**
     * usuwanie aktywnego ucznia
     * @param $studentId
     * @return mixed
     */
    public function remove($studentId)
    {
        if ($this->isOnline) {
            return $this->model->remove($studentId);
        } else {
            return false;
        }
    }

    /**
     * usuwanie aktywnego ucznia
     * @param $studentId
     * @return mixed
     */
    public function removeActive()
    {
        $callback = $this->defaultJSONCallback();
        if ($this->allIsset(array('studentId'))) {
            $studentId = $this->studentId($_POST['studentId']);

            $callback['status'] = $this->model->remove($studentId);
        }
        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
    }


    /**
     * dodawanie nowego ucznia
     * @return bool
     */
    public function add()
    {
        $callback = $this->defaultJSONCallback();

        if ($this->allIsset(array('hash', 'studentName'))) {
            $hash = $_POST['hash']; // zwalidowac
            $name = $_POST['studentName']; // zwalidowac
            $testObj = new Test();

            if ($testData = $testObj->getOneByHash($hash)[0]) {
                // check user exist -  with the same name
                if (!$this->nameExist($name, $testData['uid'])) {
                    $userHash = $this->generateHash(STUDENT_TEST_HASH_LENGTH);

                    if ($studentId = $this->model->add($name, $testData['uid'], $userHash)) {
                        $questionsIds = unserialize($testData['questionsIds']);
                        shuffle($questionsIds);

                        $answeredObj = new Answered();
                        $added = false;
                        foreach ($questionsIds as $questionId) {
                            $added = $answeredObj->addNew($studentId, $testData['uid'], $questionId);
                        }

                        if ($added) {
                            $callback['status'] = true;
                            $this->setStudentCookie($name, $userHash, $testData['uid']);
                        }

                    }
                } else {
                    $callback['data'] = 'Taki użytkownik już istnieje!';
                }
            }
        }

        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }


    /**
     * sprawdzanie czy uczen o takim imieniu istnieje ??
     * @param $studentName
     * @param $testId
     * @return mixed
     */
    private function nameExist($studentName, $testId)
    {
        return $this->model->nameExist($studentName, $testId);
    }


    public function selvesTest()
    {
        $objAnswered = new Answered();
        if ($questionData = $objAnswered->getOne()) {

            if (!$questionData['data']['isOver']) {
                $testObj = new Test(); // poprawic to, co jak ktos podmieni cookie
                $testData = $testObj->getOne($this->getTestIDFromCookie());
                $studentData = $this->getDataByHash($this->getStudentIdFromCookie())[0];


                $test = array(
                    'title' => $testData[0]['shortTitle'],
                    'description' => $testData[0]['shortDescription'],
                    'answered_count' => $studentData['answered'] + 1,
                    'count' => $testData[0]['questionsCount'],
                    'end_date' => $testData[0]['endDate'],
                    'now_date' => date('Y-m-d H:i:s'),
                );

                if ($testData[0]['testType']) {
                    $test['type'] = $testData[0]['testType'];
                }

                $student = array(// poprawic to, co jak ktos podmieni cookie
                    'name' => $studentData['name']
                );


                return array(
                    'question' => $questionData,
                    'status' => true,
                    'test' => $test,
                    'student' => $student
                );
            } else {
                return array(
                    'status' => true,
                    'isOver' => true
                );
            }

        }
        return array('status' => false);
    }


    /**
     * check - user exist
     * @param $testId
     * @param $studentId
     * @param $studentHash
     * @return mixed
     */
    public function exist($testId, $studentId)
    {
        return $this->model->exist($testId, $studentId);
    }


    /**
     * get student data
     *
     * @param $studentId
     * @return mixed
     */
    public function getData($studentId)
    {
        return $this->model->getData($studentId);
    }


    /**
     * get student data by hash
     *
     * @param $studentHash
     * @return mixed
     */
    public function getDataByHash($studentHash)
    {
        return $this->model->getDataByHash($studentHash);
    }


    /**
     * get student data from cookies
     * @return array|bool
     */
    public function getStudentDataFromCookies()
    {
        if (isset($_COOKIE[COOKIE_TEST_ID]) && isset($_COOKIE[COOKIE_STUDENT_ID]) && isset($_COOKIE[COOKIE_STUDENT_TEST])) {
            //  valid
            $testId = $_COOKIE[COOKIE_TEST_ID];
            $studentId = $_COOKIE[COOKIE_STUDENT_ID];
            $studentName = $_COOKIE[COOKIE_STUDENT_TEST];
            $studentHash = $this->generateUserHash($studentName);

            if ($testId && $studentId && $studentName && $studentHash) {
                return array(
                    'testId' => $testId,
                    'studentId' => $studentId,
                    'studentName' => $studentName,
                    'studentHash' => $studentHash,
                );
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * get student name from cookie
     * @return bool
     */
    public function getStudentNameFromCookie()
    {
        if (isset($_COOKIE[COOKIE_STUDENT_TEST])) {
            return $_COOKIE[COOKIE_STUDENT_TEST];
        } else {
            return false;
        }
    }

    /**
     * get student id from cookie
     * @return bool
     */
    public function getStudentIdFromCookie()
    {
        if (isset($_COOKIE[COOKIE_STUDENT_ID])) {
            return $_COOKIE[COOKIE_STUDENT_ID];
        } else {
            return false;
        }
    }

    /**
     * get test id from cookie
     * @return bool
     */
    public function getTestIDFromCookie()
    {
        if (isset($_COOKIE[COOKIE_TEST_ID])) {
            return $_COOKIE[COOKIE_TEST_ID];
        } else {
            return false;
        }
    }


    /**
     * clear cookies
     * @return bool
     */
    public function clearData()
    {
        $callback = false;
        if ($studentData = $this->getStudentDataFromCookies()) {
            if (isset($_SERVER['HTTP_COOKIE'])) {
                $cookies = array(COOKIE_TEST_ID, COOKIE_STUDENT_ID, COOKIE_STUDENT_TEST);

                foreach ($cookies as $name) {
                    setcookie($name, '', time() - 1000);
                    setcookie($name, '', time() - 1000, '/');
                }

                $callback = true;
            }
        }

        $this->view->renderJSON(array("status" => $callback));
        return false;
    }


    /**
     * get new students
     * @param int $lastStudentId
     * @param $testId
     * @return mixed
     */
    public function getNewStudents($lastStudentId = 0, $testId)
    {
        return $this->model->getNewStudents($lastStudentId, $testId);
    }


    public function getStudentIdFromHash($studentHash)
    {
        return $this->model->getStudentIdFromHash($studentHash);
    }

    /**
     * set cookie
     * @param $name
     * @param $studentId
     * @param $testId
     */
    private function setStudentCookie($name, $studentId, $testId)
    {
        setcookie(COOKIE_STUDENT_TEST, $name, COOKIE_TEST_LIFE);
        setcookie(COOKIE_STUDENT_ID, $studentId, COOKIE_TEST_LIFE);
        setcookie(COOKIE_TEST_ID, $testId, COOKIE_TEST_LIFE);
    }

}
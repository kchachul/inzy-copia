<?php

namespace System\Controller;

/**
 * Class Answer
 * @package System\Controller
 */
class Answer extends \System\Engine\Controller
{
    /**
     * Answer constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new $this->model;
    }

    /**
     * Zwracanie wszystkich odpowiedzi
     * @param integer $questionId - id pytania
     * @return mixed
     */
    public function getAllByQuestionId($questionId)
    {
        return $this->model->getAllByQuestionId($questionId);
    }

    /**
     * Dodawanie odpowiedzi do pytania
     * @param array $answers - tablica z odpowiedziami
     * @param integer $questionId - id pytania
     * @return bool - czy udalo sie dodac
     */
    public function add($answers, $questionId)
    {
        return $this->model->add($answers[0], $questionId)
            && $this->model->add($answers[1], $questionId)
            && $this->model->add($answers[2], $questionId);
    }

    /**
     * Usuwanie odpowiedzi
     * @param integer $answerId - id odpowiedzi
     * @return bool - czy udalo sie usunac
     */
    public function remove($answerId)
    {
        return $this->model->remove($answerId);
    }

    /**
     * Sprawdzanie czy odpowiedz jest poprawna
     * @param integer $answerId
     * @return bool
     */
    public function isCorrect($answerId)
    {
        return $this->model->isCorrect($answerId);
    }

}



<?php

namespace System\Controller;

class Question extends \System\Engine\Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->view = new $this->view;
        $this->model = new $this->model;
    }

    /**
     * zwracanie wszystkich pytan
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->getAll();
    }

    /**
     * zwracanie wszystkich pytan po ID testu
     * @return mixed
     */
    public function getAllByTestId()
    {
        $testId = 1;
        return $this->model->getAllByTestId($testId);
    }


    /**
     * zwracanie wszystkich pytan po ID sceny
     * @param $sceneId
     * @return mixed
     */
    public function getAllBySceneId($sceneId)
    {
        return $this->model->getAllBySceneId($sceneId);
    }

    /**
     * zwracanie jednego pytania po ID
     * @param $questionId
     * @return mixed
     */
    public function getOneById($questionId)
    {
        return $this->model->getOneById($questionId);
    }

    /**
     * zwracanie pytania i odpowiedzi po ID pytania
     * @param $questionId
     * @return array
     */
    public function getOneWithAnswers($questionId)
    {
        $question = $this->model->getOneWithAnswers($questionId);
        $data = array(
            'question' => $question[0]['title'],
            'questionId' => $questionId,
            'answers' => array(
                array('title' => $question[0]['answer'], 'isCorrect' => $question[0]['is_correct'], 'id' => $question[0]['answerId']),
                array('title' => $question[1]['answer'], 'isCorrect' => $question[1]['is_correct'], 'id' => $question[1]['answerId']),
                array('title' => $question[2]['answer'], 'isCorrect' => $question[2]['is_correct'], 'id' => $question[2]['answerId']),
            )

        );

        return $data;
    }

    /**
     * dodawanie nowego pytania
     * @return bool
     */
    public function add()
    {
        $callback = $this->defaultJSONCallback();
        $lessonId = $this->lessonId($_POST['lessonId']);
        // zwalidaowac
        $question = array('title' => $_POST['title'], 'image' => false, 'test_id' => 0);
        $sceneObj = new Scene();

        if ($sceneData = $sceneObj->getIdByLessonId($lessonId)) {
            $question['scene_id'] = (int)$sceneData['uid'];

            if ($questionData = $this->model->add($question)) {
                $answerObj = new Answer();
                // zwalidowac
                $answers = json_decode($_POST['answers'], true);

                if ($answerObj->add($answers, $questionData['id'])) {
                    $callback['status'] = true;
                    $callback['data'] = $questionData;
                }
            }
        }

        $this->view->renderJSON(array("status" => $callback['status'], "data" => $callback['data']));
        return false;
    }

    /**
     * usuwanie pytania
     * @return bool
     */
    public function remove()
    {
        $questionId = $this->questionId($_POST['id']);
        $callback = $this->defaultJSONCallback();

        if ($this->model->remove($questionId)) {
            $callback['status'] = true;
        }
        $this->view->renderJSON(array("status" => $callback['status'], "data" => array('questionId' => (int)$questionId)));
        return false;
    }


}
const userPanel = (function ($, window, document) {

  init = function () {
    $('[data-toggle="popover"]').popover()
    changeEmialListener()
    changePasswordListener()
    ValidatorHelper.changeEmail()
    ValidatorHelper.changePassword()
  }

  /**
   * click change email
   */
  changeEmialListener = function () {
    $('#changeEmail').click(function (e) {
      e.preventDefault()

      if ($('#changeEmailForm').valid()) {
        const newEmail = $('#changeEmailForm input[name=addressemail]').val()
        const user = new User()
        $(this).addClass('disabled').attr('disabled', true)
        user.changeEmail(newEmail, $(this))
      }
    })
  }

  /**
   * click change password
   */
  changePasswordListener = function () {
    $('#changePassword').click(function (e) {
      e.preventDefault()

      if ($('#changePasswordForm').valid()) {
        const password = $('#changePasswordForm input[name=passwd]').val()
        const newPassword = $('#changePasswordForm input[name=new_passwd]').val()
        const passwordKS = Helper.keyStretching(password, Helper.getKSIterations())
        const newPasswordKS = Helper.keyStretching(newPassword, Helper.getKSIterations())

        $(this).addClass('disabled').attr('disabled', true)
        const user = new User()
        user.changePassword(passwordKS, newPasswordKS, $(this))
      }
    })
  }

  return {
    init: init
  }


})(jQuery, window, document)
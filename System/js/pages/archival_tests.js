const archivalTests = (function ($, window, document) {

  const pageData = Helper.pagesData().archival
  const archivalTest = new Archival()
  let testToRmv = null

  init = function () {
    studentsResultsListener()
    removeTestListener()
    studentDetail()
  }

  studentsResultsListener = function () {
    $('.test_container .head').click(function () {
      const closestCnt = $(this).closest('.test_container')
      const userAnswersContainer = closestCnt.find('.body')

      if (closestCnt.hasClass('active')) {
        closestCnt.removeClass('active')
        userAnswersContainer.slideUp(300)
      } else {
        closestCnt.addClass('active')
        userAnswersContainer.slideDown(300)
      }
    })
  }
  removeTestListener = function () {
    $('.btn_delete_test').click(function () {
      testToRmv = $(this).data('test_id')
      Helper.confirmModal().showModal(pageData.message.rmvTest)
    })

    $('#confirmModal .confirm_btn').on('click', function () {
      removeArchival()
    })
  }

  removeArchival = function () {
    if(testToRmv !== null) {
      const testId = testToRmv
      archivalTest.init(testId)
      archivalTest.remove()
      testToRmv = null
    }
  }


  studentDetail = function(){
    $('.row_table.user').click(function () {
      $(this).find('.student_answers').slideToggle(400)
    })
  }

  return {
    init: init
  }

})(jQuery, window, document)
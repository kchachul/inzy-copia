const testPage = (function ($, window, document) {

  const testPage = new TestPage()
  let testContainer = undefined
  let startContainer = undefined
  let testBoxContainer = undefined

  init = function () {
    testContainer = $('#testPageContainer')
    startContainer = $('.hashBox')
    btnNextQuestion = $('#next_question')
    testEndedContainer = $('#testEnded')
    Helper.setBodyWH()
    selectView()
    leftMenuListener()
    setClickListener()
  }

  /**
   * select view
   * start || question
   */
  selectView = function () {
    if (startContainer.length) {
      addStudentListener()
      ValidatorHelper.addStudent()
    } else if (testEndedContainer.hasClass('active')) {
      testPage.clearTimer()
      finishTestListener()
    } else if (testContainer.hasClass('active')) {
      $('#testBox').fadeIn(400)
      setTestData()
      questionListeners()
    }
  }

  /**
   * add answer listener
   */
  questionListeners = function () {
    checkQuestion()
    sendAnswerListener()
  }

  /**
   * add new student
   * listener
   */
  addStudentListener = function () {
    $('#select_test_btn').click(function (e) {
      e.preventDefault()
      if ($('#addStudent').valid()) {
        const testCode = $('#testPageContainer input[name=hash]').val()
        const studentName = $('#testPageContainer input[name=student_name]').val()
        testPage.addStudent(testCode, studentName, studentAdded)
      }
    })
  }

  /**
   * draw test box
   */
  drawTestBox = function () {
    testContainer.prepend(testPage.questionView())
    testBoxContainer = $('#testBox')
    testBoxContainer.fadeIn(400)
  }

  /**
   * draw answers //  sprawdzic czy dziala
   * @param answers
   * @param questionId
   */
  drawAnswers = function (answers, questionId) {
    const answersContainer = $('#answers')
    answersContainer.empty()
    answersContainer.data('question-id', questionId)
    answersContainer.html(testPage.getAnswersList(answers))
  }

  /**
   * update test data
   */
  setTestData = function () {
    let timeToEvent = Helper.datatimeToJsDate($('#time').data('timetoevent'))
    let nowTime = Helper.datatimeToJsDate($('#time').data('nowtime'))
    testPage.setTimer(nowTime, timeToEvent)
  }

  /**
   * draw question
   * @param question
   */
  drawQuestion = function (questionTitle) {
    $('#question').empty()
    $('#question').html(questionTitle)
  }

  /**
   * set question image
   * @param questionId
   * @param questionImage
   */
  setImage = function (questionId, questionImage) {
    if (questionImage) {
      //  zamienic na link i na img z db
      $('#image_answer img').attr('src', Helper.getTemplateUrl() + 'pages/test/questions/' + questionId + '.jpg')
      $('.image_question').show()
    } else {
      $('.image_question').hide()
    }
  }

  /**
   * question loaded callback
   * @param questionData
   */
  questionLoaded = function (questionData) {
    try {
      if (questionData.data.isOver) {
        $('.test_view').fadeOut(300, function () {
          $('#testEnded').addClass('active')
          testPage.clearTimer()
          finishTestListener()
        })
      }
    } catch (e) {
      $('html, body').animate({scrollTop: 0}, 200)
      drawAnswers(questionData.answers, questionData.id)
      drawQuestion(questionData.title)
      setImage(questionData.id, questionData.image)
      setAnsweredCounter()
      setProgressbarWidth()
      nextQuestionBtn(false)
      showHideLoader(false)
    }
  }

  /**
   * load new question - fail
   */
  questionLoadedFail = function () {
    showHideLoader(false)
  }

  /**
   * student added callback
   */
  studentAdded = function () {
    location.reload()
  }

  /**
   * check question = add active class
   */
  checkQuestion = function () {
    $('#answers').on('click', 'li', function () {
      $('#answers li').removeClass('active')
      $(this).addClass('active')
      nextQuestionBtn(true)
    })
  }

  sendAnswerListener = function () {
    btnNextQuestion.click(function () {
      showHideLoader(true)
      const answerId = $('#answers li.active').data('answer-id')
      const questionId = $('#answers').data('question-id')
      testPage.sendAnswer(answerId, questionId, answerAdded, answerAddedFail)
    })
  }

  /**
   * answer added  - udzielona odpowiedz
   */
  answerAdded = function () {
    testPage.loadNewQuestion(questionLoaded, questionLoadedFail)
  }

  /**
   * answer not added = nie udzielono odpowiedzi
   */
  answerAddedFail = function () {
    showHideLoader(false)
  }

  /**
   * show/hide loader
   * @param show
   */
  showHideLoader = function (show) {
    const loader = $('#bg_testBox')
    if (show) {
      loader.show()
    } else {
      loader.hide()
    }
  }

  /**
   * set answer counter - bottom footer
   */
  setAnsweredCounter = function () {
    const answeredCount = $('.answeredCount')
    let answered = answeredCount.html() * 1 + 1
    answeredCount.html(answered)
  }

  /**
   * set progressbar width - bottom footer
   */
  setProgressbarWidth = function () {
    const answeredCount = $('.answeredCount').html() * 1
    const questionsCount = $('.questionsCount').html() * 1
    $('#progressbar').css('width', ((answeredCount * 100) / questionsCount) + '%')
  }

  /**
   * next question btn - styles
   * @param setActive
   */
  nextQuestionBtn = function (setActive) {
    if (setActive) {
      //  slowly
      setTimeout(function () {
        btnNextQuestion.attr('disabled', false).removeClass('disabled')
      }, 200)
    } else {
      btnNextQuestion.attr('disabled', true).addClass('disabled')
    }
  }

  /**
   * click finishTest = rmv cookies
   */
  finishTestListener = function () {
    $('#finishTest').click(function () {
      testPage.clearStudentData(Helper.reloadPage) //  sprawdzic czy to dziala
    })
  }

  /**
   * show/hide left menu
   */
  leftMenuListener = function () {
    $('#moreInfo').click(function () {
      $('#greyBg').show(0, function () {
        $('#leftMenu').animate({
          left: 0
        }, 300)
      })
    })

    $('#greyBg').click(function () {
      $(this).hide()
      $('#leftMenu').animate({
        left: '-' + $('#leftMenu').width() + 'px'
      }, 300)
    })

    $('button.lesson').click(function () {
      $(this).closest('li').children('ul').slideToggle(200)
    })

    $('.scenes > button').click(function () {
      $(this).closest('li').children('ul').slideToggle(200)
    })
  }

  /**
   * test navigation = keypress listeners
   */
  setClickListener = function () {
    const answers = $('#answers')
    $(document).keypress(function (e) {
      if (e.which == 49 || e.which == 97) {
        answers.find('li').eq(0).trigger('click')
      } else if (e.which == 50 || e.which == 98) {
        answers.find('li').eq(1).trigger('click')
      } else if (e.which == 51 || e.which == 99) {
        answers.find('li').eq(2).trigger('click')
      } else if (e.which == 13) {
        if (!$('#next_question').is(':disabled')) {
          $('#next_question').trigger('click')
        }
      }
    })
  }

  return {
    init: init
  }

})(jQuery, window, document)
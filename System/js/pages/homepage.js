/**
 * homepage view
 * @type {{init}}
 */
const homepage = (function ($, window, document) {

  /**
   * start
   */
  init = function () {
    loginListener()
    lostPasswordListener()
    clickRegistration()
    showRegisterLoginBox()
    carouselHome()
    ValidatorHelper.login()
    ValidatorHelper.registration()
    ValidatorHelper.lostPassword()
  }

  /**
   * login - button listener
   */
  loginListener = function () {
    $('#loginBtn').click(function (e) {
      e.preventDefault()

      if ($('#loginform').valid()) {
        $(this).attr('disabled', true).addClass('disabled')
        const system = new System()
        system.login($('#loginform'))
      }
    })
  }

  /**
   * lost password - button listener
   */
  lostPasswordListener = function () {
    $('#passwdRecovery').click(function (e) {
      e.preventDefault()
      if ($('#passwdRecoveryForm').valid()) {
        $('#passwdRecovery').addClass('disabled')
        const system = new System()
        system.lostPassword($('#modalLostPassword'), callbackSuccess, callbackFail)
      }
    })
  }

  /**
   * registration - button listener
   */
  clickRegistration = function () {
    $('#registration_btn').click(function (e) {
      e.preventDefault()
      if ($('#registr').valid()) {
        $(this).attr('disabled', true).addClass('disabled')
        const system = new System()
        system.registration($('#registr'), callbackRegistration)
      }
    })
  }

  callbackSuccess = function () {
    $('#passwdRecoveryForm input').val('')
    $('#passwdRecovery').removeClass('disabled')
  }

  callbackFail = function(){
    $('#passwdRecovery').removeClass('disabled')
  }



  callbackRegistration = function () {
    $('#registr').attr('disabled', false).removeClass('disabled')
  }


  /**
   * init lessons carousel
   */
  carouselHome = function () {
    $('.owl-home-lessons').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      items: 1,
      navText: ['<i class=\'fa fa-chevron-left\'></i>', '<i class=\'fa fa-chevron-right\'></i>']
    })
  }

  showRegisterLoginBox = function () {
    $('#showLoginBox').click(function () {
      $('.register_form').slideUp(300, function () {
        $('.login_form').slideDown(300)
      })
    })

    $('#showRegisterBox').click(function () {
      $('.login_form').slideUp(300, function () {
        $('.register_form').slideDown(300)
      })
    })
  }

  return {
    init: init
  }

})(jQuery, window, document)
class testCreator {

  constructor () {
    this.pageData = Helper.pagesData().testCreator
  }

  setParameters () {
    this.inputs = {
      testTitle: $('input[name=title]'),
      testDescription: $('input[name=description]'),
      testType: $('.right input[name=type]'),
      testDuration: $('#test_duration_slider'),
      startSlots: $('#how_many_people_slider')
    }

    this.mainContainer = $('#testCreator')
    this.form = $('#testCreatorForm')
    this.submitBtn = $('#addTest')
    this.questionsList = $('input.question')
    this.questionsCountContainers = $('#questions_count, #questLength')
    this.selectedQuestionsCount = 0
  }

  /**
   * start
   */
  init () {
    this.setParameters()
    this.initSliders()
    this.submitListener()
    this.clickQuestionListener()
    this.setQuestionsCount()
    this.setFildersListeners()
    ValidatorHelper.testCreator()
  }

  /**
   * submit form
   * add new test
   *
   */
  submitListener () {
    this.submitBtn.click((e) => {
      e.preventDefault()

      if (this.form.valid() && this.validQuestionsCount()) {
        Helper.btnBlock(this.submitBtn, true)
        this.addTest()
      }
    })
  }

  /**
   * add new test
   * send request
   */
  addTest () {
    const action = this.form.attr('action')
    const data = {
      shortTitle: this.inputs.testTitle.val(),
      shortDescription: this.inputs.testDescription.val(),
      testType: this.inputs.testType.is(':checked') ? 1 : 0,
      startSlots: this.inputs.startSlots.slider('option', 'value'),
      timeDuration: this.inputs.testDuration.slider('option', 'value'),
      questionsList: JSON.stringify(this.getQuestionsList())
    }

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.testAddCallback(response)
    }, error => {
      this.testNotAdded(error)
    })

  }

  /**
   *test add callback
   *
   */
  testAddCallback (data) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      this.testAdded()
    } else {
      Helper.btnBlock(this.submitBtn, false)
      if (dataJson.data === null) {
        Helper.formAlert('error', null, this.pageData.message.notAdded)
      } else {
        Helper.formAlert('error', null, dataJson.data)
      }
    }
  }

  /**
   * callback - test added
   *
   * @param data
   */
  testAdded () {
    Helper.btnBlock(this.submitBtn, false)
    Helper.clearForm(this.form)
    $('html,body').animate({scrollTop: 0}, 500)
    $('.alert_box').addClass('active')
    Helper.formAlert('info', null, this.pageData.message.added)
  }

  /**
   * callback - test not added
   *
   * @param error
   */
  testNotAdded (error) {
    Helper.btnBlock(this.submitBtn, false)
    Helper.formAlert('error', null, this.pageData.message.notAdded)
  }

  /**
   *
   * @returns {V|*|jQuery}
   */
  getQuestionsList () {
    return $('input[name=questionsList]:checked').map(function () {return parseInt($(this).val())}).get()
  }

  /**
   * click question
   *
   */
  clickQuestionListener () {

    this.questionsList.on('click', e => {
      const questionInput = $(e.currentTarget)
      const checkedClass = 'checked'

      if (questionInput.is(':checked')) {
        questionInput.closest('li').addClass(checkedClass)
      } else {
        questionInput.closest('li').removeClass(checkedClass)
      }
      this.setQuestionsCount()
    })

  }

  /**
   * set questions count in containers
   */
  setQuestionsCount () {
    const howMany = this.questionsList.filter(':checked').length
    this.questionsCountContainers.html(howMany)
    this.selectedQuestionsCount = howMany
  }

  validQuestionsCount () {
    const questSubmContainer = $('.select_questions_submit')

    if (this.selectedQuestionsCount > 1) {
      questSubmContainer.removeClass('error')
      return true
    } else {
      $.jGrowl('Wybierz pytania!', {group: 'error'})
      questSubmContainer.addClass('error')
      return false
    }
  }

  /**
   * init sliders
   * duration & slots
   */
  initSliders () {
    const sliderDuration = this.inputs.testDuration
    const sliderPeople = this.inputs.startSlots
    const durationCount = $('#duration')
    const peopleCount = $('#people')

    sliderDuration.slider({
      range: 'min', value: 60, min: 1, max: 300,
      slide: function (event, ui) {
        durationCount.html(testCreator.minutesToHour(ui.value))
      }
    })

    sliderPeople.slider({
      range: 'min', value: 10, min: 1, max: 30,
      slide: function (event, ui) {
        peopleCount.html(ui.value + '<span>/</span>' + sliderPeople.slider('option', 'max'))
      }
    })

    durationCount.html(sliderDuration.slider('value') + '<span>min</span>')
    peopleCount.html(sliderPeople.slider('value') + '<span>/</span>' + sliderPeople.slider('option', 'max'))
  }

  static minutesToHour (time) {
    if (time >= 60) {
      const hour = (time / 60)
      let min = time % 60

      if (min < 10) {
        min = '0' + min
      }

      return Math.floor(hour) + '<span>godz.</span>' + min + '<span>min</span>'
    } else {
      if (time < 10) {
        time = '0' + time
      }
      return time + '<span>min</span>'
    }
  }

  /**
   * filters - select questions
   */
  setFildersListeners () {
    const checkAllBtn = $('.check_lesson')
    const checkWithImgBtn = $('.check_with_image')
    const checkWithoutImgBtn = $('.check_without_image')
    const rememberInfo = this.pageData.message.imageFilterRemember

    const setFilter = function (thisBtn, tabPane, children, successAlert = null) {
      tabPane.find('input.question').prop('checked', false).closest('li').removeClass('checked')
      if (!thisBtn.attr('checked')) {
        thisBtn.find('.fa').removeClass('fa-square-o').addClass('fa-check-square-o')
        children.closest('li').addClass('checked')
        children.prop('checked', true)
        thisBtn.closest('.top_lesson').find('button').attr('disabled', true)
        thisBtn.attr('checked', true).attr('disabled', false)
        if (successAlert) {
          Helper.formAlert('info', null, successAlert)
        }
      } else {
        thisBtn.find('.fa').addClass('fa-square-o').removeClass('fa-check-square-o')
        children.closest('li').removeClass('checked')
        children.prop('checked', false)
        thisBtn.attr('checked', false)
        thisBtn.closest('.top_lesson').find('button').attr('disabled', false)
      }
    }

    // check all questions
    checkAllBtn.on('click', e => {
      const thisBtn = $(e.currentTarget)
      const tabPane = thisBtn.closest('.tab-pane')//.data('lid')
      const children = tabPane.find('input.question')

      setFilter(thisBtn, tabPane, children)
      this.setQuestionsCount()
    })

    // check all questions with images
    checkWithImgBtn.on('click', e => {
      const thisBtn = $(e.currentTarget)
      const tabPane = thisBtn.closest('.tab-pane')//.data('lid')
      const children = tabPane.find('input.question.with_img')

      setFilter(thisBtn, tabPane, children, rememberInfo)
      this.setQuestionsCount()
    })

    // check all questions with images
    checkWithoutImgBtn.on('click', e => {
      const thisBtn = $(e.currentTarget)
      const tabPane = thisBtn.closest('.tab-pane')//.data('lid')
      const children = tabPane.find('input.question').not('.with_img')

      setFilter(thisBtn, tabPane, children)
      this.setQuestionsCount()
    })
  };

}
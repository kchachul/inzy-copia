const questionsCollection = (function ($, window, document) {

  const pageData = Helper.pagesData().questionsCollection
  let removeLessonId = false
  let removeQuestionId = false

  init = function () {
    showQuestion()
    addLessonAction()
    addQUestionAction()
    removeLesson()
    changeRadioInput()
    removeQuestionListener()
    editByID()
    ValidatorHelper.lessonCreator()
    ValidatorHelper.questionCreator()
  }

  editByID = function () {
    const editId = Helper.getUrlParameter('edit') * 1
    $(`.nav.nav-tabs li a[aria-controls=test${editId}] `).trigger('click')
    $(`button.addQuestion[data-less_id=${editId}] `).trigger('click')
  }

  addLessonAction = function () {
    $('#addNewLesson').click(function (e) {
      e.preventDefault()
      const btnConfirm = $(this)

      if ($('#addLessonForm').valid()) {
        const title = $('#addLessonModal input[name=title]').val()
        const description = $('#addLessonModal input[name=description]').val()
        const newLesson = new Lesson({title: title, description: description})
        btnConfirm.addClass('disabled')
        newLesson.add(btnConfirm)
      }
    })
  }

  addQUestionAction = function () {
    $('.addNewQuestion').click(function (e) {
      e.preventDefault()
      if ($('#questionCreator').valid()) {
        const questionModal = $('#addQuestionModal')
        const lessonId = $('#questionsCollection .tab-pane.active .options .addQuestion').data('less_id')
        const title = questionModal.find(' input[name=question]').val()
        const numCorrect = questionModal.find('input[name=num_correct]:checked').val() - 1
        const answers =
          [
            {title: questionModal.find('input[name=answer1]').val(), isCorrect: numCorrect === 0},
            {title: questionModal.find('input[name=answer2]').val(), isCorrect: numCorrect === 1},
            {title: questionModal.find('input[name=answer3]').val(), isCorrect: numCorrect === 2}
          ]
      // taka stroktora bo w aktywnych testach dostahe data.title
        const questionObj = new Question({data: {title: title, image: null}, answers: answers})
        questionObj.add(lessonId)
      }
    })
  }

  removeLesson = function () {
    $('#questionsCollection').on('click', '.options .removeLesson', function () {
      removeLessonId = $(this).data('less_id')
      Helper.confirmModal().showModal('Czy na pewno chcesz usunąć kartę?', 'remove_lesson_modal')
    })

    $('#confirmModal .confirm_btn').on('click', function () {
      const confirmModal = $(this).closest('#confirmModal')

      if (confirmModal.hasClass('remove_lesson_modal')) {
        const lessonObj = new Lesson({id: removeLessonId})
        lessonObj.remove()
        removeLessonId = false
      } else if (confirmModal.hasClass('remove_question_modal')) {
        const questionId = removeQuestionId
        const questionObj = new Question({id: questionId})
        questionObj.remove(removedQuestionSuccess)
      }

      $('#confirmModal').modal('hide')
    })

  }

  showQuestion = function () {
    $('#questionsCollection').on('click', '.quest .top', function () {
      const childrenBox = $(this).closest('.quest').children('.bottom')

      if ($(this).hasClass('active')) {
        $(this).removeClass('active')
        childrenBox.slideUp(500)
      } else {
        $(this).addClass('active')
        childrenBox.slideDown(500)
      }
    })
  }

  removeQuestionListener = function () {
    $('#questionsCollection').on('click', '.removebtn button', function () {
      removeQuestionId = $(this).data('quest_id')
      Helper.confirmModal().showModal('Czy na pewno chcesz usunąć pytanie?', 'remove_question_modal')
    })
  }

  changeRadioInput = function () {
    $('input.radio').click(function (event) {
      $('#addQuestionModal label').removeClass('correct')
      $(this).closest('.line').find('label').addClass('correct')
    })
  }

  removedQuestionSuccess = function () {
    questionsNumbers()
    removeQuestionId = false
  }

  questionsNumbers = function () {
    $('.tab-pane.active .questions-list > ul li.quest').each(function (index, value) {
      $(this).children('.top').children('.number').html(`${index + 1}. `)
    })
  }

  return {
    init: init
  }

})(jQuery, window, document)
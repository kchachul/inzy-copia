$(document).ready(function () {

  const system = (function ($, window, document) {

    const config = {
      routungId: {
        homepage: 'main#homepage',
        activeTests: 'main#activeTests',
        testCreator: 'main#testCreator',
        archivalTests: 'main#archivalTests',
        questionsCollection: 'main#questionsCollection',
        userPanel: 'main#userPanel',
        testPage: 'main#testPageContainer',
      }
    }

    // pages routing
    const pageRouting = function () {
      if ($(config.routungId.homepage).length) {
        homepage.init()
      } else if ($(config.routungId.activeTests).length) {
        let tests = new ActiveTests()
        tests.init()
      } else if ($(config.routungId.testCreator).length) {
        let creator = new testCreator()
        creator.init()
      } else if ($(config.routungId.archivalTests).length) {
        archivalTests.init()
      } else if ($(config.routungId.questionsCollection).length) {
        questionsCollection.init()
      } else if ($(config.routungId.userPanel).length) {
        userPanel.init()
      } else if ($(config.routungId.testPage).length) {
        testPage.init()
      }
    };

    (function () {
      pageRouting()
    })()

  })(jQuery, window, document)

})



class System {

  constructor () {
    this.pageData = Helper.pagesData().system
  }

  /**
   * login
   * @param LoginContainer
   */
  login (LoginContainer) {
    const login = LoginContainer.find('input[name=login]').val()
    const password = LoginContainer.find('input[name=password]').val()
    const newPaswd = Helper.keyStretching(password, Helper.getKSIterations())
    const data = {username: login, passwd: newPaswd}
    const action = this.pageData.actions.login

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.loggedIn(response)
    }, error => {
      $('#loginBtn').attr('disabled', false).removeClass('disabled')
      this.failLogin()
    })
  }

  /**
   * lost password
   * @param lostPasswordContainer
   * @param callbackSuccess
   * @param callbackFail
   */
  lostPassword (lostPasswordContainer, callbackSuccess, callbackFail) {
    const login = lostPasswordContainer.find('input[name=login]').val()
    const email = lostPasswordContainer.find('input[name=email]').val()

    const action = this.pageData.actions.lostPassword
    const data = {username: login, email: email}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.callbackLostPassword(response, callbackSuccess, callbackFail)
    }, error => {
      this.callbackLostPasswordFail()
      callbackFail()
    })
  }

  callbackLostPassword (data, callbackSuccess, callbackFail) {
    let dataJson = Helper.parseToJson(data)
    if (dataJson.status === true) {
      this.lostPasswordSuccess()
      callbackSuccess()
    } else if (dataJson.status === false) {
      this.callbackLostPasswordFail()
      callbackFail()
    }
  }

  lostPasswordSuccess () {
    Helper.formAlert('info', null,  this.pageData.message.lostPasswordSuccess)
  }

  callbackLostPasswordFail () {
    Helper.formAlert('error', null, this.pageData.message.loginFailAfter)
  }

  registration (registrationContainer, callbackRegistration) {
    const login = registrationContainer.find('input[name=reg_username]').val()
    const password = registrationContainer.find('input[name=reg_passwd]').val()
    const newPaswd = Helper.keyStretching(password, Helper.getKSIterations())
    const email = registrationContainer.find('input[name=reg_email]').val()
    const data = {username: login, passwd: newPaswd, email: email}

    const action = this.pageData.actions.registration

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      $('#registration_btn').attr('disabled', false).removeClass('disabled')
      this.registeredCallback(response, registrationContainer, callbackRegistration)
    }, error => {
      $('#registration_btn').attr('disabled', false).removeClass('disabled')
      this.failRegistration()
      callbackRegistration()
    })

  }

  registeredCallback (data, registrationContainer, callbackRegistration) {
    let dataJson = Helper.parseToJson(data)

    // lesson added
    if (dataJson.status === true) {
      registrationContainer.find('input').val('')
      Helper.formAlert('info', null, dataJson.data)
      callbackRegistration()
    } else if (dataJson.status === false) {
      Helper.formAlert('error', null, dataJson.data)
      callbackRegistration()
    }
  }

  loggedIn (data) {
    let dataJson = Helper.parseToJson(data)
    if (dataJson.status === true) {
      window.location.replace('panel')
    } else if (dataJson.status === false) {
      $('#loginBtn').attr('disabled', false).removeClass('disabled')
      Helper.formAlert('error', null, this.pageData.message.loginFail)
    }
  }

  failLogin () {
    Helper.formAlert('error', null, this.pageData.message.loginFailAfter)
  }

  failRegistration () {
    Helper.formAlert('error', null, this.pageData.message.registrationFail)
  }

}
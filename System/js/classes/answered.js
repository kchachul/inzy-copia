/**
 * answered questions by student
 */
class Answered {
  constructor (params = {}, questionsList) {

    this.questionId = parseInt(params.questionId) || false
    this.answerId = parseInt(params.answerId) || false

    const questionObj = this.searchQuestion(this.questionId, questionsList)
    const answerObj = this.answerId ? this.searchAnswer(this.answerId, questionObj.answers) : false

    this.question = questionObj.title ? questionObj.title : false
    this.answer = answerObj ? answerObj.title : false
    this.date = params.dateAnswered
    this.isCorrect = !!(params.isCorrect * 1)
  }

  /**
   * search question by question id
   *
   * @param questionId
   * @param questionsList
   * @returns {string|*|null|rules.title|{required}|string}
   */
  searchQuestion (questionId, questionsList) {
    return $.grep(questionsList, function (e) { return e.id == questionId })[0]
  }

  /**
   * search answer by answers list
   *
   * @param answerId
   * @param answersList
   * @returns {string|*|null|rules.title|{required}|string}
   */
  searchAnswer (answerId, answersList) {
    let answer = $.grep(answersList, function (e) { return e.uid == answerId })[0]
    return answer
  }

}
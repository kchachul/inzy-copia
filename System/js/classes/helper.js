class Helper {

  static getTemplateUrl () {
    return 'https://moja-ojczyzna.pl/template/default/assets/images/'
  }

  static pagesData () {
    return {
      archival: {
        message: {
          rmvTest: 'Czy na pewno chcesz usunąć test?',
          testRemoved: 'Test został usumięty!',
          testNotRemoved: 'Test nie mógł zostać usunięty!'
        },
        actions: {
          remove: 'remove_test'
        }
      },
      questionsCollection: {
        message: {
          loadedFail: 'Nie udało się załadować aktywnych testów!',
          confirmRemoveTest: 'Czy na pewno chcesz usunąć test?',
          confirmStopTest: 'Czy na pewno chcesz zatrzymać test?',
          confirmRemoveStudent: 'Czy na pewno chcesz usunąć studenta?',
          failRemoveStudent: 'Niestety nie udało się usunąć studenta',
        },
        actions: {
          get: 'get_active_tests',
          removeActiveStudent: 'remove_active_student'
        }
      },
      testCreator: {
        message: {
          imageFilterRemember: 'Pamiętaj! Aby widzieć obrazki z podpowiedziami musisz zmienić typ testu! ',
          notAdded: 'Test nie został utworzony!',
          added: 'Test został utworzony!'
        }
      },
      lesson: {
        message: {
          notAdded: 'Niestety nie udało się dodać nowego testu!',
          ajaxFail: 'Niestety wystąpił nieoczekiwany błąd!'
        },
        actions: {
          remove: 'remove_lesson',
          add: 'add_lesson'
        }
      },
      question: {
        message: {
          notAdded: 'Niestety nie udało się dodać nowego pytania!',
          notRmoved: 'Niestety nie udało się usunąć pytania!',
          ajaxFail: 'Niestety wystąpił nieoczekiwany błąd!'
        },
        actions: {
          add: 'add_question',
          remove: 'remove_question'
        }
      },
      test: {
        message: {
          removed: 'Test został usumięty!',
          notRemoved: 'Test nie mógł zostać usunięty!',
          started: 'Test został uruchomiony!',
          notStarted: 'Test nie został uruchomiony!',
          notStopped: 'Test nie mógł zostać zatrzymany!',
          ajaxFail: 'Niestety wystąpił nieoczekiwany błąd!',
          notUpdated: 'Nie udało się odświeżyć testu! Nie ma nic nowego!',
          addedToArchival: 'Test został dodany do archiwum!',
          studentAdded: 'Dołączyłeś do testu. Powodzenia!',
          failStarting: 'Nie udało się dołączyć do testu. Sprawdź czy kod testu jest poprawny!',
          failStartingUserExist: 'Nie udało się dołączyć do testu. Taki użytkownik już istnieje!',
          failClearStudent: 'Nie udało się zakończyc testu!'
        },
        actions: {
          remove: 'remove_test',
          start: 'start_test',
          update: 'update_test',
          stop: 'stop_test',
          loadStudentQuestion: 'load_student_question',
          addStudentAnswer: 'add_student_answer',
          clear: 'clear_student',
          addStudent: 'add_student'
        }
      },
      system: {
        message: {
          loginFail: 'Nieprawidłowe dane logowania. Spróbuj jeszcze raz.',
          loginFailAfter: 'Niestety nie udało się zalogować. Spróbuj ponownie poźniej',
          registrationFail: 'Niestety nie udało się zarejestrować. Spróbuj ponownie poźniej',
          lostPasswordFail: 'Niestety nie udało się odzyskac hasla. ',
          lostPasswordSuccess: 'Aby odzyskać hasło sprawdz poczte! ',
        },
        actions: {
          login: 'login',
          lostPassword: 'lost_password',
          registration: 'registration',
        }
      },
      user: {
        message: {
          emailChanged: 'Sprawdź pocztę aby potwierdzić zmianę adresu email. ',
          failEmailChanged: 'Niestety nie udało się zmienić adresu email. Spróbuj później!',
          failRequestEmailChanged: 'Niestety nie udało się zmienić adresu email. Nastąpił nieoczekiwany błąd. Spróbuj później!',
          passwordChenged: 'Sprawdź pocztę aby potwierdzić zmianę hasła!',
          failPasswordChenged: 'Niestety nie udało się zmienić hasła. Czy podałeś prawidłowe dane? ',

        },
        actions: {
          changeEmail: 'change_email',
          changePasswd: 'change_password'
        }
      }
    }
  }

  /**
   *
   * @param type
   * @param element
   * @param text
   */
  static formAlert (type, element, text) {
    switch (type) {
      case 'error':
        $.jGrowl(text, {group: 'error', header: 'UWAGA'})
        break
      case 'success':
        $.jGrowl(text, {group: 'success'})
        break
      case 'info':
        $.jGrowl(text, {group: 'info', header: 'UWAGA'})
        break
    }
  }

  static ajaxRequest (obj) {
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest()
      xhr.open(obj.method || 'GET', obj.url)
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
      xhr.onload = () => {
        if (xhr.status >= 200 && xhr.status < 300) {
          resolve(xhr.response)
        } else {
          reject(xhr.statusText)
        }
      }

      const params = typeof obj.data === 'string' ? obj.data : Object.keys(obj.data).map((k) => {
        return encodeURIComponent(k) + '=' + encodeURIComponent(obj.data[k])
      }).join('&')

      xhr.onerror = () => reject(xhr.statusText)
      xhr.send(params)
    })
  }

  /**
   * show/hide confim modal
   */
  static confirmModal () {
    return {
      showModal: function (text, className = null) {
        $('#confirmModal #text').html(text)
        $('#confirmModal').modal('show')
        if (className) {
          $('#confirmModal').addClass(className)
        }
      },
      closeModal: function () {
        $('#confirmModal').modal('hide')
      }

    }
  }

  /**
   * string to json
   *
   * @param data
   * @returns {*}
   */
  static parseToJson (data) {
    let dataJson = $.parseJSON(data)
    return dataJson
  }

  /**
   * clear inputs & textarea from form
   * @param form
   */
  static clearForm (form) {
    form.find('teaxtarea,input').val('')
  }

  static btnBlock (element, action = true) {
    let disableClass = 'disabled'

    switch (action) {
      case true:
        if (!element.hasClass(disableClass)) {
          element.addClass(disableClass)
          element.prop('disabled', true)
        }
        break
      case false:
        if (element.hasClass(disableClass)) {
          element.removeClass(disableClass)
          element.prop('disabled', false)
        }
        break
    }

  }

  /**
   *
   * @param options
   * @returns {*}
   */
  static ajaxQuery (options) {
    function dafaultCallback () {
      //dafaultError("Set callback function");
    }

    function dafaultError (logInfo) {
      logInfo = logInfo || 'error'
    }

    function dafaultParse () {
      return true
    }

    function checkFunction (newFunction, defaultFunction) {
      if (jQuery.type(newFunction) === 'function') {
        return newFunction
      }
      return defaultFunction
    }

    /*  default options */
    let settings = $.extend({
      method: 'POST',
      url: 'ajax.php',
      data: false,
      dataType: 'json',
      callback: checkFunction(options.callback, dafaultCallback),
      error: checkFunction(options.error, dafaultError),
      parse: checkFunction(options.parse, dafaultParse),
      timeout: 1000
    }, options)

    if (settings.parse(settings.data)) {
      return $.ajax({
        method: settings.method,
        url: settings.url,
        data: settings.data,
        dataType: settings.dataType,
        timeout: settings.timeout

      }).done(function (getData) {
        settings.callback(getData)

      }).fail(function () {
        settings.error()
      })

    } else {
      settings.error()
      return false
    }
  }

  /**
   * datatime to js date
   * @param dateTime
   * @returns {*}
   */
  static datatimeToJsDate (dateTime) {
    if (dateTime !== '0000-00-00 00:00:00') {
      let t = dateTime.split(/[- :]/)
      return new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5])
    } else {
      return false
    }
  }

  /**
   * change minutes to format - 00:00:00
   * @param minutes
   * @returns {string}
   */
  static minutesToFullTime (minutes) {
    let h = Math.floor(minutes / 60)
    let m = minutes % 60
    h = h < 10 ? '0' + h : h
    m = m < 10 ? '0' + m : m
    return `${h}:${m}:00`
  }

  /**
   * time to event - format 00:00:00
   * @param nowTime
   * @param endTime
   * @param loop
   * @returns {*}
   */
  static timeToEvent (nowTime, endTime, loop) {
    let currentTime = nowTime
    let eventTime = endTime
    let remainingTime = (eventTime.getTime() - currentTime.getTime()) - (loop * 1000)

    if (remainingTime > 0) {
      let s = remainingTime / 1000
      let min = s / 60
      let h = min / 60
      let zero = ''
      let sLeft = Math.floor(s % 60)
      let minLeft = Math.floor(min % 60)
      let hLeft = Math.floor(h)

      if ((hLeft === 0)) {
        hLeft = '00'
      } else if (hLeft < 10) {
        zero = '0'
      }

      if (minLeft < 10)
        minLeft = '0' + minLeft
      if (sLeft < 10)
        sLeft = '0' + sLeft
      return zero + hLeft + ':' + minLeft + ':' + sLeft
    } else {
      return 0
    }
  }

  /**
   * create hash - key stretching
   * @param text
   * @param iterations
   * @returns {string}
   */
  static keyStretching (text, iterations) {
    let hash = text
    for (let i = 0; i < iterations; i++) {
      hash = sha3_512(hash)
    }
    return hash.toString()
  }

  static getKSIterations () {
    return 25
  }

  static setBodyWH () {
    $('html,body').css({
      height: '100%',
      width: '100%'
    })
  }

  static getUrlParameter (param) {
    const sPageURL = decodeURIComponent(window.location.search.substring(1))
    const sURLVariables = sPageURL.split('&')
    let sParameterName = ''

    for (let i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=')

      if (sParameterName[0] === param) {
        return sParameterName[1] === undefined ? true : sParameterName[1]
      }
    }
  }

  /**
   * reload page
   */
  static reloadPage () {
    location.reload()
  }

  /**
   * shuffle array
   * @param list
   */
  static shuffleArray (list) {
    for (let i = list.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [list[i], list[j]] = [list[j], list[i]]
    }
  }

}
class Lesson {
  constructor (params = {}) {
    this.pageData = Helper.pagesData().lesson
    this.id = params.id || null
    this.sceneId = params.sceneId || null
    this.title = params.title || null
    this.description = params.description || null
  }

  /**
   * add new lesson
   */
  add (btnConfirm) {
    let action = this.pageData.actions.add
    let data = {title: this.title, description: this.description}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.addCallback(response,btnConfirm)
    }, error => {
      this.fail(error,btnConfirm)
    })
  }

  /**
   * zmienic na lepsza
   */
  draw () {
    $('.questions-list-box .tab-content .tab-pane.active').removeClass('active')
    $('.nav.nav-tabs li.active').removeClass('active')

    const data2 = {
      lessonId: this.id,
      lessonTitle: this.title,
      lessonDescription: this.description
    }

    const html2 = Mustache.render($('#newLessonNavTemplate').html(), data2)
    $('#questionsCollection .nav.nav-tabs').prepend(html2)

    // --------

    const data = {
      lessonId: this.id,
      sceneId: this.sceneId,
      lessonDescription: this.description
    }

    const html = Mustache.render($('#newLessonTemplate').html(), data)
    $('#questionsCollection .tab-content').prepend(html)

    $('#addLessonModal input[name=title]').val('')
    $('#addLessonModal input[name=description]').val('')
    $('#addLessonModal').modal('hide')
  }

  addCallback (data,btnConfirm) {
    let dataJson = Helper.parseToJson(data)

    // lesson added
    if (dataJson.status === true) {
      this.id = dataJson.data.lessonId
      this.sceneId = dataJson.data.sceneId
      this.draw()
    } else if (dataJson.status === false) {
      Helper.formAlert('error', null, this.pageData.message.notAdded)
    }

    btnConfirm.removeClass('disabled')
  }

  /**
   * callback - fail
   */
  fail (btnConfirm) {
    btnConfirm.removeClass('disabled')
    Helper.formAlert('error', null, this.pageData.message.ajaxFail)
  }

  /**
   * remove lesson by lesson ID
   * @param lessonId
   */
  remove () {
    let action = this.pageData.actions.remove
    let data = {lessonId: this.id}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.removeCallback()
    }, error => {
      this.fail(error)
    })
  }

  /**
   * remove callback
   */
  removeCallback () {
    $('.questions-list-box .tab-pane.active').slideUp(200, function () {
      $('.nav.nav-tabs .active').remove()
      $(this).remove()
      $('.nav.nav-tabs li:first').addClass('active')
      $('.tab-content .tab-pane:first').addClass('active')
    })
  }

}
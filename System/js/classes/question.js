class Question {
  constructor (params = {}) {
    this.pageData = Helper.pagesData().question
    this.id = params.id || null
    try {
      this.title = params.data.title || null
      this.answers = params.answers || null
      this.image = params.data.image || null
      this.lessonId = params.lessonId || null
    } catch (e) {
      //  obsluzyc podczas usuwania
    }
  }

  /**
   * add new question
   * @param lessonId - lesson id
   */
  add (lessonId) {
    let action = this.pageData.actions.add
    let data = {title: this.title, answers: JSON.stringify(this.answers), lessonId: lessonId}
    this.lessonId = lessonId

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.addCallback(response)
    }, error => {
      this.fail(error)
    })
  }

  /**
   * callback
   * @param data
   */
  addCallback (data) {
    let dataJson = Helper.parseToJson(data)

    // lesson added
    if (dataJson.status === true) {
      this.id = dataJson.data.id
      this.draw()
    }
    if (dataJson.status === false) {
      Helper.formAlert('error', null, this.pageData.message.notAdded)
    }
  }

  /**
   * callback fail
   */
  fail () {
    Helper.formAlert('error', null, this.pageData.message.ajaxFail)
  }

  /**
   * draw new, added question
   */
  draw () {
    const element3 = $('#questionsCollection .tab-content .tab-pane.active .questions-list >ul')
    const rowNum = ++element3.find('.quest').length

    const data3 = {
      rowNumber: rowNum,
      questionId: this.id,
      questionTitle: this.title,
      lessonId: this.lessonId,
      questionAnswer1class: this.answers[0].isCorrect ? ' correct' : '',
      questionAnswer2class: this.answers[1].isCorrect ? ' correct' : '',
      questionAnswer3class: this.answers[2].isCorrect ? ' correct' : '',
      questionAnswer1: this.answers[0].title,
      questionAnswer2: this.answers[1].title,
      questionAnswer3: this.answers[2].title
    }

    element3.append(Mustache.render($('#newQuestionTemplate').html(), data3))
    $('#addQuestionModal input[type=text]').val('')
    $('#addQuestionModal').modal('hide')

  }

  /**
   * remove THIS question
   */
  remove (removeSuccess) {
    let action = this.pageData.actions.remove
    let data = {id: this.id}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.removeCallback(response,removeSuccess)
    }, error => {
      this.fail(error)
    })
  }

  /**
   * remove callback
   * @param data
   */
  removeCallback (data,removeSuccess) {
    let dataJson = Helper.parseToJson(data)

    // lesson added
    if (dataJson.status === true) {
      const questBox = $(`.quest[data-question-id=${dataJson.data.questionId}]`)
      questBox.slideUp(200, function () {
        questBox.remove()
        removeSuccess()
      })
    }
    if (dataJson.status === false) {
      Helper.formAlert('error', null, this.pageData.message.notRmoved)
    }
  }

}
class User {
  constructor (params = {}) {
    this.pageData = Helper.pagesData().user
  }

  /**
   * change user email
   * @param newEmail
   * @param btnChangeEmail
   */
  changeEmail (newEmail, btnChangeEmail) {
    const action = this.pageData.actions.changeEmail
    const data = {email: newEmail}
    const className = 'disabled'
    this.btnChangeEmail = btnChangeEmail

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.emailChangedCallback(response)
      this.btnChangeEmail.removeClass(className).attr('disabled', false)
    }, error => {
      this.fail(error)
      this.btnChangeEmail.removeClass(className).attr('disabled', false)
    })
  }

  /**
   * change password
   * @param password
   * @param newPassword
   * @param btnChangePasswd
   */
  changePassword (password, newPassword, btnChangePasswd) {
    const action = this.pageData.actions.changePasswd
    const data = {password: password, new_password: newPassword}
    const className = 'disabled'
    this.btnChangePasswd = btnChangePasswd

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.passwdChangedCallback(response)
      this.btnChangePasswd.removeClass(className).attr('disabled', false)
    }, error => {
      this.fail(error)
      this.btnChangePasswd.removeClass(className).attr('disabled', false)
    })

  }

  /**
   * email changed callback
   * @param data
   */
  emailChangedCallback (data) {
    let dataJson = Helper.parseToJson(data)

    // lesson added
    if (dataJson.status === true) {
      $('#changeEmailForm input[name=addressemail]').val('')
      Helper.formAlert('info', null, this.pageData.message.emailChanged)
    }
    if (dataJson.status === false) {
      Helper.formAlert('error', null, this.pageData.message.failEmailChanged)
    }
  }

  /**
   * change emial/password callback = fail
   */
  fail () {
    Helper.formAlert('error', null, this.pageData.message.failRequestEmailChanged)
  }

  /**
   * change password callback
   * @param data
   */
  passwdChangedCallback (data) {
    let dataJson = Helper.parseToJson(data)
    if (dataJson.status === true) {
      $('#changePasswordForm input').val('')
      Helper.formAlert('info', null, this.pageData.message.passwordChenged)
    } else if (dataJson.status === false) {
      Helper.formAlert('error', null, this.pageData.message.failPasswordChenged)
    }
  }

}
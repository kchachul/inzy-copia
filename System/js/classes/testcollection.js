class TestCollection {
  constructor (options = {}) {
    this.pageData = Helper.pagesData().questionsCollection

    this.collection = {
      lastDate: null,
      list: []
    }

    this.testPageContainer = $('#activeTests')
    this.testsContainer = $('#testsContents')
    this.infoBoxContainer = this.testPageContainer.find('.info_box')
    this.topHeaderContainer = this.testPageContainer.find('.top_page_header')
    this.activeStudentId = 0
  }

  init () {
    this.loadActive()
    this.studentsResultsListener()
    this.studentRemoveListener()
  }

  studentsResultsListener () {
    this.testPageContainer.on('click', '.user', function () {
      const userAnswersContainer = $(this).children('.student_answers')
      if (userAnswersContainer.is(':visible')) {
        $(this).removeClass('active')
        userAnswersContainer.slideUp(300)
      } else {
        $(this).addClass('active')
        userAnswersContainer.slideDown(300)
      }
    })
  }

  /* ---- poprawic, przeniesc do test.js ? ---- */

  studentRemoveListener () {
    this.testPageContainer.on('click', '.rmv-student button', (e) => {
      const thisBtn = $(e.currentTarget)
      this.activeStudentId = thisBtn.closest('.user').data('student_id')

      Helper.confirmModal().showModal(this.pageData.message.confirmRemoveStudent, 'remove_student')
    })

    $('#confirmModal .confirm_btn').on('click', e => {
      const confirmBtn = $(e.currentTarget)
      if (confirmBtn.closest('#confirmModal').hasClass('remove_student')) {
        this.removeStudent()
      }
    })
  }

  removeStudent () {
    const studentId = this.activeStudentId
    let action = this.pageData.actions.removeActiveStudent
    let data = {studentId: studentId}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.collection.list[0].removeActiveStudent(studentId)
      this.studentRemoved()
    }, error => {
      this.failRemoveStudent()
    })
  }

  studentRemoved () {
    let activeStudentsCount = ($('.active_users_count').html() * 1) - 1
    Helper.confirmModal().closeModal()
    $('.table_users  .user[data-student_id=' + this.activeStudentId + ']').slideUp(200,
      function () {
        $(this).remove()
        $('.active_users_count').html(activeStudentsCount)
      })
    this.activeStudentId = 0
  }

  /* ---- end poprawic ---- */

  /**
   * load active tests
   */
  loadActive () {
    // przeniesc do jakiegos pliku
    let action = this.pageData.actions.get
    let data = {lastDate: this.collection.lastDate}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.checkCallback(response)
    }, error => {
      this.failLoaded(error)
    })
  }

  /**
   * check callback data
   * @param data
   */
  checkCallback (data) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      this.loaded(dataJson.data)
    }
    if (dataJson.status === false) {
      this.noActiveTests()
    }
  }

  /**
   * test loaded
   *
   * @param data
   */
  loaded (data) {
    this.topHeaderContainer.removeClass('hide')
    this.createTestObj(data)
  }

  createTestObj (data) {

    $.each(data, (index, test) => {
      let newTest = new Test(test, this.testPageContainer)
      this.collection.lastDate = test.dateCreated
      this.collection.list.push(newTest)
      newTest.draw($('#testTheme'), this.testsContainer)
      this.setListeners(newTest)

      $.each(newTest.students, (index, student) => {
        student.draw($('#studentTheme'), newTest.id, newTest.questionsCount)
      })
    })
  }

  /**
   * set listeners stop / start / remove
   */
  setListeners (test) { //  poprawic
    // remove test
    $('.test_desc[data-test_id=' + test.id + '] .right_menu .btn_delete_test').click(() => {
      $('#confirmModal').removeClass('stop_test_modal')
      Helper.confirmModal().showModal(this.pageData.message.confirmRemoveTest, 'remove_test_modal')
    })

    $('.test_desc[data-test_id=' + test.id + '] .right_menu .btn_refresh').click(() => {
      test.setRefreshStatus()
    })

    // remove test
    $('.test_desc[data-test_id=' + test.id + '] .right_menu .start_stop:not(.active)').click(() => {
      test.start()
    })

    // stop test
    $('.test_desc[data-test_id=' + test.id + '] .right_menu .start_stop.active').click(() => {
      $('#confirmModal').removeClass('remove_test_modal')
      Helper.confirmModal().showModal(this.pageData.message.confirmStopTest, 'stop_test_modal')
    })

    $('#confirmModal .confirm_btn').on('click', function () {
      if ($(this).closest('#confirmModal').hasClass('stop_test_modal')) {
        test.stop()
      }
      if ($(this).closest('#confirmModal').hasClass('remove_test_modal')) {
        test.remove()
      }
    })
  }

  /**
   * fail test loaded
   *
   * @param data
   */
  failLoaded (data) {
    Helper.formAlert('error', null, this.pageData.message.loadedFail)
  }

  /**
   * fail remove student
   *
   * @param data
   */
  failRemoveStudent (data) {
    Helper.formAlert('error', null, this.pageData.message.failRemoveStudent)
  }

  /**
   * no active tests
   */
  noActiveTests () {
    this.infoBoxContainer.show()
  }

}
class TestPage {
  constructor () {
    this.pageData = Helper.pagesData().test
  }

  /**
   * add student
   * @param testCode
   * @param studentName
   * @param callbackAdded
   */
  addStudent (testCode, studentName, callbackAdded) {
    let action = this.pageData.actions.addStudent
    let data = {hash: testCode, studentName: studentName}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.addCallback(response, callbackAdded)
    }, error => {
      this.fail(error)
    })
  }

  /**
   * load question
   * @param newQuestionCallback
   * @param callbackFail
   */
  loadNewQuestion (newQuestionCallback, callbackFail) {
    let action = this.pageData.actions.loadStudentQuestion
    let data = {test: 'test'}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.loadedQuestionCallback(response, newQuestionCallback, callbackFail)
    }, error => {
      this.fail(error)
      callbackFail()
    })
  }

  /**
   * send answer
   * @param answerId
   * @param questionId
   * @param callbackAnswerAdded
   * @param callbackFail
   */
  sendAnswer (answerId, questionId, callbackAnswerAdded, callbackFail) {
    let action = this.pageData.actions.addStudentAnswer
    let data = {answerId: answerId, questionId: questionId}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.addAnswerCallback(response, callbackAnswerAdded, callbackFail)
    }, error => {
      this.fail(error)
      callbackFail()
    })
  }

  /**
   * load question - callback
   * @param data
   * @param newQuestionCallback
   * @param callbackFail
   */
  loadedQuestionCallback (data, newQuestionCallback, callbackFail) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      newQuestionCallback(dataJson.data)
    }
    if (dataJson.status === false) {
      this.fail()
      callbackFail()
    }
  }

  addAnswerCallback (data, callbackAnswerAdded, callbackFail) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      callbackAnswerAdded()
    }
    if (dataJson.status === false) {
      this.fail()
      callbackFail()
    }
  }

  addCallback (data, callbackAdded) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      this.studentAdded()
      callbackAdded()

    }
    if (dataJson.status === false && dataJson.data !== null ) {
      this.studentExist()
    }else if(dataJson.status === false){
      this.fail()
    }
  }

  studentAdded () {
    Helper.formAlert('info', null, this.pageData.message.studentAdded)
  }

  fail () {
    Helper.formAlert('error', null, this.pageData.message.failStarting)
  }

  studentExist () {
    Helper.formAlert('error', null, this.pageData.message.failStartingUserExist)
  }

  getAnswersList (answers) {
    let answersHtml = ''

    $.each(answers, function (index, answer) {
      answersHtml += `<li data-answer-id="${answer.id}"><span>${index + 1}</span>${answer.title}</li>`
    })

    return answersHtml
  }

  hideStartView () {
    $('.hashBox').slideUp(200)
    this.questionView()
  }

  drawQuestion () {
    let question = ''
    return question
  }

  startView () {
    const html = Mustache.render($('#hashBox').html())
    return html
  }

  questionView () {
    const html = Mustache.render($('#questionBox').html())
    return html
  }

  /**
   * seat refresh timer - to end
   */
  setTimer (nowDate, endDate) {
    this.timer = 0
    this.intervalObj = setInterval(() => {
      this.updateTimer(this.remainingTime(nowDate, endDate))
    }, 1000)
  }

  remainingTime (nowDate, endDate) {
    this.timer++
    return Helper.timeToEvent(nowDate, endDate, this.timer)
  }

  clearTimer(){
    if(this.intervalObj) {
      clearInterval(this.intervalObj)
    }
  }

  clearStudentData(callbackClearedStudentData){
    let failError = this.pageData.message.failClearStudent
    let action = this.pageData.actions.clear
    let data = {}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.clearStudentCallback(response, callbackClearedStudentData)
    }, error => {
      Helper.formAlert('error', null, failError)
    })
  }

  clearStudentCallback (data, callbackClearedStudentData) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      callbackClearedStudentData()
    }
    if (dataJson.status === false) {
      Helper.formAlert('error', null, this.pageData.message.failClearStudent)
    }
  }


  updateTimer (remainingTime) {
    $('#time').html(remainingTime)
  }

}

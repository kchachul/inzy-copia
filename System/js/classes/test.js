class Test {

  constructor (params = {}, testPageContainer) {
    this.pageData = Helper.pagesData().test
    this.testPageContainer = testPageContainer || false
    this.id = parseInt(params.uid)
    this.dateCreated = params.dateCreated
    this.nowDate = Helper.datatimeToJsDate(params.nowDate)
    this.endDate = Helper.datatimeToJsDate(params.endDate)
    this.shortTitle = params.shortTitle
    this.shortDescription = params.shortDescription
    this.hashName = params.hashName
    this.testType = params.testType
    this.startSlots = params.startSlots
    this.freeSlots = params.freeSlots
    this.activeStudents = params.activeStudents
    this.timeDuration = params.timeDuration
    this.isActive = params.isActive
    this.questions = params.questions ? this.unserializeQuestions(params.questions) : false
    this.questionsCount = this.questions.length || false
    this.students = params.students ? this.unserializeStudents(params.students) : false
    this.studentsCount = this.students.length
    this.timer = 0
    this.refresh = {status: false, blocked: false}
    this.init()
  }

  /**
   * start obj methods
   */
  init () {
    if (this.isActive) {
      this.setTimer()
    }
  }

  /**
   * unserialize questions to Question Object
   *
   * @param questions
   * @returns {Array}
   */
  unserializeQuestions (questions) {
    let questionsArr = []
    $.each(typeof helper !== 'undefined' ? helper.shuffleArray(questions) : questions, function (index, question) {
      questionsArr.push(new Question(question))
    })
    return questionsArr
  }

  /**
   * unserialize active students to Student Object
   *
   * @param students
   * @returns {Array}
   */
  unserializeStudents (students) {
    let studentsArr = []

    $.each(students, (index, student) => {
      studentsArr.push(new Student(student, this.questions))
    })

    return studentsArr
  }

  setRefreshStatus () {
    const refreshBtn = this.testPageContainer.find('.test_desc[data-test_id="' + this.id + '"] .btn_refresh')
    if (refreshBtn.hasClass('active') && this.refresh.status) {
      refreshBtn.removeClass('active')
      this.refresh.status = false
      this.refresh.blocked = false
    } else {
      refreshBtn.addClass('active')
      this.refresh.status = true
      this.refresh.blocked = false
    }
  }

  /**
   * Mustache test template
   *
   * @param testTheme
   * @param testsContainer
   */
  draw (testTheme, testsContainer) {

    const data = {
      testId: this.id,
      testTitle: this.shortTitle,
      testDescription: this.shortDescription,
      testHash: this.hashName,
      testUrl: 'test?code=' + this.hashName,
      questionsCount: this.questionsCount,
      activeStudentsCount: this.studentsCount,
      timeToEnd: this.timeDuration ? Helper.minutesToFullTime(this.timeDuration) : '00:00:00',
      optionsMenuClass: (this.isActive ? 'fa-stop' : 'fa-play'),
      optionsMenuActiveClass: (this.isActive ? ' active' : ''),
      offline: !this.isActive
    }
    let html = Mustache.render(testTheme.html(), data)
    testsContainer.append(html)
  }


  setTimer () {
    setInterval(() => {
      this.updateTimer(this.remainingTime(), this.id)

      if (this.refresh.status && !this.refresh.blocked) {
        this.updateData()
      }
    }, 1000)
  }

  /**
   * remaining time to ending
   *
   * @returns {*}
   */
  remainingTime () {
    this.timer++
    return Helper.timeToEvent(this.nowDate, this.endDate, this.timer)
  }

  /**
   * update timer
   * @param remainingTime
   * @param testId
   */
  updateTimer (remainingTime, testId) {
    this.testPageContainer.find('.test_desc[data-test_id="' + testId + '"] .end_time').html(remainingTime)
  }

  removeActiveStudent (studentId) {
    this.students = $.grep(this.students, function (e) { return e.id != studentId })
    this.studentsCount -= 1
    this.activeStudents -= 1
  }

  /**
   * remove test
   */
  remove () {
    let action = this.pageData.actions.remove
    let data = {testId: this.id}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.removeCallback(response)
    }, error => {
      this.fail(error)
    })
  }

  /**
   * check callback data
   * @param data
   */
  removeCallback (data) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      this.removed(dataJson.data)
    }
    if (dataJson.status === false) {
      this.notRmoved()
    }
  }

  /**
   * success - callback remove test
   * // to samo co przy usuwaniu archiwalnego?
   */
  removed () {
    let alertText = this.pageData.message.removed
    $('#confirmModal').modal('hide')
    $(this.testPageContainer).find('#testsContents').find(`.test_desc[data-test_id="${this.id}"]`).hide(function () {
      $(this).remove()
      Helper.formAlert('info', null, alertText)
    }, this.showInfoBox())

  }

  /**
   * fail - callback remove test
   */
  notRmoved () {
    $('#confirmModal').modal('hide')
    Helper.formAlert('info', null, this.pageData.message.notRemoved)
  }

  /**
   * start test
   */
  start () {
    let action = this.pageData.actions.start
    let data = {testId: this.id}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.startCallback(response)
    }, error => {
      this.fail(error)
    })
  }

  /**
   * check callback data
   * @param data
   */
  startCallback (data) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      this.started(dataJson.data)
    }
    if (dataJson.status === false) {
      this.notStarted()
    }
  }

  /**
   *
   */
  started () {
    Helper.formAlert('info', null, this.pageData.message.started)
    Helper.reloadPage()
  }

  /**
   *
   */
  notStarted () {
    Helper.formAlert('error', null, this.pageData.message.notStarted)
  }

  /**
   *
   */
  fail () {
    Helper.formAlert('error', null, this.pageData.message.ajaxFail)
  }

  /**
   * check test is ended
   * @returns {boolean}
   */
  isEnded () {
    return this.endDate > this.nowDate
  }


  updateData () {
    this.refresh.blocked = true
    let action = this.pageData.actions.update
    const studentsData = []

    $.each(this.students, (index, student) => {
      const lastDate = student.answeredList.length > 0 ? student.answeredList[student.answeredList.length - 1].date : false
      studentsData.push({
        'studentId': student.id,
        'lastAnsweredData': lastDate
      })
    })

    const data = {testId: this.id, students: JSON.stringify(studentsData)}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.updateCallback(response)
    }, error => {
      this.refresh.blocked = false
      this.fail(error)
    })
  }

  /**
   * test data
   * @param data
   */
  updateCallback (data) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      this.updated(dataJson.data)
    }
    if (dataJson.status === false) {
      this.notUpdated()
    }
  }

  /**
   *
   * @param data
   */
  updated (data) {
    if (data.new.length) {
      this.setNewStudents(data.new)
      this.updateActiveStudentsCount()
    }

    if (data.old.length) {
      this.updateStudentData(data.old)
    }

    this.refresh.blocked = false
  }

  setNewStudents (newStudents) {
    // foreach new students
    $.each(newStudents, (index, student) => {
      let newStudent = new Student({
        uid: student.uid * 1,
        name: student.name,
        joinDate: student.joinDate,
        points: student.points * 1
      })
      this.students.push(newStudent)
      this.studentsCount += 1
      this.activeStudents += 1

      newStudent.draw($('#studentTheme'), this.id, this.questionsCount)
    })
  }

  /**
   * update students results
   * @param students
   */
  updateStudentData (students) {
    $.each(students, (index, student) => {
      let selectStudent = this.students.filter(x => x.id === student.studentId)[0]

      $.each(student.answered, (index, answer) => {
        selectStudent.answeredCount += 1

        const newAnswered = new Answered(answer, this.questions)
        selectStudent.answeredList.push(newAnswered)

        if (newAnswered.isCorrect) {
          selectStudent.points += 1
        }
      })
      selectStudent.drawNewResults(this.id)
      selectStudent.updateData(this.id, this.questionsCount)
    })

  }

  /**
   * DOM - update active students
   */
  updateActiveStudentsCount () {
    $('.test_desc[data-test_id="' + this.id + '"] .active_users_count').html(this.studentsCount)
  }

  /**
   * updated fail
   */
  notUpdated () {
    this.refresh.blocked = false
    Helper.formAlert('error', null, this.pageData.message.notUpdated)
  }

  /**
   * remove test
   */
  stop () {
    let action = this.pageData.actions.stop
    let data = {testId: this.id}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.stopCallback(response)
    }, error => {
      this.failStoped(error)
    })
  }

  /**
   * check callback data
   * @param data
   */
  stopCallback (data) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      this.stoped(dataJson.data)
    }
    if (dataJson.status === false) {
      this.notStoped()
    }
  }

  /**
   * success - callback remove test
   */
  stoped () {
    $('#confirmModal').modal('hide')
    $(this.testPageContainer).find('#testsContents').find(`.test_desc[data-test_id="${this.id}"]`).remove()
    Helper.formAlert('info', null, this.pageData.message.addedToArchival)
    this.showInfoBox()
  }

  /**
   * fail - callback remove test
   */
  notStoped () {
    $('#confirmModal').modal('hide')
    Helper.formAlert('info', null, this.pageData.message.notStopped)
  }

  failStoped () {
    Helper.formAlert('error', null, this.pageData.message.notStopped)
  }

  showInfoBox () {
    $('#activeTests .top_page_header').hide(function () {
      $('#activeTests .info_box').slideDown()
    })

  }

}
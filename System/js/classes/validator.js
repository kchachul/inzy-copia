$(document).ready(function () {
  $.extend($.validator.messages, {
    required: 'To pole jest wymagane.',
    remote: 'Proszę o wypełnienie tego pola.',
    email: 'Proszę o podanie prawidłowego adresu email.',
    url: 'Proszę o podanie prawidłowego URL.',
    date: 'Proszę o podanie prawidłowej daty.',
    dateISO: 'Proszę o podanie prawidłowej daty (ISO).',
    number: 'Proszę o podanie prawidłowej liczby.',
    digits: 'Proszę o podanie samych cyfr.',
    creditcard: 'Proszę o podanie prawidłowej karty kredytowej.',
    equalTo: 'Proszę o podanie tej samej wartości ponownie.',
    extension: 'Proszę o podanie wartości z prawidłowym rozszerzeniem.',
    maxlength: $.validator.format('Proszę o podanie nie więcej niż {0} znaków.'),
    minlength: $.validator.format('Proszę o podanie przynajmniej {0} znaków.'),
    rangelength: $.validator.format('Proszę o podanie wartości o długości od {0} do {1} znaków.'),
    range: $.validator.format('Proszę o podanie wartości z przedziału od {0} do {1}.'),
    max: $.validator.format('Proszę o podanie wartości mniejszej bądź równej {0}.'),
    min: $.validator.format('Proszę o podanie wartości większej bądź równej {0}.'),
    pattern: $.validator.format('Pole zawiera niedozwolone znaki.')
  })

  $.validator.addMethod('lettersonly', function (value, element) {
    return this.optional(element) || /^[a-z]+$/i.test(value)
  }, 'Proszę o podanie samych liter')

  $.validator.addMethod('username', function (value, element) {
    return this.optional(element) || /^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/i.test(value)
  }, 'Podany tekst jest nieprawidłowy! (a-z A-Z 0-9 _)')

/*
  $.validator.addMethod('alphanumeric', function (value, element) {
    return this.optional(element) || /^[^ĄąĆćĘęŁłŃńÓóŚśŹźŻż{L}0-9]+$/i.test(value)
  }, 'Podany tekst jest nieprawidłowy! (a-z A-Z 0-9)')

  $.validator.addMethod('polishAlphanumeric', function (value, element) {
    return this.optional(element) || /^[\s\p{L}0-9 ]+$/u.test(value)
  }, 'Podany tekst jest nieprawidłowy!')
*/
})

class ValidatorHelper {

  constructor () {

  }

  /**
   * test creator form
   */
  static testCreator () {
    let formContainer = $('#testCreatorForm')

    formContainer.validate({
      onfocusout: false,
      onkeyup: false,
      rules: {
        title: {
          required: true,
          minlength: 3,
          maxlength: 100,
          // polishAlphanumeric: true
        },
        description: {
          required: true,
          minlength: 3,
          maxlength: 200,
          // polishAlphanumeric: true
        }
      },
      errorPlacement: function (error, element) {
        return false
      },
      invalidHandler: function (form, validator) {
        if (!validator.numberOfInvalids())
          return
        Helper.formAlert('error', validator.errorList[0].element, validator.errorList[0].message)

        $('html, body').animate({
          scrollTop: $(validator.errorList[0].element).offset().top - 20
        }, 500)
      }
    })
  }

  /**
   * lesson creator form
   */
  static lessonCreator () {
    let formContainer = $('#addLessonForm')

    formContainer.validate({
      onfocusout: false,
      onkeyup: false,
      rules: {
        title: {
          required: true,
          minlength: 3,
          maxlength: 50,
          // polishAlphanumeric: true
        },
        description: {
          required: true,
          minlength: 3,
          maxlength: 200,
          // polishAlphanumeric: true
        }
      },
      errorPlacement: function (error, element) {
        return false
      },
      invalidHandler: function (form, validator) {
        if (!validator.numberOfInvalids())
          return
        Helper.formAlert('error', validator.errorList[0].element, validator.errorList[0].message)
      }
    })
  }

  /**
   * login
   */
  static login () {
    let formContainer = $('#loginform')

    formContainer.validate({
      onfocusout: false,
      onkeyup: false,
      rules: {
        login: {
          required: true,
          maxlength: 25,
          username: true
        },
        password: {
          required: true,
          minlength: 8,
          maxlength: 40
        }
      },
      errorPlacement: function (error, element) {
        return false
      },
      invalidHandler: function (form, validator) {
        if (!validator.numberOfInvalids())
          return
        Helper.formAlert('error', validator.errorList[0].element, validator.errorList[0].message)
      }
    })
  }

  /**
   * lost password
   */
  static lostPassword () {
    let formContainer = $('#passwdRecoveryForm')

    formContainer.validate({
      onfocusout: false,
      onkeyup: false,
      rules: {
        login: {
          required: true,
          maxlength: 25
        },
        email: {
          required: true,
          email: true
        }
      },
      errorPlacement: function (error, element) {
        return false
      },
      invalidHandler: function (form, validator) {
        if (!validator.numberOfInvalids())
          return
        Helper.formAlert('error', validator.errorList[0].element, validator.errorList[0].message)
      }
    })
  }

  static registration () {
    let formContainer = $('#registr')

    formContainer.validate({
      onfocusout: false,
      onkeyup: false,
      rules: {
        reg_username: {
          required: true,
          minlength: 3,
          maxlength: 25
        },
        reg_passwd: {
          required: true,
          minlength: 8,
          maxlength: 40
        },
        reg_passwd2: {
          equalTo: '#reg_passwd'
        },
        reg_email: {
          required: true,
          email: true
        }
      },
      errorPlacement: function (error, element) {
        return false
      },
      invalidHandler: function (form, validator) {
        if (!validator.numberOfInvalids())
          return
        Helper.formAlert('error', validator.errorList[0].element, validator.errorList[0].message)
      }
    })
  }

  /**
   * question creator form
   */
  static questionCreator () {
    let formContainer = $('#questionCreator')

    formContainer.validate({
      onfocusout: false,
      onkeyup: false,
      rules: {
        question: {
          required: true
        },
        answer1: {
          required: true
        },
        answer2: {
          required: true
        },
        answer3: {
          required: true
        }
      },
      errorPlacement: function (error, element) {
        return false
      },
      invalidHandler: function (form, validator) {
        if (!validator.numberOfInvalids())
          return
        Helper.formAlert('error', validator.errorList[0].element, validator.errorList[0].message)
      }
    })
  }

  /**
   * change email
   */
  static changeEmail () {
    let formContainer = $('#changeEmailForm')

    formContainer.validate({
      onfocusout: false,
      onkeyup: false,
      rules: {
        addressemail: {
          required: true,
          email: true
        }
      },
      errorPlacement: function (error, element) {
        return false
      },
      invalidHandler: function (form, validator) {
        if (!validator.numberOfInvalids())
          return
        Helper.formAlert('error', validator.errorList[0].element, validator.errorList[0].message)
      }
    })
  }

  /**
   * change password
   */
  static changePassword () {
    // changePasswordForm
    let formContainer = $('#changePasswordForm')

    formContainer.validate({
      onfocusout: false,
      onkeyup: false,
      rules: {
        passwd: {
          required: true
        },
        new_passwd: {
          required: true,
          minlength: 8,
          maxlength: 40
        },
        new_passwd2: {
          required: true,
          equalTo: 'input[name=new_passwd]'
        }
      },
      errorPlacement: function (error, element) {
        return false
      },
      invalidHandler: function (form, validator) {
        if (!validator.numberOfInvalids())
          return
        Helper.formAlert('error', validator.errorList[0].element, validator.errorList[0].message)
      }
    })

  }

  /**
   * add student to active test
   */
  static addStudent () {
    let formContainer = $('#addStudent')

    formContainer.validate({
      onfocusout: false,
      onkeyup: false,
      rules: {
        hash: {
          required: true
        },
        student_name: {
          required: true,
          minlength: 3,
          maxlength: 60
        }
      },
      errorPlacement: function (error, element) {
        return false
      },
      invalidHandler: function (form, validator) {
        if (!validator.numberOfInvalids())
          return
        Helper.formAlert('error', validator.errorList[0].element, validator.errorList[0].message)
      }
    })
  }

}
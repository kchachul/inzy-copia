class Student {

  constructor (params = {}, questionsList) {
    this.id = parseInt(params.uid) || false
    this.name = params.name || false
    this.joinDate = params.joinDate || false
    this.answered = params.answered ? this.unserializeAnswered(params.answered, questionsList) : false
    this.points = parseInt(params.uPoints) || 0
    this.answeredList = this.answered ? this.getAnsweredCount() : []
    this.answeredCount = this.answeredList.length
  }

  getAnsweredCount () {
    const answeredArr = this.answered.filter(function (elem) {
      return elem.answerId !== false
    })

    return answeredArr
  }

  /**
   * Student test template
   *
   * @param studentTheme
   * @param testsContainer
   */
  draw (studentTheme, testId, questionsCount) {
    const studentPercent = this.pointsPercents(questionsCount)
    const data = {
      studentId: this.id,
      studentName: this.name,
      questionsCount: questionsCount,
      answeredCount: this.answeredCount,
      questionsPercent: this.answeredPercents(questionsCount),
      studentPoints: this.points,
      studentPointsPercent: studentPercent,
      studentGradge: this.gradge(studentPercent)
    }

    let html = Mustache.render(studentTheme.html(), data)
    $('.test_desc[data-test_id="' + testId + '"] .users_list .active_users_table').append(html)

    this.drawNewResults(testId)
  }

  answeredPercents (questionsCount) {
    return Math.round(((this.answeredCount * 100) / questionsCount) * 100) / 100
  }

  pointsPercents (questionsCount) {
    return Math.round(((this.points * 100) / questionsCount) * 100) / 100
  }

  gradge (percents) {
    if (percents >= 90) {
      return 'bdb'
    } else if (percents >= 75) {
      return 'db'
    } else if (percents >= 51) {
      return 'dst'
    } else if (percents >= 30) {
      return 'dop'
    } else {
      return 'ndst'
    }
  }

  drawNewResults (testId) {
    const container = $('.test_desc[data-test_id="' + testId + '"]  .user[data-student_id=' + this.id + '] .student_answers .answers_list')
    const emptyResults = $('.test_desc[data-test_id="' + testId + '"] .student_answers .empty')
    const answeredDrawnLeng = container.find('.answer').length
    const newElementsLeng = this.answeredCount - answeredDrawnLeng

    if (newElementsLeng > 0) {
      if (emptyResults.length) {
        emptyResults.hide()
      }
      this.drawStudentResults(container, this.answeredList.slice(-newElementsLeng), answeredDrawnLeng)
    }
  }

  updateData (testId, questionsCount) {
    const studentPercent = this.pointsPercents(questionsCount)
    const container = $('.test_desc[data-test_id="' + testId + '"]  .user[data-student_id=' + this.id + ']')
    container.find('.question_count').html(this.answeredCount)
    container.find('.progress-bar').css('width', this.answeredPercents(questionsCount) + '%')
    container.find('.student_points').html(this.points)
    container.find('.students_points_pr').html(studentPercent)
    container.find('.student_gradge').html('<span class="'+this.gradge(studentPercent)+'"></span>')
  }

  unserializeAnswered (answered, questionsList) {
    let AnsweredArr = []
    $.each(answered, function (index, answered) {
      AnsweredArr.push(new Answered(answered, questionsList))
    })
    return AnsweredArr
  }

  drawStudentResults (element, results, answeredDrawnLeng) {
    let loop = answeredDrawnLeng + 1

    $.each(results, function (key, result) {
      const data = {
        loopId: loop++,
        question: result.question,
        answer: result.answer,
        className: (result.isCorrect ? 'correct' : 'wrong'),
        pkt: (result.isCorrect ? '+1' : '+0')
      }

      const html = Mustache.render($('#studentResult').html(), data)
      element.append(html)
    })

  }

}
class Archival {

  constructor () {
    this.pageData = Helper.pagesData().archival
  }

  init (testId) {
    this.id = testId
  }

  /**
   * remove test
   */
  remove () {
    let action = this.pageData.actions.remove
    let data = {testId: this.id}

    Helper.ajaxRequest({method: 'POST', url: action, data: data}).then(response => {
      this.removeCallback(response)
    }, error => {
      this.notRmoved(error)
    })
  }

  /**
   * check callback data
   * @param data
   */
  removeCallback (data) {
    let dataJson = Helper.parseToJson(data)

    if (dataJson.status === true) {
      this.removed(dataJson.data)
    }
    if (dataJson.status === false) {
      this.notRmoved()
    }
  }

  /**
   * success - callback remove test
   */
  removed () {
    const removedText = this.pageData.message.testRemoved

    $('#confirmModal').modal('hide')
    $(`.test_desc[data-test_id="${this.id}"]`).hide(function () {
      $(this).remove()
      $('.nav-tabs li.active').remove()
      Helper.formAlert('info', null, removedText)

      if ($(`.test_desc`).length) {
        $('.nav-tabs a:first').tab('show')
      } else {
        $('#archivalTests .top_page_header').hide(function () {
          $('#archivalTests .info_box').slideDown()
        })
      }
    })
  }

  /**
   * fail - callback remove test
   */
  notRmoved () {
    $('#confirmModal').modal('hide')
    Helper.formAlert('info', null, this.pageData.text.testNotRemoved)
  }

}
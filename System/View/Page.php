<?php

namespace System\View;

class Page extends \System\Engine\View
{
    public function homepage() {
        $this->getHead();
        $this->getHeader();
        $this->renderHTML('registration_modal', 'components/');
        $this->renderHTML('homepage', 'pages/');
        $this->renderHTML('lost_password_modal', 'components/');
        $this->renderHTML('authors', 'components/');
        $this->renderHTML('contact_modal', 'components/');
        $this->getFooter();
    }

    public function activeTests() {
        $this->getHead();
        $this->getHeader();
        $this->renderHTML('top_panel', 'components/');
        $this->renderHTML('active_tests', 'pages/');
        $this->renderHTML('confirm_modal', 'components/');
        $this->renderHTML('authors', 'components/');
        $this->renderHTML('contact_modal', 'components/');
        $this->getFooter();
    }

    public function testCreator() {
        $this->getHead();
        $this->getHeader();
        $this->renderHTML('top_panel', 'components/');
        $this->renderHTML('test_creator', 'pages/');
        $this->renderHTML('authors', 'components/');
        $this->renderHTML('contact_modal', 'components/');
        $this->getFooter();
    }

    public function archivalTests() {
        $this->getHead();
        $this->getHeader();
        $this->renderHTML('top_panel', 'components/');
        $this->renderHTML('archival_tests', 'pages/');
        $this->renderHTML('confirm_modal', 'components/');
        $this->renderHTML('authors', 'components/');
        $this->renderHTML('contact_modal', 'components/');
        $this->getFooter();
    }

    public function questionsCollection() {
        $this->getHead();
        $this->getHeader();
        $this->renderHTML('top_panel', 'components/');
        $this->renderHTML('questions_collection', 'pages/');
        $this->renderHTML('confirm_modal', 'components/');
        $this->renderHTML('authors', 'components/');
        $this->renderHTML('contact_modal', 'components/');
        $this->getFooter();
    }

    public function userPanel() {
        $this->getHead();
        $this->getHeader();
        $this->renderHTML('top_panel', 'components/');
        $this->renderHTML('user_panel', 'pages/');
        $this->renderHTML('authors', 'components/');
        $this->renderHTML('contact_modal', 'components/');
        $this->getFooter();
    }

    public function test() {
        $this->getHead();
        $this->renderHTML('top_left_menu', 'components/');
        $this->renderHTML('test', 'pages/');
    }

    public function resultsPdf() {
        $this->renderHTML('pdf', 'pages/');
    }

    public function lessons() {
        $this->renderHTML('head', 'lessons/containers/');
        $this->renderHTML('top_left_menu', 'components/');
        $this->renderHTML('l1', 'lessons/');
        $this->renderHTML('footer', 'lessons/containers/');
    }


    public function lesson($sceneId) {
        $this->renderHTML('s'.$sceneId, 'lessons/l1/');
    }

    public function error404() {
        $this->getHead();
        $this->getHeader();
        $this->renderHTML('error404', 'pages/');
        $this->renderHTML('authors', 'components/');
        $this->getFooter();
    }

}
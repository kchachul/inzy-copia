const gulp = require('gulp');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');


gulp.task('css', function() {
  return gulp.src([
    './template/default/libs/bootstrap/bootstrap.min.css',
    './template/default/libs/bootstrap/bootstrap-theme.min.css',
    './template/default/libs/jquery/jquery-ui.css',
    './template/default/libs/jquery/jquery.jgrowl.min.css',
    './template/default/libs/lightbox/lightbox.min.css',
    './template/default/libs/owlcarousel/owl.carousel.min.css',
    './template/default/assets/css/main.css'
  ])
    .pipe(sourcemaps.init())
    .pipe(concat('public-main.css'))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('js', function() {
  return gulp.src([
    './template/default/libs/sha3/sha3.js',
    './template/default/libs/jquery/jquery-3.2.0.min.js',
    './template/default/libs/owlcarousel/owl.carousel.min.js',
    './template/default/libs/bootstrap/bootstrap.min.js',
    './template/default/libs/jquery/jquery-ui.js',
    './template/default/libs/jquery/jquery.validate.min.js',
    './template/default/libs/jquery/jquery.jgrowl.min.js',
    './template/default/libs/mustache/mustache.min.js',
    './template/default/libs/lightbox/lightbox.min.js',
    './System/js/classes/helper.js',
    './System/js/classes/validator.js',
    './System/js/classes/system.js',
    './System/js/classes/user.js',
    './System/js/classes/lesson.js',
    './System/js/classes/student.js',
    './System/js/classes/test.js',
    './System/js/classes/question.js',
    './System/js/classes/answer.js',
    './System/js/classes/answered.js',
    './System/js/classes/testcollection.js',
    './System/js/classes/testpage.js',
    './System/js/classes/archival.js',
    './System/js/pages/homepage.js',
    './System/js/pages/test_creator.js',
    './System/js/pages/active_tests.js',
    './System/js/pages/archival_tests.js',
    './System/js/pages/questions_collection.js',
    './System/js/pages/user_panel.js',
    './System/js/pages/test_page.js',
    './System/js/main.js'
  ])
    .pipe(sourcemaps.init())
    .pipe(concat('public-main.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/js'));
});

gulp.task('production', gulp.series('css', 'js'));
<!-- Modal -->
<div class="modal fade" id="modalLostPassword" tabindex="-1" role="dialog" aria-labelledby="modalLostPasswordLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modalLostPasswordLabel">Odzyskiwanie hasła</h4>
            </div>
            <div class="modal-body">
                <form id="passwdRecoveryForm">
                    <div class="row">
                        <div class="col-md-2 left"><label>login</label></div>
                        <div class="col-md-10 box">
                            <input name="login" placeholder="podaj login" type="text">
                            <i class="fa fa-exclamation error" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 left"><label>email</label></div>
                        <div class="col-md-10 box">
                            <input name="email" placeholder="podaj adres email" type="text">
                            <i class="fa fa-exclamation error" aria-hidden="true"></i>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center btnbox">
                            <button class="btn btn_blue smallest_pdn" id="passwdRecovery" data-uid="1">odzyskaj</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 log"></div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
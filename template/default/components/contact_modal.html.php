
<!-- Modal -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="contactModalLabel">Kontakt</h3>
            </div>
            <div class="modal-body">
                <p>Wszelkie pytania lub sugestie dotyczące działania serwisu prosimy kierować na adres:</p>
                <p><a href="mailto:kontakt@moja-ojczyzna.pl">kontakt@moja-ojczyzna.pl</a></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
            </div>
        </div>
    </div>
</div>
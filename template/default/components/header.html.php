<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <nav class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="<?php echo HTTP_SERVER.URL_HOMEPAGE; ?>"><img src="<?php echo HTTP_SERVER.DIR_TEMPLATE.'assets/images/main/logo.png';?>" alt=""></a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="<?php echo(ACTIVE_PAGE === URL_HOMEPAGE ? 'active' : ''); ?>">
                                    <a href="/" class="btn btn-default">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span class="hdn">STRONA GŁÓWNA</span>
                                    </a>
                                </li>
                                <li>
                                    <?php if ($this->isOnline): ?>
                                        <div class="btn-group ">
                                            <button type="button" class="btn btn-default dropdown-toggle"
                                                    data-toggle="dropdown" id="mainSubmenuBtn">
                                                <i class="fa fa-user"></i>
                                                <span>PANEL</span>
                                            </button>
                                            <div class="dropdown-menu col-sm-12" role="menu" id="mainSubmenu">
                                                <i class="fa fa-caret-up caret-top" aria-hidden="true"></i>
                                                <?php
                                                $this->renderHTML('menu_panel', 'components/');
                                                ?>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <button type="button" class="btn btn-default"
                                                data-toggle="modal" data-target="#registrationLoginBox">
                                            <i class="fa fa-user"></i>
                                            <span>NAUCZYCIEL</span>
                                        </button>
                                    <?php endif; ?>
                                </li>
                                <li>
                                    <a href="<?php echo URL_LESSONS; ?>" class="btn btn-default">
                                        <i class="fa fa-book" aria-hidden="true"></i>
                                        <span>NAUKA</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo URL_TEST; ?>" class="btn btn-default">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                        <span>TESTY</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
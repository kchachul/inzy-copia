<div id="menu_panel">

    <p class="dropdown-header">
        <a href="<?php echo URL_USER_PANEL; ?>">
            <i class="fa fa-user" aria-hidden="true"></i><?php echo $this->userName; ?>
        </a>
    </p>
    <div class="subnav">
        <ul>
            <li>
                <a class="btn btn-default <?php echo(ACTIVE_PAGE === URL_TEST_CREATOR ? 'active' : ''); ?>"
                   href="<?php echo URL_TEST_CREATOR; ?>">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i><?php echo MENU_TITLE_TEST_CREATOR; ?>
                </a>
            </li>

            <li><a class="btn btn-default <?php echo(ACTIVE_PAGE === URL_ACTIVE_TESTS ? 'active' : ''); ?>"
                   href="<?php echo URL_ACTIVE_TESTS; ?>">
                    <i class="fa fa-list" aria-hidden="true"></i><?php echo MENU_TITLE_PANEL; ?>
                </a>
            </li>

            <li>
                <a class="btn btn-default <?php echo(ACTIVE_PAGE === URL_ARCHIVAL_TESTS ? 'active' : ''); ?>"
                   href="<?php echo URL_ARCHIVAL_TESTS; ?>">
                    <i class="fa fa-archive" aria-hidden="true"></i><?php echo MENU_TITLE_ARCHIVAL_TESTS; ?>
                </a>
            </li>

            <li>
                <a class="btn btn-default <?php echo(ACTIVE_PAGE === URL_QUESTIONS_COLLECTION ? 'active' : ''); ?>"
                   href="<?php echo URL_QUESTIONS_COLLECTION; ?>">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i><?php echo MENU_TITLE_QUESTIONS_COLLECTION; ?>
                </a>
            </li>
        </ul>
    </div>

    <div class="sub-btn">
        <a class="btn btn_blue_border logout" href="<?php echo ACT_LOGOUT;?>"><b>wyloguj się</b></a>
    </div>

    <div class="to_panel text-center">
        <a class="" href="<?php echo URL_USER_PANEL; ?>">ustawienia konta &rsaquo;</a>
    </div>

</div>
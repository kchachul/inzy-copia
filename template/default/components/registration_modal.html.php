<div class="modal fade" id="registrationLoginBox" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <section id="register_modal_box">

                <div class="row main">
                    <div class="col-md-6 left">
                        <div class="title">
                            <h3>Sprawdź swoich uczniów w naszej platformie testowej</h3>
                            <h4>Gotowe scenariusze dla nauczycieli</h4>
                        </div>
                    </div>
                    <div class="col-md-6 right">

                        <div class="register_form">

                            <h3 class="text-center">Zarejestruj się!</h3>

                            <div class="row">
                                <form id="registr">

                                    <div class="col-md-12 input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input name="reg_username" id="reg_username" class="form-control"
                                               placeholder="Login"
                                               autocomplete="off"
                                               required="" type="text">
                                    </div>
                                    <div class="col-md-12 input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input name="reg_passwd" id="reg_passwd" class="form-control"
                                               placeholder="Hasło"
                                               autocomplete="off"
                                               required="" type="password">
                                    </div>
                                    <div class="col-md-12 input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input name="reg_passwd2" id="reg_passwd2" class="form-control"
                                               placeholder="Powtórz hasło" autocomplete="off"
                                               required="" type="password">
                                    </div>
                                    <div class="col-md-12 input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input name="reg_email" id="reg_email" class="form-control"
                                               placeholder="Adres e-mail"
                                               autocomplete="off"
                                               required="" type="text">
                                    </div>

                                    <div class="col-md-12 sbmt text-center">
                                        <button class="btn btn_green small_pdn dsb_text_transform" id="registration_btn">Zarejestruj mnie</button>
                                    </div>

                                </form>
                                <div class="col-md-12 login_btn text-center">
                                    <button id="showLoginBox">mam już konto w serwisie</button>
                                </div>
                            </div>
                        </div>


                        <div class="login_form">
                            <h3 class="text-center">Zaloguj się!</h3>
                            <div class="row">
                                <form id="loginform" method="post">
                                    <div class="col-md-12 input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" name="login" class="form-control" placeholder="Username"
                                               id="panel_username"
                                               autocomplete="off" />
                                    </div>

                                    <div class="col-md-12 input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input type="password" name="password" class="form-control" id="panel_passwd"
                                               placeholder="Password"
                                               autocomplete="off" />
                                    </div>

                                    <div class="col-md-12 sbmt text-center">
                                        <button class="btn btn_green small_pdn dsb_text_transform" id="loginBtn">Zaloguj się</button>
                                    </div>
                                </form>

                                <div class="col-md-12 lost_passw text-center">
                                    <button id="lost_password" type="button" data-toggle="modal"
                                            data-target="#modalLostPassword">zapomniałem hasła
                                    </button>
                                </div>

                                <div class="col-md-12 register_btn">
                                    <button id="showRegisterBox">Zarejestruj się</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </section>

        </div>
    </div>
</div>
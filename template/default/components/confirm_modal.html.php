<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p id="text"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn disabled without_loading" data-dismiss="modal">ANULUJ</button>
                <button type="button" class="btn btn_green smallest_pdn confirm_btn">Wykonaj</button>
            </div>
        </div>
    </div>
</div>
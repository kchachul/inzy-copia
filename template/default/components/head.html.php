<!DOCTYPE html>
<html lang="pl">
<head>
    <title><?php echo $this->pageTitle;?> - <?php echo WEBSITE_TITLE?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo HTTP_SERVER.DIR_TEMPLATE.'libs/awesomeicons/css/font-awesome.min.css';?>">
    <link rel="stylesheet" href="<?php echo HTTP_SERVER.'dist/css/public-main.css';?>">
    <script src="<?php echo HTTP_SERVER.'dist/js/public-main.js';?>"></script>
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=latin-ext" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140224793-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-140224793-1');
    </script>


</head>
<body>
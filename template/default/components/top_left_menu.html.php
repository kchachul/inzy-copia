<!-- menu -->
<nav class="navbar navbar-default navbar-fixed-top" id="topNavbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>


        <div id="navbar" class="navbar-collapse collapse">
            <div class="row">
                <div class="col-md-2 left <?php echo ACTIVE_PAGE === URL_TEST ? 'without_border' : ''; ?>">
                    <div class="image">
                        <a href="/"><img
                                    src="<?php echo HTTP_SERVER . DIR_TEMPLATE . 'assets/images/pages/test/'; ?>logo_mo.png"
                                    alt=""></a>
                    </div>
                    <? if(ACTIVE_PAGE !== URL_TEST): ?>
                        <button id="moreInfo"><i class="fa fa-bars" aria-hidden="true"></i> MENU</button>
                    <?php endif; ?>
                </div>
                <?php if (ACTIVE_PAGE === URL_LESSONS): ?>
                    <div class="col-md-8 center">
                        <ul class="nav navbar-nav navbar-center">
                            <li class="navi-left">
                                <button id="prevNavi">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                </button>
                            </li>
                            <li class="navi-title">
                                <ul class="titles">
                                    <?php /* foreach ($this->js as $lesson): */ ?>
                                    <?php foreach ($this->lessons as $key => $lesson): ?>
                                        <?php foreach ($lesson['scenes'] as $scene): ?>
                                            <li data-lessonNr="<?php echo $scene['nr'] ?>">
                                                <div class="number">Lekcja <?php echo $lesson['nr']; ?>| <strong>Scena <?php echo $scene['nr'] ?></strong></div>
                                                <div class="title"><?php echo $scene['title'] ?></div>
                                            </li>
                                            <?php /* endforeach; */ ?>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                            <li class="navi-right">
                                <button id="nextNavi">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-2 right">
                        <div class="navi-all">
                            <button data-toggle="modal" data-target="#ModalSelectLesson">
                                <i class="fa fa-th" aria-hidden="true"></i> ZMIEŃ LEKCJE
                            </button>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</nav>
<? if(ACTIVE_PAGE !== URL_TEST): ?>
<div id="leftMenu">
    <nav>
        <ul>
            <li><a href="/">Strona główna</a></li>
            <li class="<?php echo ACTIVE_PAGE === URL_TEST ? 'active' : ''; ?>">
                <a href="<?php echo URL_TEST; ?>">Test</a>
            </li>
            <li class="<?php echo ACTIVE_PAGE === URL_LESSONS ? 'active ' : ''; ?>scenes">
                <button>Lekcje</button>
                <ul>
                    <?php foreach ($this->lessons as $key => $lesson): ?>
                        <li>
                            <button class="lesson"> <?php echo $lesson['nr'] . '. ' . $lesson['title']; ?></button>
                            <ul class="scenes">
                                <?php foreach ($lesson['scenes'] as $scene): ?>
                                    <li>
                                        <a href="<?php echo URL_LESSONS . '#scene' . $scene['nr']; ?>"
                                           data-scene="<?php echo $scene['nr']; ?>"><?php echo $scene['title']; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </li>
        </ul>
    </nav>
</div>
<?php endif ?>

<div id="greyBg"></div>
<!-- end menu -->
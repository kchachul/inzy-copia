<footer class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-6 left">
                <div class="row">
                    <div class="col-md-3">
                        <ul>
                            <li><a href="<?php echo URL_HOMEPAGE; ?>">Strona główna</a></li>
                            <li>
                                <?php if ($this->isOnline): ?>
                                    <a href="<?php echo URL_ACTIVE_TESTS; ?>">Nauczyciel</a>
                                <?php else: ?>
                                    <button type="button" class="btn btn-default"
                                            data-toggle="modal" data-target="#registrationLoginBox">
                                        <span>Nauczyciel</span>
                                    </button>
                                <?php endif; ?>
                            </li>
                            <li><a href="<?php echo URL_LESSONS; ?>">Lekcje</a></li>
                            <li><a href="<?php echo URL_TEST; ?>">Testy</a></li>
                        </ul>
                    </div>


                    <div class="col-md-4">
                        <ul>
                            <li>
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#authorsModal">
                                    Od autorów
                                </button>
                            </li>
                            <li>
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#contactModal">
                                    Kontakt
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-5 social_urls">
                        <a target="_blank" href="<?php echo SOCIAL_MEDIA_FACEBOOK; ?>" class="social"><i
                                    class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a target="_blank" href="<?php echo SOCIAL_MEDIA_INSTAGRAM; ?>" class="social"><i
                                    class="fa fa-instagram" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 right">
                <p class="b10"><?php echo FOOTER_COPYRIGHT_TEXT1; ?></p>
                <p><?php echo FOOTER_COPYRIGHT_TEXT2; ?></p>
            </div>
        </div>
    </div>
</footer>


</body>
</html>

<!-- Modal -->
<div class="modal fade" id="authorsModal" tabindex="-1" role="dialog" aria-labelledby="authorsModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="authorsModalLabel">Od autorów</h3>
            </div>
            <div class="modal-body">
                <p>W sieci istnieje wiele takich serwisów, ale żaden z nich nie zawiera starannie
                wyselekcjonowanych i łatwych w odbiorze podstawowych informacji, na przykład o historii
                    własnej ojczyzny.</p>

                <p>Kluczową ideą było odejście od tradycyjnych sposobów nauczania, które często
                oscylowały wokół metody „suchych faktów” oraz zbyt zawiłego podejścia do tematu, zbyt
                    trudnego, mało atrakcyjnego w odbiorze dla dzieci.</p>

                <p>Wzbudzenie zainteresowania wśród najmłodszych jest możliwe wtedy, gdy informacje
                zawarte na stronie są dostosowane do ich kategorii wiekowej oraz umiejętności. Trzymanie
                    się tych zasad jest gwarancją skutecznej i swobodnej nauki.</p>

                <p>Nieustający rozwój nowych technologii umożliwia tworzenie nowej drogi
                prezentowania treści na witrynach internetowych. Rodzice oraz przede wszystkim
                szkolni pedagodzy powinni dołożyć wszelkich starań, aby „przygoda” ich podopiecznych
                    z komputerem była efektywna i pożyteczna, a nauka dobrą zabawą.</p>

                <p>W najbliższej przyszłości planowana jest rozbudowa aplikacji o kolejne lekcje (z różnych
                dziedzin wiedzy) oraz nowe funkcjonalności. Dalsza wizja rozwoju serwisu zakłada współpracę
                z wydawnictwami tworzącymi podręczniki szkolne dla dzieci. Nie jest wykluczone również,
                    że stworzony serwis stanie się platformą e-learningową dla polskich szkół za granicą.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
            </div>
        </div>
    </div>
</div>
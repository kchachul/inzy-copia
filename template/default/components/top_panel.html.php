<div class="container-fluid" id="top_panel">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title">Panel nauczyciela</h2>
            </div>
        </div>
    </div>

    <div class="navPanel">
        <ul class="nav nav-pills container">
            <li role="presentation" class="<?php echo(ACTIVE_PAGE === URL_ACTIVE_TESTS ? 'active' : ''); ?>">
                <a href="<?php echo URL_ACTIVE_TESTS;?>">
                    <i class="fa fa-list"></i>
                    <span class="hidden-xs"><?php echo MENU_TITLE_PANEL;?></span>
                </a>
            <li role="presentation" class="<?php echo(ACTIVE_PAGE === URL_TEST_CREATOR ? 'active' : ''); ?>">
                <a href="<?php echo URL_TEST_CREATOR;?>">
                    <i class="fa fa-plus-circle"></i>
                    <span class="hidden-xs"><?php echo MENU_TITLE_TEST_CREATOR;?></span></a>
            </li>
            <li role="presentation" class="<?php echo(ACTIVE_PAGE === URL_ARCHIVAL_TESTS ? 'active' : ''); ?>">
                <a href="<?php echo URL_ARCHIVAL_TESTS;?>">
                    <i class="fa fa-archive"></i>
                    <span class="hidden-xs"><?php echo MENU_TITLE_ARCHIVAL_TESTS;?></span>
                </a>
            </li>
            <li role="presentation" class="<?php echo(ACTIVE_PAGE === URL_QUESTIONS_COLLECTION ? 'active' : ''); ?>">
                <a href="<?php echo URL_QUESTIONS_COLLECTION;?>">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                    <span class="hidden-xs"><?php echo MENU_TITLE_QUESTIONS_COLLECTION;?></span>
                </a>
            </li>
        </ul>
    </div>
</div>
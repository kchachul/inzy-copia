<?php
require(DIR_TEMPLATE.DIR_LIBS . '/fpdf/tfpdf.php');




class PDF extends tFPDF
{
    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 10, $this->PageNo() . '/{nb}', 0, 0, 'C');
    }
}


$testDescription = $this->Results[0];


$pdf = new PDF();
$pdf->AliasNbPages();

$pdf->AddPage();
$pdf->AddFont('DejaVu', '', 'DejaVuSansCondensed.ttf', true);
$pdf->SetFont('DejaVu', '', 14);

$pdf->SetFillColor(222, 222, 222);
$pdf->Write(5, "Szczegóły testu");
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('DejaVu', '', 9);
$pdf->Cell(30, 6, 'Tytuł', 1, 0, "L", true);
$pdf->Cell(150, 6, $testDescription['shortTitle'], 1);
$pdf->Ln();

$pdf->Cell(30, 6, 'Opis', 1, 0, "L", true);
$pdf->Cell(150, 6, $testDescription['shortDescription'], 1);
$pdf->Ln();

$pdf->Cell(30, 6, 'Data', 1, 0, "L", true);
$pdf->Cell(150, 6, $testDescription['dateCreated'] . ' - ' . $testDescription['endDate'].' ('.date('H:i', mktime(0,$testDescription['timeDuration'])).'h)', 1);
$pdf->Ln();

$pdf->Cell(30, 6, 'Ilość pytań', 1, 0, "L", true);
$pdf->Cell(150, 6, $testDescription['questionsCount'], 1);
$pdf->Ln();
$pdf->Cell(30, 6, 'Ilość uczniów', 1, 0, "L", true);
$pdf->Cell(150, 6, count($testDescription['students']), 1);
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();


//name
$pdf->SetFont('DejaVu', '', 14);
$pdf->Write(5, "Uczniowie");
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('DejaVu', '', 9);


if($testDescription['students']) {
    $header = array(array('nr'), array('Nazwa'), array('pkt'), array('szczegóły'));
    $pdf->Cell(7, 6, $header[0][0], 1, 0, "L", true);
    $pdf->Cell(148, 6, $header[1][0], 1, 0, "L", true);
    $pdf->Cell(8, 6, $header[2][0], 1, 0, "L", true);
    $pdf->Cell(17, 6, $header[3][0], 1, 0, "L", true);

    foreach ($testDescription['students'] as $key => $student) {
        $pdf->Ln();
        $pdf->Cell(7, 6, (string)($key + 1), 1);
        $pdf->Cell(148, 6, $student['name'], 1);
        $pdf->Cell(8, 6, $student['points'], 1);
        $link[$key] = $pdf->AddLink('link');
        $pdf->Cell(17, 6, 'zobacz', 1, 0, "L", false, $link[$key]);
    }

    foreach ($testDescription['students'] as $key => $student) {
        $pdf->AddPage();
        $pdf->SetLink($link[$key]);
        $pdf->SetFont('DejaVu', '', 14);
        $pdf->Write(5, "Użytkownik: " . $student['name']);
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('DejaVu', '', 9);
        foreach ($student['questions'] as $key => $result) {

            if($result['answer']) {
                $pdf->Cell(6, 6, (string)($key + 1), 1, 0, "L", true);
                $pdf->Cell(23, 6, "pytanie", 1, 0, "L", true);
                $pdf->Cell(150, 6, $result['question'], 1);
                $pdf->Ln();
                $pdf->Cell(6, 6, "", 0);
                $pdf->Cell(12, 6, ($result['isCorrect'] ? '+1pkt' : '0pkt'), 1, 0, "L", true);
                $pdf->Cell(11, 6, "odp", 1, 0, "L", true);
                $pdf->Cell(150, 6, $result['answer'], 1);
                $pdf->Ln();
                $pdf->Ln();
            }
        }
    }
}else{
    $pdf->Write(5, "Brak uczniów");
}


$pdf->Output();

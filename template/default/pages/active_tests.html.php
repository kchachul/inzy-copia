<main id="activeTests" class="subpage bg_grey">

    <div class="container top_page_header hide">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title"><?php echo TMP_ACTIVE_TESTS_TITLE;?></h2>
                <h3 class="subtitle"><?php echo TMP_ACTIVE_TESTS_SUBTITLE;?></h3>
            </div>
        </div>
    </div>

    <div class="info_box container text-center no_active">
        <div class="image text-center"></div>
        <div class="text">
            <h3 class="title"><?php echo TMP_ACTIVE_TESTS_NO_ACTIVE_TITLE;?></h3>
            <h4 class="subtitle"><?php echo TMP_ACTIVE_TESTS_NO_ACTIVE_SUBTITLE;?></h4>
        </div>
        <div class="row btns">
            <div class="col-md-6 col-xs-6 left_text_right">
                <a href="<?php echo URL_ARCHIVAL_TESTS; ?>" class="btn btn_blue_border">ARCHIWUM</a>
            </div>
            <div class="col-md-6 col-xs-6 right_text_left">
                <a href="<?php echo URL_TEST_CREATOR; ?>" class="btn btn_green">NOWY TEST</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row tab tab-content" id="testsContents"></div>
    </div>

    <?php /* test Mustache template */ ?>
    <script id="testTheme" type="text/html">
        <div class="col-md-12 test_desc tab-pane fade in {{className}} active" role="tabpanel" data-test_id="{{testId}}"
             id="test{{testId}}">

            <div class="row head">
                <div class="col-md-8 left">
                    <h2>{{testTitle}}</h2>
                    <p>{{testDescription}}</p>
                </div>
                <div class="col-md-4 right">
                    <div class="code">
                        <i class="fa fa-clipboard"></i>
                        ID: <a target="_blank" href="{{testUrl}}">
                            <span>{{testHash}}</span>
                        </a>
                    </div>
                </div>


                <div class="right_menu">
                    <ul>
                        <li>
                            <button class="start_stop {{optionsMenuActiveClass}}" data-toggle="tooltip"
                                    data-placement="top" title="uruchom test">
                                <i class="fa {{optionsMenuClass}}" aria-hidden="true"></i>
                            </button>
                        </li>
                        {{#offline}}
                        <li>
                            <button class="btn_delete_test" data-toggle="tooltip" data-placement="top" title="usuń">
                                <i class="fa fa-trash" aria-hidden="true"></i></button>
                        </li>
                        {{/offline}}
                        <li>
                            <button class="btn_refresh" data-toggle="tooltip" data-placement="top"
                                    title="automatyczne odświeżanie">
                                <i class="fa fa-history" aria-hidden="true"></i>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row options">


                <div class="col-md-4 box">
                    <div class="cont1">
                        <div class="left"><i class="fa fa-check"></i></div>
                        <div class="right">
                            <p class="small">ilość pytań</p>
                            <p class="big">{{questionsCount}}</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 box">
                    <div class="cont2">
                        <div class="left"><i class="fa fa-users"></i></div>
                        <div class="right">
                            <p class="small">aktywnych osób</p>
                            <p class="big"><span class="active_users_count">{{activeStudentsCount}}</span>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 box">
                    <div class="cont3">
                        <div class="left"><i class="fa fa-clock-o"></i></div>
                        <div class="right">
                            <p class="small">czas do końca</p>
                            <p class="big end_time">{{timeToEnd}}</p>
                        </div>
                    </div>
                </div>


            </div>


            <div class="row users_list">
                <div class="col-md-12 table_users">
                    <div class="row table">

                        <div class="thead">
                            <div class="row row_table">
                                <div class="col-md-3 ">Imię i nazwisko</div>
                                <div class="col-md-4 ">udzielonych odpowiedzi</div>
                                <div class="col-md-1 text-center">punkty</div>
                                <div class="col-md-2 text-center">POPRAWNE</div>
                                <div class="col-md-2 ">sugerowana ocena</div>
                            </div>
                        </div>

                        <div class="active_users_table">

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </script>


<?php /* student Mustache template */ ?>
    <script id="studentTheme" type="text/html">
        <div class="row row_table user" data-student_id="{{studentId}}">
            <div class="rmv-student">
                <button>
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </button>
            </div>
            <div class=" col-md-3 first">{{studentName}}</div>
            <div class=" col-md-4">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuemin="0"
                         aria-valuemax="100"
                         data-test_count="100"
                        style="width:{{questionsPercent}}%">
                        <span class="question_count">{{answeredCount}}</span>/{{questionsCount}}
                    </div>
                </div>
            </div>

            <div class=" col-md-1 text-center">
                <span class="student_points">{{studentPoints}}</span> / {{questionsCount}}
            </div>

            <div class=" col-md-2 text-center">
                <span class="students_points_pr">{{studentPointsPercent}}</span>%
            </div>


            <div class=" col-md-2 last">
                <span class="student_gradge">
                    <span class="{{studentGradge}}"></span>
                </span>
            </div>

            <div class="col-md-12 student_answers">
                <div class="answers_list">
                    <p class="empty text-center">Brak wyników</p>
                </div>
            </div>
        </div>
    </script>


    <script id="studentResult" type="text/html">
        <div class="row answer {{className}}">
            <div class="col-md-1">{{loopId}}</div>
            <div class="col-md-5">{{question}}</div>
            <div class="col-md-5">{{answer}}</div>
            <div class="col-md-1 text-center">{{pkt}} pkt</div>
        </div>
    </script>

</main>
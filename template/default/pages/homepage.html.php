<?php if ($this->infoBox): ?>
    <div class="infoBox"><?php echo $this->infoBox; ?></div>
<?php endif; ?>

<main id="homepage">
    <section id="mainStart">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-6">
                    <h2><?php echo TMP_HOMEPAGE_SUBTITLE; ?></h2>
                    <h1><?php echo TMP_HOMEPAGE_TITLE; ?></h1>
                    <p><?php echo TMP_HOMEPAGE_SUBTITLE2; ?></p>
                    <p class="btns">
                        <a href="<?php echo URL_LESSONS; ?>" class="btn btn_blue">ROZPOCZNIJ NAUKĘ</a>
                        <?php if (!$this->isOnline): ?>
                            <button class="btn btn_blue_border" data-toggle="modal" data-target="#registrationLoginBox">
                                <?php echo TMP_HOMEPAGE_TEST_PANEL_LABEL;?>
                            </button>
                        <?php else: ?>
                            <a class="btn btn_blue_border" href="<?php echo URL_ACTIVE_TESTS; ?>"><?php echo TMP_HOMEPAGE_TEST_PANEL_LABEL;?>
                            </a>
                        <?php endif; ?>
                    </p>
                </div>
            </div>
        </div>
    </section>


    <div class="container" id="teacher">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2><?php echo TMP_HOMEPAGE_TEACHER_TITLE; ?></h2>
                <p><?php echo TMP_HOMEPAGE_TEACHER_SUBTITLE; ?></p>
            </div>
        </div>
    </div>


    <div class="container" id="lessons_slider">
        <div class="owl-carousel owl-home-lessons">
            <?php if ($this->lessons): ?>
                <?php foreach ($this->lessons as $key => $lesson): ?>
                    <?php foreach ($lesson['scenes'] as $scene): ?>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-6 image">
                                    <img src="<?php echo DIR_TEMPLATE_IMAGES . 'lessons/' . $scene['thumb']; ?>"
                                         alt="">
                                </div>
                                <div class="col-md-6 right">
                                    <h2><?php echo $scene['nr'] . ". " . $scene['title']; ?></h2>
                                    <?php echo $scene['description']; ?>
                                    <p class="btns"><a href="<?php echo URL_LESSONS . '#scene' . $scene['nr'] ?>"
                                                       class="btn btn_blue_border">WYBIERAM</a></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>


    <div class="container-fluid" id="quotation">
        <div class="flag_bg">
            <img src="<?php echo HTTP_SERVER . DIR_TEMPLATE; ?>assets/images/pages/homepage/flag.png" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="quot"><?php echo TMP_HOMEPAGE_QUOTATION; ?></p>
                    <p class="text-right">
                        <i>Józef Piłsudski</i>
                    </p>

                </div>
                <div class="col-md-6 right">
                    <h2><?php echo TMP_HOMEPAGE_QUOTATION_SUBTITLE; ?></h2>

                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid grey_bg">

        <div class="container" id="knowledge">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2><?php echo TMP_HOMEPAGE_SECTION2_TITLE; ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 left">
                    <img src="<?php echo HTTP_SERVER . DIR_TEMPLATE; ?>assets/images/pages/homepage/sample.png" alt="">
                </div>
                <div class="col-md-6 right">
                    <p><?php echo TMP_HOMEPAGE_SECTION2_TEXT; ?></p>
                    <p class="btns">
                        <?php if (!$this->isOnline): ?>
                            <button class="btn btn_blue_border" data-toggle="modal" data-target="#registrationLoginBox">
                                <?php echo TMP_HOMEPAGE_TEACHER_PANEL_LABEL;?>
                            </button>
                        <?php else: ?>
                            <a class="btn btn_blue_border" href="<?php echo URL_ACTIVE_TESTS; ?>"><?php echo TMP_HOMEPAGE_TEACHER_PANEL_LABEL;?>
                            </a>
                        <?php endif; ?>

                    </p>
                </div>
            </div>
        </div>

        <div class="container" id="testSection">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2><?php echo TMP_HOMEPAGE_SECTION3_TITLE;?></h2>
                    <p><?php echo TMP_HOMEPAGE_SECTION3_TEXT;?></p>
                </div>
            </div>
            <div class="teacher_test">
                <div class="step step1">
                    <span class="num">1</span>
                    <h3><?php echo TMP_HOMEPAGE_SECTION3_STEP1_TITLE;?></h3>
                    <p><?php echo TMP_HOMEPAGE_SECTION3_STEP1_TEXT;?></p>
                </div>
                <div class="step step2">
                    <span class="num">2</span>
                    <h3><?php echo TMP_HOMEPAGE_SECTION3_STEP2_TITLE;?></h3>
                    <p><?php echo TMP_HOMEPAGE_SECTION3_STEP2_TEXT;?></p>
                </div>
                <div class="step step3">
                    <span class="num">3</span>
                    <h3><?php echo TMP_HOMEPAGE_SECTION3_STEP3_TITLE;?></h3>
                    <p><?php echo TMP_HOMEPAGE_SECTION3_STEP3_TEXT;?></p>
                </div>
            </div>
        </div>

    </div>

</main>


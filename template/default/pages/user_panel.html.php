<main id="userPanel" class="subpage bg_grey">

    <div class="container top_page_header">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title"><?php echo TMP_USER_PANEL_TITLE; ?></h2>
                <h3 class="subtitle"><?php echo TMP_USER_PANEL_SUBTITLE; ?></h3>
            </div>
        </div>
    </div>


    <div class="container settings">
        <div class="row">

            <div class="col-md-12 subtitle">
                <h3><?php echo TMP_USER_PANEL_SUBTITLE_1; ?></h3>
            </div>

            <div class="col-md-12 box">
                <div class="row">
                    <div class="col-md-4 left"><?php echo TMP_USER_PANEL_FIELD_1; ?></div>
                    <div class="col-md-8 right login"><?php echo $this->userName; ?></div>
                </div>
                <div class="row">
                    <div class="col-md-4 left"><?php echo TMP_USER_PANEL_FIELD_2; ?></div>
                    <div class="col-md-8 right login"><?php echo $this->userEmail; ?></div>
                </div>
            </div>

            <div class="col-md-12 subtitle">
                <h3><?php echo TMP_USER_PANEL_SUBTITLE_2; ?></h3>
            </div>

            <div class="col-md-12 box">

                <form method="post" id="changeEmailForm">
                    <div class="row">
                        <div class="col-md-4 left"><?php echo TMP_USER_PANEL_FIELD_3; ?></div>
                        <div class="col-md-8 right">
                            <input type="text" name="addressemail" placeholder="<?php echo TMP_USER_PANEL_FIELD_3_PLACEHOLDER; ?>"
                                   autocomplete="off">
                            <i class="fa fa-exclamation error" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 left"></div>
                        <div class="col-md-8 right">
                            <button class="btn btn_blue smallest_pdn" id="changeEmail"><?php echo TMP_USER_PANEL_BTN_1; ?></button>
                            <i data-toggle="popover" data-placement="top" title="Zmiana adresu email"
                               data-content="<?php echo TMP_USER_PANEL_BTN_1_INFO_BOX;?> <?php echo $this->userEmail; ?>"
                               class="fa fa-question-circle popover-info"></i>
                        </div>
                    </div>
                </form>

            </div>

            <div class="col-md-12 subtitle">
                <h3><?php echo TMP_USER_PANEL_SUBTITLE_3; ?></h3>
            </div>

            <div class="col-md-12 box">
                <form method="post" id="changePasswordForm">
                    <div class="row">
                        <div class="col-md-4 left"><?php echo TMP_USER_PANEL_FIELD_4; ?></div>
                        <div class="col-md-8 right">
                            <input type="password" name="passwd" placeholder="<?php echo TMP_USER_PANEL_FIELD_4_PLACEHOLDER; ?>" autocomplete="off">
                            <i class="fa fa-exclamation error" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 left"><?php echo TMP_USER_PANEL_FIELD_5; ?></div>
                        <div class="col-md-8 right">
                            <input type="password" name="new_passwd" placeholder="<?php echo TMP_USER_PANEL_FIELD_5_PLACEHOLDER; ?>" autocomplete="off">
                            <i class="fa fa-exclamation error" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 left"><?php echo TMP_USER_PANEL_FIELD_6; ?></div>
                        <div class="col-md-8 right">
                            <input type="password" name="new_passwd2" placeholder="<?php echo TMP_USER_PANEL_FIELD_6_PLACEHOLDER; ?>"
                                   autocomplete="off">
                            <i class="fa fa-exclamation error" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 left"></div>
                        <div class="col-md-8 right">
                            <button class="btn btn_blue smallest_pdn" id="changePassword"><?php echo TMP_USER_PANEL_BTN_2; ?>
                            </button>
                            <i data-toggle="popover" data-placement="top" title="Zmiana hasła"
                               data-content="<?php echo TMP_USER_PANEL_BTN_2_INFO_BOX;?> <?php echo $this->userEmail; ?>"
                               class="fa fa-question-circle popover-info"></i>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</main>
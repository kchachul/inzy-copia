<main class="container-fluid<?php echo $this->testStatus ? ' active' : ''; ?>" id="testPageContainer">

    <?php if (!$this->testStatus): ?>
        <div class="hashBox">
            <div class="bg_top"></div>
            <section class="start_view container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="start_image"></div>
                    </div>
                    <div class="col-md-12 text-center text">
                        <h2>Witaj w teście on-line</h2>
                        <p>Trzymamy za Ciebie kciuki! Powodzenia!</p>
                    </div>
                    <div class="col-md-12 box">
                        <form method="POST" id="addStudent">
                            <input id="test_hash" name="hash" class="hash_input" placeholder="kod testu"
                                   data-toggle="tooltip" data-placement="left"
                                   title="Jeśli sobie nie radzisz, poproś o pomoc&nbsp;nauczyciela!"
                                   type="text"
                                   value="<?php echo isset($_GET['code']) ? $_GET['code'] : ''; ?>" autocomplete="off">
                            <input type="text" class="username_input" placeholder="Imię i Nazwisko"
                                   name="student_name" autocomplete="off">
                            <button id="select_test_btn" class="hash_btn" type="submit">Rozpocznj test
                            </button>
                        </form>
                    </div>
                </div>
            </section>
            <div class="bg_bottom"></div>
        </div>
    <?php endif; ?>

    <?php if ($this->testStatus): ?>

        <?php if (!$this->isOver): ?>
            <div class="test_view">
                <section id="testBox" class="container">
                    <div class="row question_row">
                        <div id="bg_testBox">
                            <div class="loader"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center title">
                                <div id="test_name"><?php echo $this->test['title'] ?></div>
                                <div id="test_description"><?php echo $this->test['description']; ?></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 question">
                                <div id="question"><?php echo $this->question['title'] ?></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 answers">
                                <div class="row">
                                    <div class="col-md-<?php echo $this->test['type'] ? '8' : '11'; ?>">
                                        <ul id="answers"
                                            data-question-id="<?php echo $this->question['id']; ?>">
                                            <?php foreach ($this->question['answers'] as &$answer): ?>
                                                <li data-answer-id="<?php echo $answer['id']; ?>"><?php echo '<span>' . ++$index . '</span> ' . $answer['title']; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>

                                    <?php if ($this->test['type']): ?>
                                        <div class="col-md-4 ">
                                            <div class="image_question">
                                                <div class="silver"></div>
                                                <div class="gold"></div>
                                                <div class="card">
                                                    <div class="front face">
                                                        <img class="pulse_anim"
                                                             src="<?php echo HTTP_SERVER . DIR_TEMPLATE . 'assets/images/pages/test/question_hidden.png'; ?>"
                                                             alt="">
                                                    </div>
                                                    <div class="back face center">
                                                        <div id="image_answer">
                                                            <img src="<?php echo HTTP_SERVER . DIR_TEMPLATE . 'assets/images/pages/test/questions/' . $this->question['id'] . '.jpg'; ?>"
                                                                 alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center btn_next">
                                <button id="next_question" class="disabled" disabled>następne</button>
                            </div>
                        </div>
                    </div>


                    <div class="row bottom_row">

                        <div class="col-md-3 col-xs-12 time">
                            Czas do końca testu:
                            <div id="time" data-timetoevent="<?php echo $this->test['end_date'] ?>"
                                 data-nowtime="<?php echo $this->test['now_date']; ?>">00:00:00
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 progressbox text-center">

                            <div class="box">
                                <div class="text">
                                    Pytanie:
                                    <span class="answeredCount"><?php echo $this->test['answered_count']; ?></span> /
                                    <span class="questionsCount"><?php echo $this->test['count']; ?></span>
                                </div>
                                <div class="progress">
                                    <div id="progressbar" class="progress-bar" role="progressbar"
                                         aria-valuenow="<?php echo $this->test['answered_count']; ?>"
                                         aria-valuemin="1" aria-valuemax="<?php echo $this->test['count']; ?>"
                                         style="width:<?php echo (($this->test['answered_count'] * 100) / $this->test['count']) . '%'; ?>;">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="author_name">Uczeń: <span
                                        id="AuthorName"><?php echo $this->student['name'] ?></span></div>
                        </div>

                    </div>
                </section>
            </div>
        <?php endif; ?>

        <section id="testEnded" class="<?php echo $this->isOver ? 'active' : ''; ?>">

            <div class="top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 icon text-center">
                            <img src="<?php echo HTTP_SERVER . DIR_TEMPLATE . 'assets/images/pages/test/end_icon.png'; ?>"
                                 alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2>Test został zakończony!</h2>
                            <h3>Trzymamy za Ciebie kciuki! Powodzenia!</h3>
                            <p class="end_btn">
                                <button id="finishTest">rozpocznij nowy test</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <?php endif; ?>

</main>
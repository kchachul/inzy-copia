<main id="archivalTests" class="subpage bg_grey">


        <div class="info_box container" <?php echo (count($this->Tests) == 0)?'style="display: block;"':'';?>>
            <div class="row">
                <div class="col-md-12 text-center">
                    <img src="<?php echo HTTP_SERVER . DIR_TEMPLATE . 'assets/images/pages/test/empty_arch.png'; ?>"
                         alt="">
                    <div class="text">
                        <h3 class="title"><?php echo TMP_ARCHIVAL_TESTS_NO_TESTS_TITLE;?></h3>
                        <h4 class="subtitle"><?php echo TMP_ARCHIVAL_TESTS_NO_TESTS_SUBTITLE;?></h4>
                    </div>
                </div>
            </div>
            <div class="row btns">
                <div class="col-md-12 text-center">
                    <a href="<?php echo URL_TEST_CREATOR; ?>" class="btn btn_green ">NOWY TEST</a>
                </div>
            </div>
        </div>

        <div class="container top_page_header" <?php echo (count($this->Tests) == 0)?'style="display: none;"':'';?>>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title"><?php echo TMP_ARCHIVAL_TESTS_TITLE;?></h2>
                    <h3 class="subtitle"><?php echo TMP_ARCHIVAL_TESTS_SUBTITLE;?></h3>
                </div>
            </div>
        </div>

    <div class="container" id="archivalTestsView">

        <div class="list">

            <?php foreach ($this->Tests as $test): ?>
                <div class="test_container test_desc" data-test_id="<?php echo $test['uid']; ?>">
               <div class="head">
                   <div class="row">
                       <div class="col-lg-8 col-md-7 col-xs-12">
                           <div class="name">
                               <i class="fa fa-file-o" aria-hidden="true"></i>
                               <?php echo $test['shortTitle']; ?>
                           </div>
                       </div>
                       <div class="col-lg-1 col-md-1 col-xs-6 text-left">
                           <div class="users">
                               <i class="fa fa-users"></i>
                               <?php echo $test['activeStudents']; ?>
                           </div>
                       </div>
                       <div class="col-lg-2 col-md-3 col-xs-6 text-right">
                           <div class="date">
                               <i class="fa fa-clock-o"></i>
                               <?php echo $test['endDate']; ?>
                           </div>
                       </div>
                   </div>
               </div>

                <div class="body">
                   <div class="top">
                       <div class="row">
                           <div class="col-lg-8 col-md-7 col-xs-12">
                               <div class="subtitle">
                                   <?php echo $test['shortDescription']; ?>
                               </div>
                           </div>
                           <div class="col-lg-3 col-md-4 col-xs-12 pdf-cnt">

                               <a class="btn btn_blue_border pdf" href="<?php echo HTTP_SERVER . URL_TEST_PDF . '?' . URL_TEST_PDF_GET . '=' . $test['uid']; ?>">
                                   <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                   <span class="hidden-xs">POBIERZ WYNIKI</span>
                               </a>
                           </div>
                       </div>
                       <button class="btn del btn_delete_test" data-toggle="tooltip" data-placement="top" title="usuń" data-test_id="<?php echo $test['uid']; ?>">
                           <i class="fa fa-trash" aria-hidden="true"></i>
                       </button>
                   </div>
                    <div class="bottom">
                        <div class="row users_list">
                            <div class="col-md-12 table_users">
                                <div class="row table">

                                    <div class="thead">
                                        <div class="row row_table">
                                            <div class="col-md-3 ">Imię i nazwisko</div>
                                            <div class="col-md-4 ">udzielonych odpowiedzi</div>
                                            <div class="col-md-1 text-center">punkty</div>
                                            <div class="col-md-2 text-center">POPRAWNE</div>
                                            <div class="col-md-2 ">sugerowana ocena</div>
                                        </div>
                                    </div>

                                    <div class="active_users_table">


                                        <?php foreach ($test['students'] as $student): ?>
                                            <div class="row row_table user" data-student_id="<?php echo $student['id']; ?>">

                                                <div class=" col-md-3 first"><?php echo $student['name']; ?></div>
                                                <div class=" col-md-4 col-x-12">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuemin="0"
                                                             aria-valuemax="100"
                                                             data-test_count="100"
                                                             style="width:<?php echo number_format(($student['answered_count'] * 100) / $test['questionsCount'], 2, '.', ''); ?>%">
                                                            <span class="question_count"><?php echo $student['answered_count']; ?></span>/<?php echo $test['questionsCount']; ?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class=" col-md-1 col-xs-4 text-center">
                                                    <span class="student_points"><?php echo $student['points']; ?></span>
                                                    /
                                                    <?php echo $test['questionsCount']; ?>
                                                </div>

                                                <div class=" col-md-2 col-xs-4 text-center">
                                                    <span class="students_points_pr"><?php echo $student['percents']; ?></span>%
                                                </div>

                                                <div class=" col-md-2 col-xs-4 last">
                                                    <span class="student_gradge"><span class="<?php echo $student['gradge']; ?>"></span></span>
                                                </div>

                                                <div class="col-md-12 col-xs-12 student_answers archival">

                                                    <div class="answers_list">


                                                        <?php foreach ($student['questions'] as $key => $question): ?>
                                                            <div class="row answer <?php echo($question['isCorrect'] ? 'correct' : 'wrong'); ?>">
                                                                <div class="col-md-1 col-xs-1"><?php echo $key + 1; ?></div>
                                                                <div class="col-md-5 col-xs-6"><?php echo $question['question']; ?></div>
                                                                <div class="col-md-5 col-xs-3"><?php echo($question['answer'] ? $question['answer'] : '---'); ?></div>
                                                                <div class="col-md-1 col-xs-1 text-center">
                                                                    +<?php echo $question['isCorrect'] == 1 ? $question['isCorrect'] : '0'; ?>
                                                                    pkt
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <?php endforeach; ?>



        </div>


    </div>

</main>
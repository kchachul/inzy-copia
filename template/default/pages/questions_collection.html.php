<main id="questionsCollection" class="subpage bg_grey">


    <div class="container top_page_header">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title"><?php echo TMP_QUESTIONS_COLLECTION_TITLE; ?></h2>
                <h3 class="subtitle"><?php echo TMP_QUESTIONS_COLLECTION_SUBTITLE; ?></h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row questions-list-box">

            <div class="col-md-3 left_col">
                <ul class="nav nav-tabs" role="tablist">
                    <?php $index = 1; ?>
                    <?php if ($this->Lessons): ?>
                        <?php foreach ($this->Lessons as $lesson): ?>
                            <li role="presentation" class="<?php echo(($index++ == 1) ? "active" : ""); ?>"><a
                                        href="#lesson<?php echo $lesson['id']; ?>"
                                        aria-controls="test<?php echo $lesson['id']; ?>"
                                        role="tab" data-toggle="tab"
                                        data-lessonid="<?php echo $lesson['id']; ?>"
                                        data-lessonid="<?php echo $lesson['sceneId']; ?>">
                                    <?php echo $lesson['title']; ?>
                                    <span><?php echo $lesson['description']; ?></span>
                                </a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <li class="add_li">
                        <button class="add" type="button" data-toggle="modal" data-target="#addLessonModal"
                                id="addLesson"
                                data-uid="<?php echo $this->user_id; ?>"><span><?php echo TMP_QUESTIONS_COLLECTION_ADD_CARD;?></span> <i class="fa fa-plus"
                                                                                       aria-hidden="true"></i>
                        </button>
                    </li>
                </ul>
            </div>

            <div class="col-md-9 right_col">
                <div class="top_label">Lista pytań</div>
                <div class="tab-content">
                    <?php $index = 1; ?>
                    <?php foreach ($this->Lessons as $lesson): ?>
                        <div role="tabpanel" class="tab-pane <?php echo(($index++ == 1) ? "active" : ""); ?>"
                             id="lesson<?php echo $lesson['id']; ?>">

                            <?php if ($lesson['teacher_id'] != 0): ?>
                                <div class="description row">
                                    <div class="col-md-12 right">
                                        <div class="options">
                                            <button data-less_id="<?php echo $lesson['id']; ?>"
                                                    type="button" data-toggle="modal" data-target="#addQuestionModal"
                                                    class="addQuestion">
                                                Dodaj nowe pytanie
                                                <i class="fa fa-plus" aria-hidden="true"></i></button>

                                            <button data-less_id="<?php echo $lesson['id']; ?>"
                                                    data-uid="<?php echo $this->user_id; ?>"
                                                    type="button" class="removeLesson">
                                                <i class="fa fa-trash" aria-hidden="true"></i></button>

                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <div class="questions-list">
                                <ul>
                                    <?php foreach ($lesson['scenes'] as $scene): ?>
                                        <?php $question_index = 1; ?>
                                        <?php if (count($scene['questions']) > 0): ?>
                                            <?php foreach ($scene['questions'] as $question): ?>
                                                <li data-question-id="<?php echo $question['id']; ?>" class="quest">
                                                    <div class="top"><?php echo "<span class='number'>".($question_index++) . ".</span> " . $question['title']; ?>
                                                        <?php echo($question['image'] ? '<span class="img"><i class="fa fa-picture-o" aria-hidden="true"></i></span>' : ''); ?>
                                                    </div>
                                                    <div class="bottom">
                                                        <div class="row">
                                                            <div class="<?php echo($question['image'] ? 'col-md-9' : 'col-md-12'); ?>">
                                                                <ul>
                                                                    <?php foreach ($question['answers'] as $key => $answer) { ?>
                                                                        <li class="<?php echo($answer['correct'] ? "correct" : ""); ?>"><?php echo "<span>" . ($key + 1) . "</span>" . $answer['title']; ?></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>


                                                            <?php if ($question['image']): ?>
                                                                <div class="col-md-3">
                                                                    <div class="image-quest text-align">
                                                                        <a href="<?php echo HTTP_SERVER . DIR_TEMPLATE . 'assets/images/pages/test/questions/' . $question['image'] ?>"
                                                                           data-lightbox="questions"
                                                                           data-title="<?php echo $question['question']; ?>">
                                                                            <img src="<?php echo HTTP_SERVER . DIR_TEMPLATE . 'assets/images/pages/test/questions/' . $question['image'] ?>"
                                                                                 alt="">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>

                                                            <?php if ($lesson['teacher_id'] != 0): ?>
                                                                <div class="removebtn">
                                                                    <button data-quest_id="<?php echo $question['id']; ?>"
                                                                            data-uid="<?php echo $this->user_id; ?>"><i
                                                                                class="fa fa-trash"
                                                                                aria-hidden="true"></i>
                                                                    </button>
                                                                </div>
                                                            <?php endif; ?>


                                                        </div>


                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addLessonModal" tabindex="-1" role="dialog" aria-labelledby="addLessonModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="addLessonModalLabel"><?php echo TMP_QUESTIONS_COLLECTION_ADD_LESSON_MODAL; ?></h4>
                </div>
                <div class="modal-body">
                    <form id="addLessonForm">
                        <div class="row">
                            <div class="col-md-2 left"><label>tytuł</label></div>
                            <div class="col-md-10 box">
                                <input type="text" name="title" placeholder="np: Lekcja z symboli narodowych">
                                <i class="fa fa-exclamation error" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 left"><label>opis</label></div>
                            <div class="col-md-10 box">
                                <input type="text" name="description" placeholder="pytania dla klasy 3a">
                                <i class="fa fa-exclamation error" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center btnbox">
                                <button class="btn btn_blue small_pdn" id="addNewLesson" data-uid="<?php echo $this->user_id; ?>"><?php echo TMP_QUESTIONS_COLLECTION_ADD_LESSON;?></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 log"><p></p></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="addQuestionModal" tabindex="-1" role="dialog" aria-labelledby="addQuestionModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addQuestionLabel">Dodawanie nowego pytania</h4>
                </div>
                <div class="modal-body">
                    <form id="questionCreator">
                        <div class="row">
                            <div class="col-md-3 left"><label>Pytanie</label></div>
                            <div class="col-md-9 box">
                                <input type="text" name="question" placeholder="pytanie">
                                <i class="fa fa-exclamation error" aria-hidden="true"></i>
                            </div>
                        </div>

                        <div class="row line">
                            <div class="col-md-3 left"><label class="correct">Odpowiedź a</label></div>
                            <div class="col-md-9 box">
                                <input type="text" name="answer1" placeholder="odpowiedz a">
                                <i class="fa fa-exclamation error" aria-hidden="true"></i>
                                <input class="radio" type="radio" name="num_correct" value="1" checked>
                                <span class="checkmark"></span>
                            </div>
                        </div>

                        <div class="row line">
                            <div class="col-md-3 left"><label>Odpowiedź b</label></div>
                            <div class="col-md-9 box">
                                <input type="text" name="answer2" placeholder="odpowiedz b">
                                <i class="fa fa-exclamation error" aria-hidden="true"></i>
                                <input class="radio" type="radio" name="num_correct" value="2">
                                <span class="checkmark"></span>
                            </div>
                        </div>


                        <div class="row line">
                            <div class="col-md-3 left"><label>Odpowiedź c</label></div>
                            <div class="col-md-9 box">
                                <input type="text" name="answer3" placeholder="odpowiedz c">
                                <i class="fa fa-exclamation error" aria-hidden="true"></i>
                                <input class="radio" type="radio" name="num_correct" value="3">
                                <span class="checkmark"></span>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 btnbox text-center">
                                <button class="btn btn_blue small_pdn addNewQuestion" data-uid="<?php echo $this->user_id; ?>">dodaj nowe pytanie</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <?php // ------------------- templates ------------------- ?>
    <script id="newLessonNavTemplate" type="text/html">
        <li role="presentation" class="active">
            <a href="#lesson{{ lessonId }}" aria-controls="test{{ lessonId }}" role="tab" data-toggle="tab"
               data-lessonid="{{ lessonId }}">
                {{ lessonTitle }} <span>{{ lessonDescription }}</span>
            </a>
        </li>
    </script>


    <script id="newLessonTemplate" type="text/html">

        <div role="tabpanel" class="tab-pane active" id="lesson{{ lessonId }}">


            <div class="description row">
                <div class="col-md-12 right">
                    <div class="options">
                        <button data-less_id="{{ lessonId }}" data-uid="1" type="button" data-toggle="modal"
                                data-target="#addQuestionModal" class="addQuestion">
                            Dodaj nowe pytanie
                            <i class="fa fa-plus" aria-hidden="true"></i></button>

                        <button data-less_id="{{ lessonId }}" data-uid="1" type="button" class="removeLesson">
                            <i class="fa fa-trash" aria-hidden="true"></i></button>

                    </div>
                </div>
            </div>


            <div class="questions-list">
                <ul></ul>
            </div>
        </div>

    </script>

    <script id="newQuestionTemplate" type="text/html">
        <li data-question-id="{{questionId}}" class="quest">
            <div class="top"><span class='number'>{{rowNumber}}.</span> {{ questionTitle }}</div>
            <div class="bottom">
                <ul>
                    <li class="{{ questionAnswer1class }}">{{ questionAnswer1 }}</li>
                    <li class="{{ questionAnswer2class }}">{{ questionAnswer2 }}</li>
                    <li class="{{ questionAnswer3class }}">{{ questionAnswer3 }}</li>
                </ul>
                <div class="removebtn">
                    <button data-quest_id="{{questionId}}" data-uid="1"><i class="fa fa-trash-o" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </li>
    </script>

</main>


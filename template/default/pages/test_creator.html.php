<main id="testCreator" class="subpage bg_grey">

    <div class="container top_page_header">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title"><?php echo TMP_TEST_CREATOR_TITLE;?></h2>
                <h3 class="subtitle"><?php echo TMP_TEST_CREATOR_SUBTITLE;?></h3>
            </div>
        </div>
    </div>


    <div class="container form">
        <form action="<?php echo ACT_ADD_TEST; ?>" id="testCreatorForm">
            <div class="row">


                <div class="col-md-12">
                    <div class="alert_box text-center  <?php echo $this->haveAcviveTest ? 'active' : ''; ?>">
                        <?php echo TMP_TEST_CREATOR_INFO_BOX; ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row row_form first without_bottom">
                        <div class="col-lg-4 col-sm-4 col-xs-4 left"><label for="title">tytuł testu</label></div>
                        <div class="col-lg-8 col-sm-8 col-xs-8 right">
                            <input type="text" name="title" placeholder="np. Test klasy IIIa" class="input_box">
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="row row_form">
                        <div class="col-lg-4 col-sm-4 col-xs-4 left"><label for="description">opis testu</label></div>
                        <div class="col-lg-8 col-sm-8 col-xs-8 right">
                            <input type="text" name="description" placeholder="np. Zaliczenie semestralne"
                                   class="input_box">
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="row row_form">
                        <div class="col-lg-4 col-sm-4 col-xs-4 left"><label for="type">typ testu</label></div>
                        <div class="col-lg-8 col-sm-8 col-xs-8 test_type">
                            <div class="radio_options row">

                                <div class="col-md-4 left">
                                    <label>
                                        <input type="radio" name="type" checked> <span class="label">Standardowe</span>
                                        <span class="check"></span>
                                    </label>
                                </div>
                                <div class="col-md-4 right">
                                    <label>
                                        <input type="radio" name="type"> <span
                                                class="label">Z obrazkami</span>
                                        <span class="check"></span>
                                    </label>
                                </div>
                            </div>
                            <p><?php echo TMP_TEST_CREATOR_TYPE_INFO; ?></p>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="row row_form without_bottom">
                        <div class="col-lg-4 col-sm-4 col-xs-4 left"><label>czas testu</label></div>
                        <div class="col-lg-5 col-sm-5 col-xs-5 slider">
                            <div class="sliderrange" id="test_duration_slider"></div>
                        </div>
                        <div class="col-lg-3  col-sm-3 col-xs-3 last-col">
                            <div id="duration"></div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="row row_form without_top">
                        <div class="col-lg-4 col-sm-4 col-xs-4 left first-col"><label>wielkość grupy: </label></div>
                        <div class="col-lg-5 col-sm-5 col-xs-5 slider">
                            <div class="sliderrange2" id="how_many_people_slider"></div>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-3 last-col">
                            <div id="people"></div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">

                    <div class="row row_form without_bottom" id="6">
                        <div class="col-md-4 col-sm-4 col-xs-4 left"><p><b><label>Wybierz pytanie: </label></b></p></div>
                        <div class="col-md-3  col-sm-3 col-xs-5 right select_questions_submit">
                            <button type="button" class="btn btn_blue_border selectQuestions" data-toggle="modal"
                                    data-target="#modalQuestions"  <?php echo $this->haveAcviveTest ? 'disabled="disabled"':''; ?>>
                                wybrano pytań <span class="badge" id="questLength">0</span>
                            </button>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 add_test_col">
                            <button class="btn btn_green" id="addTest" <?php echo $this->haveAcviveTest ? 'disabled="disabled"':''; ?>>stwórz test</button>
                        </div>
                    </div>
                </div>


            </div>


        </form>
    </div>


    <div class="modal fade" id="modalQuestions" tabindex="-1" role="dialog"
         aria-labelledby="modalQuestionsLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalQuestionsLabel">Wybierz pytania do testu</h4>
                    <div class="selected_questions_count">
                        Ilość wybranych pytań:
                        <span class="badge" id="questions_count">0</span>
                    </div>
                    <div class="lesson_list">
                        <ul class="nav nav-tabs" role="tablist">

                            <?php foreach ($this->Lessons as $key => $lesson): ?>
                                <li role="presentation"
                                    class="<?php echo((!$key) ? "active" : ""); ?>">
                                    <a href="#lesson<?php echo $key; ?>"
                                       aria-controls="lesson<?php echo $key; ?>" role="tab"
                                       data-toggle="tab"><?php echo $lesson['title']; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>

                        </ul>
                    </div>
                </div>


                <div class="modal-body">

                    <div class="tab-content">
                        <?php foreach ($this->Lessons as $key => $lesson): ?>
                            <div role="tabpanel" class="tab-pane <?php echo((!$key) ? "active" : ""); ?>"
                                 id="lesson<?php echo $key; ?>" data-lid="<?php echo $key; ?>">

                                <div class="top_lesson">
                                    <button class='check_lesson'>
                                        <i class="fa fa-square-o" aria-hidden="true"></i> zaznacz wszystkie
                                    </button>
                                    <button class='check_with_image'>
                                        <i class="fa fa-square-o" aria-hidden="true"></i> z obrazkami
                                    </button>
                                    <button class='check_without_image'>
                                        <i class="fa fa-square-o" aria-hidden="true"></i> bez obrazków
                                    </button>
                                </div>

                                <?php if (count($lesson['scenes'][0]['questions']) <= 0): ?>
                                    <div class="no_questions text-center">
                                        <p>Jeszcze nie dodałeś pytań w tej lekcji!</p>
                                        <p>
                                            <a href="<?php echo URL_QUESTIONS_COLLECTION . '?edit=' . $lesson['id']; ?>"
                                               class="btn btn_green smallest_pdn dsb_text_transform">
                                                Dodaj pytania do tego testu</a>
                                        </p>
                                    </div>
                                <?php endif; ?>

                                <div class="questions_list">
                                    <ul>
                                        <?php foreach ($lesson['scenes'] as $key2 => $scene): ?>
                                            <?php foreach ($scene['questions'] as $question): ?>
                                                <li>
                                                    <label class="checkbox_box">
                                                        <input type='checkbox' name='questionsList'
                                                               value="<?php echo $question['id']; ?>"
                                                               class="question <?php echo($question['image'] ? 'with_img' : ''); ?>"
                                                               autocomplete="off">
                                                        <span class="checkmark"></span>
                                                        <?php echo $question['title']; ?>
                                                        <?php echo($question['image'] ? '<i class="fa fa-picture-o" aria-hidden="true"></i>' : ''); ?>
                                                    </label>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </ul>

                                </div>


                            </div>
                        <?php endforeach; ?>


                    </div>

                </div>
            </div>
        </div>
    </div>

</main>
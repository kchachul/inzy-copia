// $(document).ready(function ($) {
const pageLesson = (function ($, window, document) {
  $(window).scrollTop(0)

  /* ------------------- parameters ------------------- */

  var activeSceneId = 1,
    activeLessonId = 1,
    leftMenu = $('#leftMenu'),
    leftMenuWidth = leftMenu.width() + 'px',
    maxScreenSize = {width: 1920, height: 1080},
    animatedElements = [],
    activeScroll = false,
    parallaxEffect = false,
    hashPrefix = 'scene',
    sceneDirUrl = '/lekcje',// + activeLessonId
    loader = $('#loader'),
    greyMenuBg = $('#greyBg'),
    menuAnimSpeed = 300,
    footerContainer = $('#footer'),
    sceneContainer = $('.scene_container'),
    loadingError = '<div class="error_box">Error. Coś poszło nie tak</div>',
    isMobile = deviceIsMobile(),

    /* ------------------- start ------------------- */
    init = function () {
      loadScene()
      moreInfoLeftMenu()
      scrollWindow()
      resizeWindow()
      initListeners()
    }

  /* ------------------- functions ------------------- */

  /**
   * left menu
   */
  function moreInfoLeftMenu () {
    $('#moreInfo').click(function () {
      $('#greyBg').show(0, function () {
        $('#leftMenu').animate({
          left: 0
        }, 300)
      })
    })

    $('#greyBg').click(function () {
      $(this).hide()
      $('#leftMenu').animate({
        left: '-' + $('#leftMenu').width() + 'px'
      }, 300)
    })

    $('button.lesson').click(function () {
      $(this).closest('li').children('ul').slideToggle(200)
    })

    $('.scenes > button').click(function () {
      $(this).closest('li').children('ul').slideToggle(200)
    })
  }

  /**
   * set size fixed scenes
   */
  function setFixedSectionSize () {
    if ($(window).height() > maxScreenSize.height) {

      $('.fixed-container, .section-fixed').css({
        width: maxScreenSize.width + 'px',
        height: maxScreenSize.height + 'px'
      })

      $('.fixed-container').css('margin-left', (-(maxScreenSize.width) / 2) + 'px')

    } else {
      var sceneHeight = $(window).height(),
        sceneWidth = (sceneHeight * 188.63636) / 100

      $('.fixed-container, .section-fixed').css({
        width: sceneWidth + 'px',
        height: sceneHeight + 'px'
      })

      $('.fixed-container').css('margin-left', (-(sceneWidth) / 2) + 'px')
    }
  }

  /**
   * show/hide footer fixed effect
   */
  function setFixedFooter () {
    if ($(window).scrollTop() > ($(window).height() + ($(window).height() / 2))) {
      footerContainer.css({
        zIndex: -2,
        opacity: 1
      })
      $('.fixed-container').css({
        zIndex: -2,
        opacity: 0
      })
    } else {
      footerContainer.css({
        zIndex: -1,
        opacity: 0
      })
      $('.fixed-container').css({
        zIndex: 0,
        opacity: 1
      })
    }
  }

  /**
   * animation effect
   *
   * @param box
   * @param speed
   * @param positionStart
   */
  function elementsAnimation (animObject) {
    var newTopPos = animObject.positionStart - ($(window).scrollTop() * animObject.speed)
    scrollPosBottom = (window.innerHeight + window.scrollY)

    if ((scrollPosBottom > animObject.sectionPosition) && (scrollPosBottom < animObject.sectionPosition + animObject.sectionHeight + ($(window).height() / 2))) {
      $(animObject.element).css({
        top: newTopPos + 'px'
      })
    }
  }

  /**
   * creating a list of elements
   *
   */
  function createListOfAnimatedElements () {
    $('.animated-element').each(function () {
      animatedElements.push({
        element: $(this),
        sectionPosition: $(this).closest('section').position().top,
        sectionHeight: $(this).closest('section').height(),
        positionStart: parseInt($(this).css('top'), 10),
        speed: $(this).data('animation-speed')
      })
    })
    activeScroll = true
  }

  /**
   * get scene id from hash
   *
   * @returns {*}
   */
  function idFromHash () {
    var hash = window.location.hash.substring(1)
    if (hash.indexOf(hashPrefix) != -1) {
      hash = hash.replace(hashPrefix, '')
      if (hash.length < 3 && $.isNumeric(hash)) {
        // alert("mam hash:"+hash);
        return parseInt(hash)
      }
    }
    return false
  }

  function startScene (sceneId) {
    activeSceneId = sceneId
    naviBtnStyles()

    $('#leftMenu .scenes li, #ModalSelectLesson .box').removeClass('active')
    $('#ModalSelectLesson a[data-scene="' + sceneId + '"] .box').addClass('active')
    $('#leftMenu .scenes a[data-scene="' + sceneId + '"]').closest('li').addClass('active')
    $('#navbar .navi-title li').removeClass('active')
    $('#navbar .navi-title li[data-lessonNr=' + sceneId + ']').addClass('active')

    sceneContainer.load(sceneDirUrl + '/lekcja' + activeSceneId, function (response, status) {
      if (status == 'error') {
        sceneContainer.html(loadingError)
        loader.hide()
      } else {
        setFixedSectionSize()
        setInfoBoxes()

        if ($('.parallax').length) {
          var scene = document.getElementById('boxParallax')
          parallaxEffect = new Parallax(scene)
        }

        sceneContainer.attr('id', 'scene' + sceneId)
        createListOfAnimatedElements()

        window.location.hash = hashPrefix + sceneId
        $('#ModalSelectLesson').modal('hide')
        $('#lessonNum').html(sceneId)
        /*
        disabled
        initAudio(sceneId)
        */
        loader.hide()
      }
    })
  }

  /**
   * load scene
   * @param sceneId
   */
  function loadScene (sceneId) {
    loader.show()

    if (typeof sceneId === 'undefined') {
      const hashId = idFromHash()
      if (hashId === false) {
        sceneId = activeSceneId
      } else {
        sceneId = hashId
      }
    }

    $.ajax({
      url: sceneDirUrl + '/lekcja' + sceneId,
      success: function () {
        startScene(sceneId)
      },
      error: function () {
        sceneContainer.html(loadingError)
        loader.hide()
      }
    })
  }

  /**
   * set text to 3 info-boxes
   */
  function setInfoBoxes () {
    const infoBox = $('#scene-info')

    if (typeof infoBoxesText !== 'undefined') {
      infoBox.find('.box.left').find('.back').find('.text').html(infoBoxesText.box1)
      infoBox.find('.box.middle').find('.back').find('.text').html(infoBoxesText.box2)
      infoBox.find('.box.right').find('.url').attr('href', infoBoxesText.box3)
      infoBox.show()
    } else {
      infoBox.hide()
    }
  }

  function blockParallaxEffect () {

    if ((window.innerHeight + window.scrollY) > ($('.section-fixed').height() + $(window).height() / 2)) {
      parallaxEffect.disable()
    } else {
      parallaxEffect.enable()
    }
  }

  /* ------------------- events ------------------- */

  /**
   * init listeners
   */
  function initListeners () {

    $('#ModalSelectLesson a').click(function () {
      loader.show()
      const sceneId = $(this).data('scene')
      loadScene(sceneId)
    })

    $('#leftMenu li.scenes a').click(function () {
      greyMenuBg.hide()
      leftMenu.css('left', '-' + leftMenuWidth).show()
      const sceneId = $(this).data('scene')
      loadScene(sceneId)
    })

    $('#nextNavi').click(function () {
      if ((activeSceneId + 1) <= $('#leftMenu .scenes.active ul.scenes li').length) {
        loadScene(activeSceneId + 1)
      }
    })

    $('#prevNavi').click(function () {
      if ((activeSceneId - 1) >= 1) {
        loadScene(activeSceneId - 1)
      }
    })

  }

  /**
   * set navi btns styles
   */
  function naviBtnStyles () {
    const prevBtn = $('#prevNavi')
    const nextBtn = $('#nextNavi')
    const disabledClass = 'disabled'

    if (activeSceneId === 1) {
      prevBtn.addClass(disabledClass).attr('disabled', true)
    } else {
      prevBtn.removeClass(disabledClass).attr('disabled', false)
    }

    if (activeSceneId === $('#leftMenu .scenes.active ul.scenes li').length) {
      nextBtn.addClass(disabledClass).attr('disabled', true)
    } else {
      nextBtn.removeClass(disabledClass).attr('disabled', false)
    }
  }

  /**
   * scroll into view
   * @param elem
   * @returns {boolean}
   */
  function isScrolledIntoView (elem) {
    const centerY = Math.max(0, (($(window).height() - $(elem).outerHeight()) / 1.25) + $(window).scrollTop())
    const elementTop = $(elem).offset().top
    const elementBottom = elementTop + $(elem).height()
    return elementTop <= centerY && elementBottom >= centerY
  }

  /**
   * check device is mobile
   * @returns {boolean} is mobile
   */
  function deviceIsMobile () {
    let isMobile = false

    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
      || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
      isMobile = true
    } else if ($(window).width() <= 992) {
      isMobile = true
    }
    return isMobile
  }

  /**
   * scroll window
   */
  function scrollWindow () {
    $(window).scroll(function () {
      if (!isMobile) {
        blockParallaxEffect()
        setFixedFooter()

        if (activeScroll) {
          $.each(animatedElements, function (index, element) {
            elementsAnimation(element)
          })
        }

        $('.makeVisible').each(function (index, element) {
          if (isScrolledIntoView(element)) {
            $(element).animate({opacity: 1.0}, 500)
          }
        })
      }
    })
  }

  /**
   * resize window
   */
  function resizeWindow () {
    $(window).resize(function () {
      if (!isMobile) {
        setFixedSectionSize()
      }
    })
  }

  function initAudio (sceneId) {
    let audioUrl = `/template/default/assets/audio/lessons/s${sceneId}_bg.mp3`
    let audioElement = document.getElementById('player')
    audioElement.volume = 1
    audioElement.setAttribute('src', audioUrl)
  }

  return {
    init: init
  }

})(jQuery, window, document)

$(document).ready(function () {
  pageLesson.init()
})
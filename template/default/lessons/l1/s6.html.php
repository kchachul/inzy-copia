<!-- section 1 -->
<section id="section01" class="parallax section section-fixed">
  <div class="fixed-container">
    <!-- pins -->
    <div id="pin1" class="pin text" data-toggle="modal" data-target="#pin1Modal"></div>

    <div class="frame">
      <ul id="boxParallax" class="parallax-list">
        <li class="layer" data-depth="0">
          <img class="parallax-object layer0" name="frames" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/parallax/layer0.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer1" name="floor" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/parallax/layer1.png">
        </li>
        <li class="layer" data-depth="0.05">
          <img class="parallax-object layer2" name="character1" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/parallax/layer2.png">
        </li>
        <li class="layer" data-depth="0.10">
          <img class="parallax-object layer3" name="character2" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/parallax/layer3.png">
        </li>
        <li class="layer" data-depth="0.15">
          <img class="parallax-object layer8" name="sword" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/parallax/layer8.png">
        </li>
        <li class="layer" data-depth="0.20">
          <img class="parallax-object layer4" name="character3" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/parallax/layer4.png">
        </li>
        <li class="layer" data-depth="0.20">
          <img class="parallax-object layer5" name="ribbon1" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/parallax/layer5.png">
        </li>
        <li class="layer" data-depth="0.15">
          <img class="parallax-object layer6" name="ribbon2" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/parallax/layer5.png">
        </li>
        <li class="layer" data-depth="0.05">
          <img class="parallax-object layer7" name="ribbon3" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/parallax/layer5.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer9" name="frames_information" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/parallax/layer9.png">
        </li>
      </ul>
    </div>
  </div>
  <div class="absolute-pos bottom-pos full-width">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>

<!-- section 2 -->
<section id="section02" class="section">
  <div class="main-container container relative" id="container2">

    <div class="text-section makeVisible" id="text2">
      <h2 class="text-blue">Kto rządzi<br/> państwem?</h2>
      <h3>W najdawniejszych czasach naszym państwem rządzili książęta i królowie.</h3>
      <p>Pierwszym władcą Polski był książe Mieszko I. Przez kolejne wieki państwo polskie było królestwem. Kraj był wtedy pod opieką króla. Pierwszym królem Polski był Bolesław Chrobry.</p>
    </div>

    <div class="absolute-pos hidden-sm hidden-xs animated-element" data-animation-speed="0.5" id="circles_color1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s_elements/circles_color1.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec2_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/sec2_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec2_object1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/sec2_object1.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_red.png" alt="">
  </div>
</section>

<!-- section 3 -->
<section id="section03" class="section">
  <div class="main-container container relative" id="container3">
    <div class="text-section text-center text-white" id="text3">
      <h2>Jak jest dzisiaj?</h2>
      <h3></h3>
      <p>
        Dziś władza w naszym kraju wygląda zupełnie inaczej. Najważniejszą osobą w państwie jest prezydent. Sprawuje on władzę wykonawczą, czyli wykonuje ustawy uchwalone przez rząd. Prezydenta wybieramy co pięć lat. W skład parlamentu wchodzi Sejm i Senat. W
        tym pierwszym zasiadają posłowie, a w drugim senatorowie.
      </p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.03" id="sec3_object0">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/sec3_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.06" id="sec3_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/sec3_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec3_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/sec3_object2.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="circles_white">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_white.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white2.png" alt="">
  </div>
</section>

<!-- section 4 -->
<section id="section04" class="section">
  <div class="main-container container relative" id="container4">
    <div class="row">
      <div class="col-md-6 left text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/sec4_icon1.png" alt="" class="icon">
        <h3>Co ile lat wybieramy władzę?</h3>
        <p>Posłów oraz senatorów wybieramy co cztery lata. Prezydenta wybieramy co pięć lat.</p>
      </div>
      <div style class="col-md-6 right text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/sec4_icon2.png" alt="" class="icon">
        <h3>Kto może głosować?</h3>
        <p>Dzieci nie mogą głosować. Robią to osoby dorosłe, które ukończyły 18 lat.</p>
      </div>
    </div>
  </div>
</section>

<!-- section 5 -->
<section id="section05" class="section">
  <div class="main-container container relative" id="container5">
    <div class="text-section text-center makeVisible" id="text5">
      <h3 class="text-blue">Gdzie mieszka<br/> Prezydent?</h3>
      <h4>Prezydent mieszka w zabytkowym pałacu, który mieści się w śródmieściu Warszawy.</h4>
      <p>Jest to największy pałac w Stolicy. Podczas swojej długiej historii był wielokrotnie przebudowywany i remontowany, aby jak najlepiej spełniał swoje funkcje użytkowe. Przed pałacem znajduje się pomnik księcia Józefa Poniatowskiego.</p>
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec5_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/sec5_object1.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec5_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/sec5_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec5_object2">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/sec5_object2.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.2" id="circles_color2">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_color2.png" alt="">
    </div>

  </div>
</section>

<!-- Modals -->
<div class="modal fade" id="pin1Modal" tabindex="-1" role="dialog" aria-labelledby="pin1ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content with_bottom_img">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <h2 class="text-center text-blue">"Szczerbiec"</h2>
        <h3 class="text-center">Miecz koronacyjny królów Polski</h3>
        <p><strong>Unikatowe, jedyne zachowane insygnium koronacyjne. Został on użyty po raz pierwszy podczas koronacji Władysława Łokietka w 1320 r.</strong></p>
        <p>Osoba prowdząca koronację podawała miecz królowi przed nałożeniem korony. Następnie nowy król trzykrotnie kreślił znak krzyża w powietrzu.</p>
        <p>Obecnie szczerbiec przechowywany jest Skarbcu Koronnym w Zamku Królewskim na Wawelu.</p>


      </div>
      <div class="modal-footer">
        <p>Zdjęcie:
          <a rel="nofollow" class="external text" href="https://www.flickr.com/photos/metal_beast/">metal.beast</a> - <a rel="nofollow" class="external free" href="https://www.flickr.com/photos/metal_beast/4982606492/in/photostream/">https://www.flickr.com/photos/metal_beast/4982606492/in/photostream/</a>,
          <a href="https://creativecommons.org/licenses/by-sa/2.0" title="Creative Commons Attribution-Share Alike 2.0">CC BY-SA 2.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=20349079">Link</a>
        </p>
      </div>

      <div class="bottom_image">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s6/pins/s6_pin_img1.png" alt="">
      </div>
    </div>
  </div>
</div>
</div>


<script type="text/javascript">
  var infoBoxesText = {
    box1: "<p>Czy wiesz, że nasz kraj jest państwem pozbawionym insygniów królewskich (korona, jabłko, berło)? Niestety, tuż przed III rozbiorem Prusacy zrabowali je z naszego skarbca na Wawelu. Wszystko przepadło bez śladu. Obecnie w naszych zbiorach narodowych znajdują się szczegółowo zrekonstruowane repliki.</p>",
    box2: `
        <p>
            <ol>
                <li><span>1.</span>W najdawniejszych czasach władzę w naszym państwie sprawowali książęta i królowie.</li>
                <li><span>2.</span>W Polsce władzę sprawuje parlament, prezydent i rząd.</li>
                <li><span>3.</span>Wybory prezydenckie odbywają się raz na pięć lat.</li>
                <li><span>4.</span>Wszystkie osoby, które ukończyły 18 lat mogą głosować w wyborach.</li>
            </ol>
        </p>`,
    box3: '/template/default/assets/pdf/lessons/s6_color_pdf.pdf'
  }
</script>
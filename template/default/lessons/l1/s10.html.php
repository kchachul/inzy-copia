<!-- section 1 -->
<section id="section01" class="parallax section section-fixed">
  <div class="fixed-container">
    <!-- pins -->
    <div id="pin1" class="pin image" data-toggle="modal" data-target="#pin4Modal"></div>
    <div id="pin2" class="pin video" data-toggle="modal" data-target="#pin3Modal"></div>

    <div class="frame">
      <ul id="boxParallax" class="parallax-list">

        <li class="layer" data-depth="0">
          <img class="parallax-object layer0" name="roller_coaster" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/layer0.png">
        </li>
        <li class="layer" data-depth="0.10">
          <img class="parallax-object layer1" name="mountains" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/layer1.png">
        </li>
        <li class="layer" data-depth="0.20">
          <img class="parallax-object layer2" name="clouds" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/layer2.png">
        </li>
        <li class="layer" data-depth="0.15">
          <img class="parallax-object layer3" name="grass1" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/layer3.png">
        </li>
        <li class="layer" data-depth="0.25">
          <img class="parallax-object layer4" name="grass2" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/layer4.png">
        </li>
        <li class="layer" data-depth="0.35">
          <img class="parallax-object layer5" name="house" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/layer5.png">
        </li>
        <li class="layer" data-depth="0.50">
          <img class="parallax-object layer6" name="board" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/layer6.png">
        </li>
        <li class="layer" data-depth="0.45">
          <img class="parallax-object layer7" name="sign" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/layer7.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer8" name="trees" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/layer8.png">
        </li>
        <li class="layer" data-depth="0.70">
          <img class="parallax-object layer9" name="chlopiec" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/layer9.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer10" name="barierka" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/parallax/barierka.png">
        </li>
      </ul>
    </div>
  </div>
  <div class="absolute-pos bottom-pos full-width">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>


<!-- section 2 -->
<section id="section02" class="section">
  <div class="main-container container relative" id="container2">

    <div class="text-section makeVisible" id="text2">
      <h2 class="text-blue">Nasze<br/> wyjątkowe Góry</h2>
      <h3>Polskie góry znajdują się na południu kraju. </h3>
      <p>Są to Góry Świętokrzyskie, Karpaty i Sudety. W obrębie Karpat znajdują się Tatry. Jest to najczęściej odwiedzane pasmo górskie w kraju.</p>

      <p>Na terenie polskich gór znajdują się parki narodowe. Najsłynniejsze z nich to Tatrzański, Świętokrzyski i Bieszczadzki.</p>
    </div>

    <div class="absolute-pos hidden-sm hidden-xs animated-element" data-animation-speed="0.5" id="circles_color1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s_elements/circles_color1.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.3" id="sec2_object1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/sec2_object1.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec2_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/sec2_object0.png" alt="">
    </div>
  </div>
  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>

<!-- section 3 -->
<section id="section03" class="section">
  <div class="main-container container relative" id="container3">
    <div class="row">
      <div class="col-md-12 center text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/sec3_icon_big.png" alt="" class="icon">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 center text-center makeVisible">
        <h3>Sudety</h3>
        <p>Znajdują się w południowo zachodniej Polsce. Północno wschodnia część tego pasma górskiego należy do Polski.</p>
      </div>
      <div class="col-md-4 center text-center makeVisible">
        <h3>Góry Świętokrzyskie</h3>
        <p>Są jednym z najstarszych łańcuchów górskich w Polsce. Znajdują się w centralnej części naszego kraju.</p>
      </div>
      <div class="col-md-4 center text-center makeVisible">
        <h3>Karpaty</h3>
        <p>Wielki łańcuch górski, który znajduje się na południowym wschodze Polski. Zachodnia część Karpat należy do nas.</p>
      </div>
    </div>
  </div>
  <div class="absolute-pos bottom-pos full-width">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>

<!-- section 4 -->
<section id="section04" class="section">
  <div class="main-container container relative" id="container4">
    <div class="text-section text-center makeVisible" id="text4">
      <h3 class="text-blue">Morskie oko</h3>
      <h4>W Tatrach znajduje się Morskie Oko, które jest najsłynniejszym jeziorem górskim w Polsce.</h4>
      <p>Za stolicę polskich gór uznaje się miasto Zakopane. Najwyższym szczytem górskim w naszym kraju są Rysy, a za nimi Babia Góra i Śnieżka.</p>
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec4_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/sec4_object1.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec4_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/sec4_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec4_object2">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s10/sec4_object2.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.2" id="circles_color2">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_color2.png" alt="">
    </div>

  </div>
</section>


<!-- Modals -->
<div class="modal fade" id="pin4Modal" tabindex="-1" role="dialog" aria-labelledby="pin4ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s10/pins/s10_pin_img1.jpg" alt="">
      </div>
      <div class="modal-footer">
        <p>Zdjęcie: <a href="//commons.wikimedia.org/w/index.php?title=User:Wn61&amp;action=edit&amp;redlink=1" class="new" title="User:Wn61 (page does not exist)">Wn61</a> - <span class="int-own-work" lang="pl">Praca własna</span>, <a href="https://creativecommons.org/licenses/by-sa/3.0" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=4454257">Link</a>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pin3Modal" tabindex="-1" role="dialog" aria-labelledby="pin3ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/BiNQB4f_6eY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <div class="modal-footer">
          <p>Film:
            <a rel="nofollow" href="https://www.youtube.com/channel/UC-i1wxyCai8tzWt3M-xT4oA" target="_blank">Fly Promotion (YouTube)</a>
          </p>
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript">
    var infoBoxesText = {
      box1: "<p>Rysy - to nasza duma i najwyższy szczyt, który znajduje się w Tatrach. Liczy aż 2499 metrów! Góra jest położona na granicy Polski i Słowacji. Ze szczytu można obserwować piękne pasmo Tatr Wysokich oraz Bielskich.</p>",
      box2: `
        <p>
            <ol>
                <li><span>1.</span>Polskie góry to Góry Świętokrzyskie, Karpaty oraz Sudety.</li>
                <li><span>2.</span>W Tatrach znajduje się Morskie Oko - nasze najsłynniejsze górskie jezioro.</li>
                <li><span>3.</span>Rysy to najwyższy polski szczyt - aż 2499m!</li>
            </ol>
        </p>`,
      box3: '/template/default/assets/pdf/lessons/s10_color_pdf.pdf'
    }
  </script>
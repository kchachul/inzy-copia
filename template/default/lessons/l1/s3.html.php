<!-- section 1 -->
<section id="section01" class="parallax section section-fixed">
  <div class="fixed-container">
    <!-- pins -->
    <div id="pin1" class="pin image" data-toggle="modal" data-target="#pin4Modal"></div>

    <div class="frame">
      <ul id="boxParallax" class="parallax-list">
        <li class="layer" data-depth="0">
          <img class="parallax-object layer0" name="background" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/parallax/layer0.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer0b" name="floor" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/parallax/layer0b.png">
        </li>
        <li class="layer" data-depth="0.01">
          <img class="parallax-object layer1" name="first_row" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/parallax/layer1.png">
        </li>
        <li class="layer" data-depth="0.05">
          <img class="parallax-object layer2" name="second_row" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/parallax/layer2.png">
        </li>
        <li class="layer" data-depth="0.35">
          <img class="parallax-object layer3" name="teacher_and_boy" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/parallax/layer3.png">
        </li>
        <li class="layer" data-depth="0.15">
          <img class="parallax-object layer4" name="railings" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/parallax/layer4.png">
        </li>
      </ul>
    </div>
  </div>

  <div class="absolute-pos bottom-pos full-width">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>


<!-- section 2 -->
<section id="section02" class="section">
  <div class="main-container container relative" id="container2">

    <div class="text-section makeVisible" id="text2">
      <h2 class="text-blue">Co to<br /> za symbol?</h2>
      <h3>Miejsce, w którym się znajdujesz to gmach Sejmu Rzeczpospolitej Polskiej.</h3>
      <p>Znajduje się on w Warszawie na ulicy Wiejskiej. Tutaj zbierają się politycy, by wspólnie decydować o najważniejszych sprawach naszego państwa. W sali obrad wysoko na ścianie znajduje się inny ważny symbol narodowy. To nasze godło państwowe!</p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.7" id="circles_color1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s_elements/circles_color1.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.4" id="sec2_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec2_object2.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec2_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec2_object1.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec2_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec2_object0.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>

<!-- section 3 -->
<section id="section03" class="section">
  <div class="main-container container relative" id="container3">
    <div class="text-section text-center makeVisible" id="text3">
      <h3 class="text-blue">Co jest<br /> godło państwowe?</h3>
      <h4>Naszym godłem państwowym jest herb z wizerunkiem orła białego umieszczony w czerwonym polu tarczy.</h4>
      <p>Przyjrzyj się dokładnie naszemu orłowi białemu. Ma on dumnie rozwinięte skrzydła, złote szpony oraz złoty dziób, a na głowie która jest zwrócona w prawo - przepiękną złotą koronę.</p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec3_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec3_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec3_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec3_object2.png" alt="">
    </div>
  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_red.png" alt="">
  </div>
</section>

<!-- section 4 -->
<section id="section04" class="section">

  <div class="main-container container relative" id="container4">
    <div class="text-section text-center text-white" id="text4">
      <h2>Gdzie mogę zobaczyć </br>nasze godło?</h2>
      <h3>Miejsc, w których można dostrzec nasze godło jest tak naprawdę wiele. Część z nich pewnie już dobrze kojarzysz.</h3>
      <p>
        Godło Polski znajduje się we wszystkich urzędach, sądach i szkołach. Pojawia się także na polskich monetach, dokumentach, mundurach i koszulkach sportowców reprezentujących nasz kraj na zawodach oraz olimpiadach. Godło, podobnie jak flaga obecne jest
        podczas ważnych uroczystości państwowych. Dbaj o to, aby traktować godło z należytym szacunkiem.
      </p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.03" id="sec4_object0">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec4_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec4_object3">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec4_object3.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec4_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec4_object2.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec4_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec4_object1.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.25" id="circles_white">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_white.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white2.png" alt="">
  </div>
</section>

<!-- section 5 -->
<section id="section05" class="section">
  <div class="main-container container relative" id="container5">
    <div class="text-section text-center makeVisible" id="text5">
      <h3 class="text-blue">Dlaczego<br /> orzeł biały?</h3>
      <h4>Wszystko zaczęło się od pewnej legendy.</h4>
      <p>Legenda głosi, że założycielem państwa Polan był Lech, który podczas pieszej wędrówki zatrzymał się niedaleko Poznania. Tam zobaczył ogromne drzewo z gniazdem na szczycie. W gnieździe zaś ujrzał białego orła i pisklęta. W pewnym momencie oczom Lecha ukazał się niezwykły widok. Na tle zachodzącego, czerwonego słońca biały ptak rozstawił szeroko swe skrzydła. Mężczyznę zachwyciło, to co zobaczył. Postanowił, że założy gród w tym miejscu, a w jego herbie umieści białego orła. Swoją osadę nazwał Gniezdnem (od słowa gniazdo). 
      </p>

        <p>Ten piękny ptak w koronie to symbol dumy, siły, potęgi, a jego biel jest wyrazem dobroci. Czerwone tło przypomina nam o dostojności. </p>
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.15" id="sec5_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec5_object1.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.4" id="sec5_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec5_object2.png" alt="">
    </div>

    <div class="absolute-pos" data-animation-speed="0.1" id="sec5_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec5_object0.png" alt="">
    </div>

    <div class="absolute-pos" data-animation-speed="0.1" id="sec5_object3">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s3/sec5_object3.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.4" id="circles_color2">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_color2.png" alt="">
    </div>

  </div>
</section>

<!-- Modals -->
<div class="modal fade" id="pin4Modal" tabindex="-1" role="dialog" aria-labelledby="pin4ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s3/pins/s3_pin_img1.jpg" alt="">
      </div>
      <div class="modal-footer">
        <p>Zdjęcie: <a href="//commons.wikimedia.org/wiki/User:Network.nt" title="User:Network.nt">Network.nt</a>, <a href="https://creativecommons.org/licenses/by/2.5" title="Creative Commons Attribution 2.5">CC BY 2.5</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=2259744">Link</a>
        </p>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  var infoBoxesText = {
    box1: "<p>W zbiorach Muzeum Narodowego w Warszawie znajduje się unikalny obraz zatytułowany „Bitwa pod Orszą”. Można na nim dostrzec jedną z najstarszych polskich chorągwi z orłem białym.</p>",
    box2: `
        <p>
            <ol>
                <li><span>1.</span>Gmach Sejmu Rzeczypospolitej Polskiej znajduje się w Warszawie przy ul. Wiejskiej.</li>
                <li><span>2.</span>Naszym godłem jest wizerunek orła białego w złotej koronie na czerwonym tle.</li>
                <li><span>3.</span>Pamiętaj, że orzeł na naszym godle zwrócony jest dziobem w prawą w stronę!</li>
            </ol>
        </p>`,
    box3: '/template/default/assets/pdf/lessons/s3_color_pdf.pdf'
  }
</script>
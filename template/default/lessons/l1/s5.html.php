<!-- section 1 -->
<section id="section01" class="parallax section section-fixed">
  <div class="fixed-container">
    <!-- pins -->
    <div id="pin1" class="pin image" data-toggle="modal" data-target="#pin4Modal"></div>

    <div class="frame">
      <ul id="boxParallax" class="parallax-list">
        <li class="layer" data-depth="0">
          <img class="parallax-object layer0" name="background" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/parallax/layer0.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer1" name="floor" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/parallax/layer1.png">
        </li>
        <li class="layer" data-depth="0.05">
          <img class="parallax-object layer2" name="cupboard" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/parallax/layer2.png">
        </li>
        <li class="layer" data-depth="0.10">
          <img class="parallax-object layer3" name="boy" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/parallax/layer3.png">
        </li>
        <li class="layer" data-depth="0.30">
          <img class="parallax-object layer4" name="grandfather" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/parallax/layer4.png">
        </li>
        <li class="layer" data-depth="0.50">
          <img class="parallax-object layer5" name="biurko" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/parallax/layer5.png">
        </li>
      </ul>
    </div>
  </div>

  <div class="absolute-pos bottom-pos full-width">
     <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>


<!-- section 2 -->
<section id="section02" class="section">
  <div class="main-container container relative" id="container2">

    <div class="text-section makeVisible" id="text2">
      <h2 class="text-blue">Gdzie<br /> znajduje się Polska?</h2>
      <h3>Polska znajduje się w samym środku Europy.</h3>
      <p>Graniczy z siedmioma innymi państwami, których nazywamy swoimi „sąsiadami”. Na zachodzie są to Niemcy, na wschodzie Białoruś i Ukraina, na północy Rosja i Litwa, a na południu Czechy i Słowacja.</p>
    </div>

    <div class="absolute-pos hidden-sm hidden-xs animated-element" data-animation-speed="0.5" id="circles_color1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s_elements/circles_color1.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec2_object1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/sec2_object1.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec2_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/sec2_object0.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
     <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white2.png" alt="">
  </div>
</section>


<!-- section 3 -->
<section id="section03" class="section">
  <div class="main-container container relative" id="container3">
    <div class="row">
      <div class="col-md-6 left text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/sec3_icon1.png" alt="" class="icon">
        <h3>Północ - nasze morze</h3>
        <p>Od strony północnej naszą granicę tworzy urokliwe wybrzeże Morza Bałtyckiego.</p>
      </div>
      <div style class="col-md-6 right text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/sec3_icon2.png" alt="" class="icon">
        <h3>Południe - nasze góry</h3>
        <p>Od strony południowej naszą granicę tworzą przepiękne góry.</p>
      </div>
    </div>
  </div>
  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_red.png" alt="">
  </div>
</section>

<!-- section 4 -->
<section id="section04" class="section">

  <div class="main-container container relative" id="container4">
    <div class="text-section text-center text-white" id="text4">
      <h2>Warszawa - stolica Polski</h2>
      <h3>Każdy kraj ma swoją stolicę</h3>
      <p>
        Na mapie kolorem czerwonym została oznaczona nasza stolica. Stolica państwa to miasto, które jest siedzibą władz kraju. Naszą stolicą jest Warszawa. Tutaj znajduje się Pałac Prezydencki i urzęduje Sejm Rzeczpospolitej Polskiej. Więcej o tym wyjątkowym mieście dowiesz się na kolejnych lekcjach.
      </p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.03" id="sec4_object0">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/sec4_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.10" id="sec4_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/sec4_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec4_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/sec4_object2.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec4_object3">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/sec4_object3.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="circles_white">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_white.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white2.png" alt="">
  </div>
</section>

<!-- section 5 -->
<section id="section05" class="section">
  <div class="main-container container relative" id="container5">
    <div class="text-section text-center makeVisible" id="text5">
      <h3 class="text-blue">Ponad 38 milionów<br />biało-czerwonych serc!</h3>
      <h4>Czy wiesz, że obszar naszego kraju zamieszkuje ponad 38 milionów polskich serc?</h4>
      <p>Jako Polak jesteś częścią wspólnoty, którą łączy na wspólna historia, wspólny język, piękne wspólne tradycje oraz zwyczaje. Cieszymy się, że jesteś z nami! </p>
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec5_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/sec5_object1.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec5_object2">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s5/sec5_object2.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.2" id="circles_color2">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_color2.png" alt="">
    </div>

  </div>
</section>

<!-- Modals -->
<div class="modal fade" id="pin4Modal" tabindex="-1" role="dialog" aria-labelledby="pin4ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s5/pins/s5_pin_img1.jpg" alt="">
      </div>
      <div class="modal-footer">
        <p>Zdjęcie: <a href="//commons.wikimedia.org/wiki/User:TUBS" title="User:TUBS">TUBS</a>
          <a href="//commons.wikimedia.org/wiki/User_talk:TUBS" title="User talk:TUBS"></a>
          <a href="http://toolserver.org/~daniel/WikiSense/Gallery.php?wikifam=commons.wikimedia.org&amp;img_user_text=TUBS" title="Gallery" rel="nofollow"></a> - <span class="int-own-work" lang="en">Own work</span>
          <a href="//commons.wikimedia.org/wiki/File:Adobe-un.svg" title="File:Adobe-un.svg"></a>This W3C-unspecified <a href="https://en.wikipedia.org/wiki/Vector_images" class="extiw" title="w:Vector images">vector image</a> was created with <a href="https://en.wikipedia.org/wiki/Adobe_Illustrator" class="extiw" title="w:Adobe Illustrator">Adobe Illustrator</a>.
          <a
          href="//commons.wikimedia.org/wiki/File:Commonist.svg" title="File:Commonist.svg"></a>This file was uploaded with <a href="//commons.wikimedia.org/wiki/Commonist" class="mw-redirect" title="Commonist">Commonist</a>.
            <a href="//commons.wikimedia.org/wiki/File:SVG_in_SVG.svg" class="image"></a>This <a href="https://en.wikipedia.org/wiki/vector_image" class="extiw" title="w:vector image">vector image</a> includes elements that have been taken or adapted from this:
            <a href="//commons.wikimedia.org/wiki/File:Europe_laea_location_map.svg"
            class="image"></a>&nbsp;<a href="//commons.wikimedia.org/wiki/File:Europe_laea_location_map.svg" title="File:Europe laea location map.svg">Europe laea location map.svg</a> (by&nbsp;<a href="//commons.wikimedia.org/wiki/User:Alexrk2" title="User:Alexrk2">Alexrk2</a>).
            <a
            href="//commons.wikimedia.org/wiki/File:SVG_in_SVG.svg" class="image"></a>This <a href="https://en.wikipedia.org/wiki/vector_image" class="extiw" title="w:vector image">vector image</a> includes elements that have been taken or adapted from this:
              <a href="//commons.wikimedia.org/wiki/File:BlankMap-World6.svg"
              class="image"></a>&nbsp;<a href="//commons.wikimedia.org/wiki/File:BlankMap-World6.svg" title="File:BlankMap-World6.svg">BlankMap-World6.svg</a> (by&nbsp;<a href="//commons.wikimedia.org/w/index.php?title=User:Canuckguy&amp;action=edit&amp;redlink=1"
              class="new" title="User:Canuckguy (page does not exist)">Canuckguy</a>).
              <a href="//commons.wikimedia.org/wiki/File:SVG_in_SVG.svg" class="image"></a>This <a href="https://en.wikipedia.org/wiki/vector_image" class="extiw" title="w:vector image">vector image</a> includes elements that have been taken or adapted from this:
              <a href="//commons.wikimedia.org/wiki/File:Azerbaijan_location_map.svg"
              class="image"></a>&nbsp;<a href="//commons.wikimedia.org/wiki/File:Azerbaijan_location_map.svg" title="File:Azerbaijan location map.svg">Azerbaijan location map.svg</a> (by&nbsp;<a href="//commons.wikimedia.org/wiki/User:Uwe_Dedering" title="User:Uwe Dedering">Uwe Dedering</a>).
              <a
              href="//commons.wikimedia.org/wiki/File:SVG_in_SVG.svg" class="image"></a>This <a href="https://en.wikipedia.org/wiki/vector_image" class="extiw" title="w:vector image">vector image</a> includes elements that have been taken or adapted from this:
                <a href="//commons.wikimedia.org/wiki/File:Moldova_location_map.svg"
                class="image"></a>&nbsp;<a href="//commons.wikimedia.org/wiki/File:Moldova_location_map.svg" title="File:Moldova location map.svg">Moldova location map.svg</a> (by&nbsp;<a href="//commons.wikimedia.org/wiki/User:NordNordWest" title="User:NordNordWest">NordNordWest</a>).
                <a
                href="//commons.wikimedia.org/wiki/File:SVG_in_SVG.svg" class="image"></a>This <a href="https://en.wikipedia.org/wiki/vector_image" class="extiw" title="w:vector image">vector image</a> includes elements that have been taken or adapted from this:
                  <a href="//commons.wikimedia.org/wiki/File:Cyprus_location_map.svg"
                  class="image"></a>&nbsp;<a href="//commons.wikimedia.org/wiki/File:Cyprus_location_map.svg" title="File:Cyprus location map.svg">Cyprus location map.svg</a> (by&nbsp;<a href="//commons.wikimedia.org/wiki/User:NordNordWest" title="User:NordNordWest">NordNordWest</a>).
                  <a
                  href="//commons.wikimedia.org/wiki/File:SVG_in_SVG.svg" class="image"></a>This <a href="https://en.wikipedia.org/wiki/vector_image" class="extiw" title="w:vector image">vector image</a> includes elements that have been taken or adapted from this:
                    <a href="//commons.wikimedia.org/wiki/File:Georgia_location_map.svg"
                    class="image"></a>&nbsp;<a href="//commons.wikimedia.org/wiki/File:Georgia_location_map.svg" title="File:Georgia location map.svg">Georgia location map.svg</a> (by&nbsp;<a href="//commons.wikimedia.org/wiki/User:NordNordWest" title="User:NordNordWest">NordNordWest</a>).,
                    <a href="https://creativecommons.org/licenses/by-sa/3.0" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=14634326">Link</a>
        </p>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
  var infoBoxesText = {
    box1: "<p>Czy wiesz, że Polska przez 123 lata nie istniała na mapach świata? Ten ciężki dla naszego państwa czas nastąpił po trzecim rozbiorze Polski. Nasz kraj został podzielony pomiędzy Rosję, Prusy oraz Austrię. Zaraz po zakończeniu pierwszej wojny światowej wiele narodów, w tym Polska odzyskała niepodległość. Stąd dzień 11 listopada jest naszym narodowym świętem!</p>",
    box2: `
        <p>
            <ol>
                <li><span>1.</span>Polska leży w środku Europy i graniczy z siedmioma państwami.</li>
                <li><span>2.</span>Podział administracyjny naszego Państwa obejmuje 16 województw.</li>
                <li><span>3.</span>Na północy naszego kraju znajduje się Morze Bałtykcie, a na południu góry.</li>
                <li><span>4.</span>Nasz kraj zamieszkuje ponad 38 mln ludzi.</li>
            </ol>
        </p>`,
    box3: '/template/default/assets/pdf/lessons/s5_color_pdf.pdf'
  }
</script>
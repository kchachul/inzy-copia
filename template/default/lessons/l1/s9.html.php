<!-- section 1 -->
<section id="section01" class="parallax section section-fixed">
  <div class="fixed-container">
    <!-- pins -->
    <div id="pin1" class="pin image" data-toggle="modal" data-target="#pin4Modal"></div>
    <div id="pin2" class="pin video" data-toggle="modal" data-target="#pin3Modal"></div>

    <div class="frame">
      <ul id="boxParallax" class="parallax-list">

        <li class="layer" data-depth="0.05">
          <img class="parallax-object layer0" name="clouds" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer0.png">
        </li>
        <li class="layer" data-depth="0.10">
          <img class="parallax-object layer1" name="airplane" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer1.png">
        </li>
        <li class="layer" data-depth="0.15">
          <img class="parallax-object layer2" name="sea" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer2.png">
        </li>
        <li class="layer" data-depth="0.25">
          <img class="parallax-object layer3" name="boat1" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer3.png">
        </li>
        <li class="layer" data-depth="0.35">
          <img class="parallax-object layer4" name="buoy" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer4.png">
        </li>
        <li class="layer" data-depth="0.15">
          <img class="parallax-object layer5" name="boat2" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer5.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer6" name="water2" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer6.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer7" name="sand" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer7.png">
        </li>
        <li class="layer" data-depth="0.25">
          <img class="parallax-object layer8" name="pail" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer8.png">
        </li>
        <li class="layer" data-depth="0.35">
          <img class="parallax-object layer9" name="umbrella" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer9.png">
        </li>
        <li class="layer" data-depth="0.70">
          <img class="parallax-object layer10" name="boy" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer10.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer11" name="screen" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/parallax/layer11.png">
        </li>
      </ul>
    </div>
  </div>
  <div class="absolute-pos bottom-pos full-width">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>


<!-- section 2 -->
<section id="section02" class="section">
  <div class="main-container container relative" id="container2">
    <div class="text-section makeVisible" id="text2">
      <h2 class="text-blue">Bałtyk -<br /> poznaj uroki<br>polskiego morza</h2>
      <h3>Północną granicę Polski tworzy wybrzeże Bałtyku. Oznacza to, że jesteśmy krajem z dostępem do morza. Możemy z tego czerpać korzyści gospodarcze i turystyczne.</h3>
      <p>Polskie regiony nadbałtyckie są odwiedzane przez turystów z kraju i z zagranicy. Atrakcje dla nich stanowią nie tylko piaszczyste plaże. Znajduje się tu Słowiński Park Narodowy, którego wizytówką są sosnowe lasy oraz ruchome wydmy.</p>
    </div>

    <div class="absolute-pos hidden-sm hidden-xs animated-element" data-animation-speed="0.5" id="circles_color1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s_elements/circles_color1.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.3" id="sec2_object1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/sec2_object1.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec2_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/sec2_object0.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_red.png" alt="">
  </div>
</section>

<!-- section 3 -->
<section id="section03" class="section">

  <div class="main-container container relative" id="container3">
    <div class="text-section text-center text-white" id="text3">
      <h2>Dla nadmorskiej gospodarki</br>największe znaczenie mają porty.</h2>
      <p>Najważniejsze miasta portowe w Polsce to Gdańsk, Gdynia, Szczecin i Świnoujście. Tutaj odbywa się wymiana handlowa oraz przypływają i wypływają statki. Dzięki dostępowi do morza, polscy rybacy mogą łowić i sprzedawać ryby. Jest to jeden z najstarszych
        i najcięższych zawodów.
      </p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.03" id="sec3_object0">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/sec3_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec3_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/sec3_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec3_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/sec3_object2.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec3_object3">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/sec3_object3.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="circles_white">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_white.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white2.png" alt="">
  </div>
</section>

<!-- section 4 -->
<section id="section04" class="section">
  <div class="main-container container relative" id="container4">
    <div class="row">
      <div class="col-md-6 left text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/sec4_icon1.png" alt="" class="icon">
        <h3>Półwysep Helski</h3>
        <p>Ten cypelek na północy to nasz jedyny półwysep nazwany Helskim. Jego długość to aż 35km! Został utworzony przez silny prąd morski na przestrzeni lat. Każdego lata to miejsce odwiedza tysiące polskich turystów!</p>
      </div>
      <div style class="col-md-6 right text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s9/sec4_icon2.png" alt="" class="icon">
        <h3>Dwie wyspy</h3>
        <p>Uznam i Wolin to przybrzeżne wyspy Morza Bałtyckiego. Wyspa Wolin jest największą wyspą Polski (265 km2). Większa część wyspy Uznam należy do naszych niemieckich sąsiadów, do nas należy tylko wschodnia jej część (72 km2).</p>
      </div>
    </div>
  </div>
</section>

<!-- Modals -->
<div class="modal fade" id="pin4Modal" tabindex="-1" role="dialog" aria-labelledby="pin4ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s9/pins/s9_pin_img1.jpg" alt="">
      </div>
      <div class="modal-footer">
        <p>Zdjęcie: <a href="//commons.wikimedia.org/wiki/User:Ejdzej" title="User:Ejdzej">Ejdzej</a> - <span class="int-own-work" lang="pl">Praca własna</span>, Domena publiczna, <a href="https://commons.wikimedia.org/w/index.php?curid=963603">Link</a>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pin3Modal" tabindex="-1" role="dialog" aria-labelledby="pin3ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/cBURPUjQFgM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <div class="modal-footer">
          <p>Film:
            <a rel="nofollow" href="https://www.youtube.com/channel/UCIJzLOvVLv3tzPDS4oyJvlA" target="_blank">Władysław Labuda (YouTube)</a>
          </p>
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript">
    var infoBoxesText = {
      box1: "<p>Czy wiesz, że aż 12.5% naszej granicy to właśnie linia brzegowa Morza Bałtyckiego? Jest to aż 440km! </p>",
      box2: `
        <p>
            <ol>
                <li><span>1.</span>Największe miasta portowe w Polsce to Gdańsk, Gdynia, Szczecin i Świnoujście.</li>
                <li><span>2.</span>Na polskim wybrzeżu znajduje się piękny Słowiński Park Narodowy.</li>
                <li><span>3.</span>Nasz jedyny polski półwysep to Półwysep Helski.</li>
                <li><span>4.</span>Mamy dwie polskie wyspy, jest to Wolin i Uznam, które znajdują się w zachodniej części wybrzeża.</li>
            </ol>
        </p>`,
      box3: '/template/default/assets/pdf/lessons/s9_color_pdf.pdf'
    }
  </script>
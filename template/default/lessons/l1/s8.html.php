<!-- section 1 -->
<section id="section01" class="parallax section section-fixed">
  <div class="fixed-container">
    <!-- pins -->
    <div id="pin1" class="pin image" data-toggle="modal" data-target="#pin4Modal"></div>
    <div id="pin2" class="pin video" data-toggle="modal" data-target="#pin3Modal"></div>

    <div class="frame">
      <ul id="boxParallax" class="parallax-list">
        <li class="layer" data-depth="0.05">
          <img class="parallax-object layer0" name="clouds" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/parallax/layer0.png">
        </li>
        <li class="layer" data-depth="0.10">
          <img class="parallax-object layer1" name="city" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/parallax/layer1.png">
        </li>
        <li class="layer" data-depth="0.15">
          <img class="parallax-object layer2" name="grass" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/parallax/layer2.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer3" name="grass2" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/parallax/layer3.png">
        </li>
        <li class="layer" data-depth="0.10">
          <img class="parallax-object layer4" name="tree" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/parallax/layer4.png">
        </li>
        <li class="layer" data-depth="0.10">
          <img class="parallax-object layer5" name="tree2" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/parallax/layer5.png">
        </li>
        <li class="layer" data-depth="0.30">
          <img class="parallax-object layer6" name="camp" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/parallax/layer6.png">
        </li>
        <li class="layer" data-depth="0.50">
          <img class="parallax-object layer7" name="boy" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/parallax/layer7.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer8" name="stones" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/parallax/layer8.png">
        </li>
      </ul>
    </div>
  </div>
  <div class="absolute-pos bottom-pos full-width">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>


<!-- section 2 -->
<section id="section02" class="section">
  <div class="main-container container relative" id="container2">

    <div class="text-section makeVisible" id="text2">
      <h2 class="text-blue">Najdłuższe<br /> rzeki w Polsce</h2>
      <h3>Najdłuższe rzeki w naszym kraju to Wisła, Odra i Warta.</h3>
      <p>Rzeki mają swoje źródła głęboko w górach. Woda, która znajduje się pod ziemią przebija się przez skały na powierzchnię tworząc malutkie strumyki, a następnie przez wydrążone przez wieki koryta kieruje się do ujścia, którym zazwyczaj jest jakiś duży
        zbiornik wodny. Może to być morze lub jezioro. Są nieodłącznym elementem naszego krajobrazu, potrafią spokojnie płynąć pokonując setki kilometrów. Przepływają przez liczne polskie miasta.</p>
    </div>

    <div class="absolute-pos hidden-sm hidden-xs animated-element" data-animation-speed="0.5" id="circles_color1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s_elements/circles_color1.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec2_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/sec2_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec2_object1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/sec2_object1.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>


<!-- section 3 -->
<section id="section03" class="section">
  <div class="main-container container relative" id="container3">
    <div class="row">
      <div class="col-md-12 center text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/sec3_icon1_big.png" alt="" class="icon">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 center text-center makeVisible">
        <h3>Wisła - 1047 km</h3>
        <p>Nazywana jest „królową polskich rzek”. Swoje źródło ma w górach na południu kraju. Płynie przez liczne polskie miasta. Wśród nich są np. Kraków, Warszawa i Gdańsk.</p>
      </div>
      <div class="col-md-4 center text-center makeVisible">
        <h3>Odra - 854 km</h3>
        <p>Odra to druga pod względem długości rzeka w Polsce. Wypływa z Czech. Największe miasta nad Odrą to Wrocław, Szczecin i Opole.</p>
      </div>
      <div class="col-md-4 center text-center makeVisible">
        <h3>Warta - 808 km</h3>
        <p>Trzecią najdłuższą rzeką w Polsce jest Warta. Najbardziej znane miasta leżące nad jej brzegiem to Częstochowa, Poznań i Gorzów Wielkopolski.</p>
      </div>
    </div>
  </div>
  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white2.png" alt="">
  </div>
</section>

<!-- section 4 -->
<section id="section04" class="section">
  <div class="main-container container relative" id="container4">
    <div class="text-section text-center makeVisible" id="text4">
      <h3 class="text-blue">Które rzeki wpływają<br /> do Morza Bałtyckiego?</h3>
      <h4>Wisła i Odra są rzekami, które wpływają do Morza bałtyckiego.</h4>
      <p> Warta do nich nie należy. Warta wpływa na teren Parku Narodowego "Ujście Warty", gdzie następnie uchodzi do Odry.</p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec4_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/sec4_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec4_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/sec4_object2.png" alt="">
    </div>
  </div>
</section>

<!-- section 5 -->
<section id="section05" class="section">
  <div class="main-container container relative" id="container5">
    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec5_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s8/sec5_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.2" id="circles_color2">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_color2.png" alt="">
    </div>

  </div>
</section>


<!-- Modals -->
<div class="modal fade" id="pin4Modal" tabindex="-1" role="dialog" aria-labelledby="pin4ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s8/pins/s8_pin_img1.jpg" alt="">
      </div>
      <div class="modal-footer">
        <p>Zdjęcie: <a href="//commons.wikimedia.org/wiki/User:Wojsyl" class="mw-redirect" title="User:Wojsyl">Wojsyl</a> - <span class="int-own-work" lang="pl">Praca własna</span>, <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>,
          <a href="https://commons.wikimedia.org/w/index.php?curid=327219">Link</a>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pin3Modal" tabindex="-1" role="dialog" aria-labelledby="pin3ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/1OkxJN_wg3U" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <div class="modal-footer">
          <p>Film:
            <a rel="nofollow" href="https://www.youtube.com/channel/UCOjz8VQnFuaioTzBfwjd_yw" target="_blank">Lubię Kujawsko Pomorskie (YouTube)</a>
          </p>
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript">
    var infoBoxesText = {
      box1: "<p>Czy wiesz, że historyczne miasta były zakładane nad rzekami głównie z powodu dostarczania wody oraz pożywienia w postaci ryb. Rzeka, która oplatała lub przepływała przez miasto również spełnia ważna funkcję obronną oraz umożliwiała sprawny transport różnych towarów.</p>",
      box2: `
        <p>
            <ol>
                <li><span>1.</span>Najdłuższą rzeką w Polsce jest Wisła, drugie miejsce w rankingu długości zajmuje Odra, a zaraz za nią Warta.</li>
                <li><span>2.</span>Odra jest jedną z polskich rzek, która nie ma swojego źródła w Polsce.</li>
                <li><span>3.</span>Wisła oraz Odra wpływają do Morza Bałtyckiego.</li>
            </ol>
        </p>`,
      box3: '/template/default/assets/pdf/lessons/s8_color_pdf.pdf'
    }
  </script>
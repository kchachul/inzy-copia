<!-- section 1 -->
<section id="section01" class="parallax section section-fixed">
  <div class="fixed-container">
    <!-- pins -->
    <div id="pin1" class="pin image" data-toggle="modal" data-target="#pin4Modal"></div>
    <div id="pin2" class="pin image" data-toggle="modal" data-target="#pin4Modal2"></div>

    <div class="frame">
      <ul id="boxParallax" class="parallax-list">
        <li class="layer" data-depth="0">
          <img class="parallax-object layer0" name="city" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/parallax/layer0.png">
        </li>
        <li class="layer" data-depth="0.05">
          <img class="parallax-object layer1" name="bridge" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/parallax/layer1.png">
        </li>
        <li class="layer" data-depth="0.10">
          <img class="parallax-object layer2" name="trees" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/parallax/layer2.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer3" name="street" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/parallax/layer3.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer4" name="castle" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/parallax/layer4.png">
        </li>
        <li class="layer" data-depth="0.30">
          <img class="parallax-object layer5" name="clouds" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/parallax/layer5.png">
        </li>
        <li class="layer" data-depth="0.15">
          <img class="parallax-object layer6" name="monument" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/parallax/layer6.png">
        </li>
        <li class="layer" data-depth="0.50">
          <img class="parallax-object layer7" name="boy" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/parallax/layer7.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer8" name="balustrade" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/parallax/layer8.png">
        </li>
      </ul>
    </div>
  </div>

  <div class="absolute-pos bottom-pos full-width">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>


<!-- section 2 -->
<section id="section02" class="section">
  <div class="main-container container relative" id="container2">

    <div class="text-section makeVisible" id="text2">
      <h2 class="text-blue">Warszawa -<br /> Stolica Polski</h2>
      <h3>Stolica państwa to miasto, które jest siedzibą władz kraju.</h3>
      <p>Naszą stolicą jest Warszawa. Tutaj znajduje się Pałac Prezydencki i urzęduje Sejm Rzeczpospolitej Polskiej. Z powstaniem miasta wiąże się słynna legenda o Warsie i Sawie. Na herbie miasta znajduje się syrena z mieczem i tarczą. Przez Warszawę przepływa rzeka Wisła.</p>
    </div>

    <div class="absolute-pos hidden-sm hidden-xs animated-element" data-animation-speed="0.5" id="circles_color1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s_elements/circles_color1.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec2_object1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec2_object1.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec2_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec2_object0.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_red.png" alt="">
  </div>
</section>

<!-- section 3 -->
<section id="section03" class="section">

  <div class="main-container container relative" id="container3">
    <div class="text-section text-center text-white" id="text3">
      <h2>Wars i Sawa</h2>
      <h3>Wanda Chotomska</h3>

      <p>
        Dawno, bardzo dawno temu nad brzegiem Wisły mieszkał młody rybak Wars. Któregoś dnia, gdy szedł nad rzekę, by zanurzyć sieci usłyszał piosenkę:
      </p>
      <p>
        Siedem fal mnie strzeże
        <br /> i siedem błyskawic.
        <br /> Kto się ich nie lęka
        <br /> niech się tutaj zjawi.
        <br />
      </p>
      <p>
        Piosenkę śpiewała dziewczyna, a głos miała tak piękny, słodki i dźwięczny, że Wars nie zawahał się ani chwili: - Nie boję się niczego!- zawołał. Wskoczył do swojej łodzi i popłynął. Ledwo jednak odbił od brzegu, rozpętała się straszliwa burza. -Roztrzaskamy ci wiosła!- syczały błyskawice. -Porwę twoje sieci na strzępy! -ryczał wicher. -Zatopimy łódź! -groziły fale. Ale Wars płynął tak szybko, że ani wicher, ani fale, ani błyskawice nie mogły go dogonić. Kiedy był już na środku rzeki, wśród wzburzonych fal ujrzał dziwną postać: pół rybę, pół dziewczynę. Była to syrena. Zdziwił się Wars. Podpłynął bliżej. Wyciągnął rękę. Syrena podała mu tarczę i miecz. I nagle... zamieniła się w piękną dziewczynę. -Na imię mam Sawa -powiedziała. Teraz ty broń mnie, rzeki i miasta. A potem było jak w bajce:
      </p>
      <p>
        Żyli długo i szczęśliwie
        <br /> dzielny Wars i piękna Sawa.
        <br /> Rosło miasto nad Wisłą,
        <br /> dzielna, piękna Warszawa.
        <br /> Fale płyną jak dawniej...
        <br /> Wiatr powtarza piosenkę.
        <br /> -Jaki herb ma Warszawa?
        <br /> -Syrenkę.
      </p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.03" id="sec3_object0">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec3_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.06" id="sec3_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec3_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec3_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec3_object2.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="circles_white">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_white.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white2.png" alt="">
  </div>
</section>


<!-- section 4 -->
<section id="section04" class="section">
  <div class="main-container container relative" id="container4">
    <div class="text-section text-center makeVisible" id="text4">
      <h3 class="text-blue">Co skrywa<br /> w sobie Warszawa?</h3>
      <h4>Warszawa to nie tylko siedziba władz kraju i instytucji państwowych. Warszawa to miasto o niepowtarzalnej historii, w którą warto sie zagłębić.</h4>
      <p>To właśnie w Warszawie znajduje się wiele zabytków. Najbardziej znane to Zamek Królewski, Kolumna króla Zygmunta III Wazy, Pałac Kultury i Nauki, pomnik Warszawskiej Syrenki nad Wisłą oraz Łazienki Królewskie.</p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec4_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec4_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec4_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec4_object2.png" alt="">
    </div>
  </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.2" id="circles_color2">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_color2.png" alt="">
    </div>

  <div class="absolute-pos bottom-pos full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white2.png" alt="">
  </div>
</section>


<!-- section 5 -->
<section id="section05" class="section">
  <div class="main-container container relative" id="container5">
    <div class="row">
      <div class="col-md-6 left text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec5_icon1.png" alt="" class="icon">
        <h3>Zamek Królewski oraz Kolumna Zygmunta III Wazy</h3>
        <p>Zamek wraz z Kolumną Zygmunta jest wizytówką starego miasta. Dawniej była to siedziba polskich królów oraz miejsce obrad sejmu Rzeczypospolitej, obecnie znajduje się tam przepiękne muzeum. Kolumna Zygmunta jest najstarszym publicznym pomnikiem w Warszawie.</p>
      </div>
      <div style class="col-md-6 right text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec5_icon2.png" alt="" class="icon">
        <h3>Łazienki Królewskie</h3>
        <p>Przepiękny zespół pałaców otoczony urokliwymi ogrodami. W samym sercu Łazienek, na wyspie znajduje się letnia rezydencja Króla Augusta Poniatowskiego (ostatni król Polski), która urzeka swoimi pięknymi wnętrzami. Podczas spaceru po parku możemy natknąć się na liczne wiewiórki oraz pawie!</p>
      </div>
    </div>
    <div class="row" style="padding-top:75px;">
      <div class="col-md-6 left text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec5_icon3.png" alt="" class="icon">
        <h3>Pomnik warszawskiej Syrenki</h3>
        <p>Syrenka warszawska to półkobieta półryba. Jest to jeden z najbardziej rozpoznawalnych pomników w Warszawie, jest symbolem miasta. Jej postać widnieje w herbie Warszawy.</p>
      </div>
      <div style class="col-md-6 right text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s7/sec5_icon4.png" alt="" class="icon">
        <h3>Stare Miasto</h3>
        <p>Stare miasto wciąż zachwyca swoimi kamieniczkami oraz urokliwymi wąskimi uliczkami. Warto pospacerować tutaj w ciagu dnia, a także wieczorami, aby poczuć niepowtarzalny klimat tego miejsca. </p>
      </div>
    </div>
  </div>
</section>

<!-- Modals -->
<div class="modal fade" id="pin4Modal" tabindex="-1" role="dialog" aria-labelledby="pin4ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s7/pins/s7_pin_img1.jpg" alt="">
      </div>
      <div class="modal-footer">
        <p>Zdjęcie: Alina Zienowicz <a href="//commons.wikimedia.org/wiki/User:Ala_z" title="User:Ala z">Ala z</a> - <span class="int-own-work" lang="pl">Praca własna</span>, <a href="https://creativecommons.org/licenses/by-sa/3.0" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>,<a href=„https://commons.wikimedia.org/w/index.php?curid=4368991 ">Link</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pin4Modal2" tabindex="-1" role="dialog" aria-labelledby="pin4ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s7/pins/s7_pin_img2.jpg" alt="">
      </div>
      <div class="modal-footer">
        <p>Zdjęcie: Alina Zienowicz <a href="//commons.wikimedia.org/wiki/User:Ala_z" title="User:Ala z">Ala z</a> - <span class="int-own-work" lang="pl">Praca własna</span>, <a href="https://creativecommons.org/licenses/by-sa/3.0" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>,<a href="https://commons.wikimedia.org/w/index.php?curid=4368928">Link</a></p>
        </div>
        </div>
    </div>
</div>


<script type="text/javascript">
  var infoBoxesText = {
    box1: "<p>Warszawa nie była naszą stolicą od zawsze. Naszą pierwszą stolicą było Gniezno. Tę zaszczytną funkcję pełniły również takie miasta jak Poznań, Płock oraz Kraków.</p>",
    box2: `
        <p>
            <ol>
                <li><span>1.</span>Naszą stolicą jest Warszawa, z którą związana jest legenda o Warsie i Sawie.</li>
                <li><span>2.</span>Herb Warszawy przedstawia syrenę, która trzyma miecz oraz tarczę.</li>
                <li><span>3.</span>Przez stolicę przepływa rzeka Wisła.</li>
            </ol>
        </p>`,
    box3: '/template/default/assets/pdf/lessons/s7_color_pdf.pdf'
  }
</script>
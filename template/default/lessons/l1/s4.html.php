<!-- section 1 -->
<section id="section01" class="parallax section section-fixed">
  <div class="fixed-container">
    <!-- pins -->
    <div id="pin1" class="pin image" data-toggle="modal" data-target="#pin4Modal"></div>
    <div id="pin2" class="pin video" data-toggle="modal" data-target="#pin3Modal"></div>

    <div class="frame">
      <ul id="boxParallax" class="parallax-list">
        <li class="layer" data-depth="0">
          <img class="parallax-object layer0" name="tlo" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/parallax/layer0.png">
        </li>
        <li class="layer" data-depth="0.02">
          <img class="parallax-object layer1" name="tlo2" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/parallax/layer1.png">
        </li>
        <li class="layer" data-depth="0.05">
          <img class="parallax-object layer2" name="dzieci" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/parallax/layer2.png">
        </li>
        <li class="layer" data-depth="0">
          <img class="parallax-object layer3" name="mikrofon" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/parallax/layer3.png">
        </li>
        <li class="layer" data-depth="0.15">
          <img class="parallax-object layer4" name="parkiet" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/parallax/layer4.png">
        </li>
        <li class="layer" data-depth="0.20">
          <img class="parallax-object layer5" name="parkiet" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/parallax/layer5.png">
        </li>
        <li class="layer" data-depth="0.35">
          <img class="parallax-object layer6" name="parkiet" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/parallax/layer6.png">
        </li>
        <li class="layer" data-depth="0.45">
          <img class="parallax-object layer7" name="parkiet" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/parallax/layer7.png">
        </li>
      </ul>
    </div>
  </div>

  <div class="absolute-pos bottom-pos full-width">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>
</section>


<!-- section 2 -->
<section id="section02" class="section">
  <div class="main-container container relative" id="container2">

    <div class="text-section makeVisible" id="text2">
      <h2 class="text-blue">Marsz,<br /> marsz Dąbrowski...</h2>
      <h3>Symbolem narodowym jest także hymn.</h3>
      <p>Jest to pieśń wychwalająca bohaterskie czyny i zwycięstwa. Polskim hymnem jest „Mazurek Dąbrowskiego”. Jego autorem jest Józef Wybicki. Pieśń została napisana we Włoszech w miasteczku Reggio, aby dodać siły i odwagi legionom polskim, które tam walczyły.</p>
    </div>

    <div class="absolute-pos hidden-sm hidden-xs animated-element" data-animation-speed="0.5" id="circles_color1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s_elements/circles_color1.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec2_object0">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/sec2_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec2_object1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/sec2_object1.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_red.png" alt="">
  </div>
</section>

<!-- section 3 -->
<section id="section03" class="section">

  <div class="main-container container relative" id="container3">
    <div class="text-section text-center text-white" id="text3">
      <h2>Mazurek Dąbrowskiego</h2>
      <h3>Hymn Polski</h3>

      <p>
        Jeszcze Polska nie zginęła,
        <br /> Kiedy my żyjemy.
        <br /> Co nam obca przemoc wzięła,
        <br /> Szablą odbierzemy.
      </p>
      <p>
        Marsz, marsz Dąbrowski,
        <br /> Z ziemi włoskiej do Polski,
        <br /> Za twoim przewodem
        <br /> Złączym się z narodem.
      </p>
      <p>
        Przejdziem Wisłę, przejdziem Wartę,
        <br /> Będziem Polakami,
        <br /> Dał nam przykład Bonaparte,
        <br /> Jak zwyciężać mamy.
        <br /> Marsz, marsz Dąbrowski...
      </p>
      <p>
        Jak Czarniecki do Poznania
        <br /> Po szwedzkim zaborze,
        <br /> Dla ojczyzny ratowania
        <br /> Wrócim się przez morze.
        <br /> Marsz, marsz Dąbrowski...
      </p>
      <p>
        Już tam ojciec do swej Basi
        <br /> Mówi zapłakany —
        <br /> „Słuchaj jeno, pono nasi,
        <br /> Biją w tarabany”.
        <br /> Marsz, marsz Dąbrowski...
      </p>

    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec3_object0">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/sec3_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.1" id="sec3_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/sec3_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec3_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/sec3_object2.png" alt="">
    </div>


    <div class="absolute-pos animated-element" data-animation-speed="0.2" id="circles_white">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_white.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white2.png" alt="">
  </div>
</section>

<!-- section 4 -->
<section id="section04" class="section">
  <div class="main-container container relative" id="container4">
    <div class="text-section text-center makeVisible" id="text4">
      <h3 class="text-blue">Kiedy możemy usłyszeć<br /> Hymn państwowy?</h3>
      <h4>Kiedy śpiewamy hymn państwowy  zachowujemy powagę i stoimy w pozycji na baczność. </h4>
      <p>Hymn państwowy jest śpiewany podczas uroczystości państwowych i apeli szkolnych. „Mazurek Dąbrowskiego” odśpiewują także sportowcy przed zawodami i podczas ceremonii wręczenia medali.</p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec4_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/sec4_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec4_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s4/sec4_object2.png" alt="">
    </div>
    <div class="absolute-pos animated-element" data-animation-speed="0.4" id="circles_color2">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_color2.png" alt="">
    </div>
  </div>
</section>


<!-- Modals -->
<div class="modal fade" id="pin4Modal" tabindex="-1" role="dialog" aria-labelledby="pin4ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s4/pins/s4_pin_img1.jpg" alt="">
      </div>
      <div class="modal-footer">
        <p>Zdjęcie: Józef Wybicki (1747–1822) - <a rel="nofollow" class="external free" href="http://www.anthem.pl/">http://www.anthem.pl/</a>, Domena publiczna, <a href="https://commons.wikimedia.org/w/index.php?curid=3680272">Link</a>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pin3Modal" tabindex="-1" role="dialog" aria-labelledby="pin3ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/jY3F5wambkk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <div class="modal-footer">
          <p>Film:
            <a rel="nofollow" href="https://www.youtube.com/channel/UC2U4ZYJsWyCMnALTuOHK_lg" target="_blank">Szkoła Podstawowa w Chwiramie (YouTube)</a>
          </p>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    var infoBoxesText = {
      box1: "<p>Często ludzie podczas śpiewu popełniają błąd w pierwszej zwrotce hymnu śpiewając „…póki my żyjemy”, warto pamiętać, że obowiązującą oraz zatwierdzoną formą tekstu jest „…kiedy my żyjemy”.</p>",
      box2: `
        <p>
            <ol>
                <li><span>1.</span>Tytułem naszego hymnu jest „Mazurek Dąbrowskiego”.</li>
                <li><span>2.</span>Autorem naszego pięknego hymnu jest Józef Wybicki.</li>
                <li><span>3.</span>Słowa do naszego hymnu powstały we Włoszech.</li>
            </ol>
        </p>`,
      box3: '/template/default/assets/pdf/lessons/s4_color_pdf.pdf'
    }
  </script>
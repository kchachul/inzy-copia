<!-- section 1 -->
<section id="section01" class="parallax section section-fixed">
    <div class="fixed-container">
        <!-- pins -->
        <div id="pin1" class="pin video" data-toggle="modal" data-target="#pin3Modal"></div>

        <div class="frame">
            <ul id="boxParallax" class="parallax-list">
                <li class="layer" data-depth="0">
                    <img class="parallax-object layer0" name="wall"
                         src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/parallax/layer0.png">
                </li>
                <li class="layer" data-depth="0.03">
                    <img class="parallax-object layer8" name="cabinets"
                         src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/parallax/layer8.png">
                </li>
                <li class="layer" data-depth="0.02">
                    <img class="parallax-object layer1" name="view"
                         src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/parallax/layer1.png">
                </li>
                <li class="layer" data-depth="0.05">
                    <img class="parallax-object layer2" name="watch"
                         src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/parallax/layer2.png">
                </li>
                <li class="layer" data-depth="0">
                    <img class="parallax-object layer3" name="elements"
                         src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/parallax/layer3.png">
                </li>
                <li class="layer" data-depth="0.22">
                    <img class="parallax-object layer4" name="boy"
                         src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/parallax/layer4.png">
                </li>
                <li class="layer" data-depth="0.27">
                    <img class="parallax-object layer5" name="teacher"
                         src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/parallax/layer5.png">
                </li>
                <li class="layer" data-depth="0.50">
                    <img class="parallax-object layer6" name="desk_1"
                         src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/parallax/layer6.png">
                </li>
                <li class="layer" data-depth="0.60">
                    <img class="parallax-object layer7" name="desk_2"
                         src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/parallax/layer7.png">
                </li>
            </ul>
        </div>
    </div>

    <div class="absolute-pos bottom-pos full-width">
        <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
    </div>

</section>


<!-- section 2 -->
<section id="section02" class="section">
    <div class="main-container container relative" id="container2">

        <div class="text-section makeVisible" id="text2">
            <h2 class="text-blue">Czym<br/> jest ojczyzna?</h2>
            <h3>Kraj, w którym żyjemy to Polska. To nasza ojczyna.</h3>
            <p>Jesteś Polakiem. Ojczyzna to kraj, w którym się urodziłeś. Tutaj urodzili się także Twoi rodzice,
                dziadkowie i przodkowie. Wiesz, że każdy z nich uczył się kiedyś pewnego wiersza, którego treść
                odnajdziesz poniżej.</p>
        </div>

        <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec2_object1">
            <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec2_object1.png"
                 alt="">
        </div>

        <div class="absolute-pos hidden-sm hidden-xs" id="sec2_object0">
            <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec2_object0.png"
                 alt="">
        </div>

        <div class="absolute-pos hidden-sm hidden-xs" id="sec2_object2">
            <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec2_object2.png" alt="">
        </div>

        <div class="absolute-pos animated-element" data-animation-speed="0.4" id="circles_color1">
            <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_color1.png"
                 alt="">
        </div>

    </div>

    <div class="absolute-pos bottom-pos index-5 full-width text-center">
        <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_red.png" alt="">
    </div>
</section>


<!-- section 3 -->
<section id="section03" class="section">

    <div class="main-container container relative" id="container3">
        <div class="text-section text-center text-white" id="text3">
            <h2>Kto Ty jesteś?</h2>
            <h3>Bełza Władysław</h3>

            <p>
                - Kto ty jesteś?
                <br/> - Polak mały.
            </p>
            <p>
                - Jaki znak twój?
                <br/> - Orzeł biały.
            </p>
            <p>
                - Gdzie ty mieszkasz?
                <br/> - Między swemi.
            </p>
            <p>
                - W jakim kraju?
                <br/> - W polskiej ziemi.
            </p>
            <p>
                - Czym ta ziemia?
                <br/> - Mą ojczyzną.
            </p>
            <p>
                - Czym zdobyta?
                <br/> - Krwią i blizną.
            </p>
            <p>
                - Czy ją kochasz?
                <br/> - Kocham szczerze.
            </p>
            <p>
                - A w co wierzysz?
                <br/> - W Boga wierzę.
            </p>
            <p>
                - Czym ty dla niej?
                <br/> - Wdzięczne dziecię.
            </p>
            <p>
                - Coś jej winien?
                <br/> - Oddać życie.
            </p>
        </div>

        <div class="absolute-pos hidden-sm hidden-xs" id="sec3_object0">
            <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec3_object0.png"
                 alt="">
        </div>

        <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.1" id="sec3_object1">
            <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec3_object1.png"
                 alt="">
        </div>

        <div class="absolute-pos hidden-sm hidden-xs" id="sec3_object2">
            <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec3_object2.png"
                 alt="">
        </div>

        <div class="absolute-pos hidden-sm hidden-xs" id="sec3_object3">
            <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec3_object3.png"
                 alt="">
        </div>

        <div class="absolute-pos animated-element" data-animation-speed="0.2" id="circles_white">
            <img class="img-absolute full-w"
                 src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_white.png" alt="">
        </div>

    </div>

    <div class="absolute-pos bottom-pos full-width text-center">
        <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white2.png" alt="">
    </div>
</section>


<!-- section 4 -->
<section id="section04" class="section">
    <div class="main-container container relative" id="container4">
        <div class="row">
            <div class="col-md-6 left text-center makeVisible">
                <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec4_ico1.png" alt="" class="icon">
                <h3>Co to znaczy być Polakiem?</h3>
                <p>Bycie Polakiem to nic innego, jak świadomość poczucia przynależności do wspólnoty jaką jest ojczyzna.
                    Każdy Polak powinien czuć się za nią odpowiedzialny oraz każdego dnia troszczyć się o jej dobro.</p>
            </div>
            <div style class="col-md-6 right text-center makeVisible">
                <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec4_ico2.png" alt="" class="icon">
                <h3>Kim jest autor wiersza?</h3>
                <p>O autorze wiersza, czyli Władysławie Bełzie mówiono "poeta, który uczył dzieci najlepiej kochać
                    Polskę". Ten skromny, a zarazem wyjątkowy człowiek, w swoich wierszach budził w najmłodszych
                    Polakach miłość do własnej ojczyzny.</p>
            </div>
        </div>
    </div>
</section>


<!-- section 5 -->
<section id="section05" class="section">
    <div class="main-container container relative" id="container5">
        <div class="text-section text-center makeVisible" id="text5">
            <h3 class="text-blue">Dlaczego powinniśmy<br/> kochać Polskę?</h3>
            <h4>W przeszłości wielu naszych przodków walczyło i umierało za naszą ojczyznę dlatego powinniśmy ją kochać
                i szanować. </h4>
            <p>Pamiętaj zawsze, że jest to ziemia, którą zamieszkiwali nasi przodkowie, a co za tym idzie, to również
                tradycje, legendy, wspólna historia, wspólny język, kultura oraz wszystko to z czego możemy być dumni
                jako Polacy. Szacunek do naszego dziedzictwa kulturowego, do ludzi którzy je tworzyli jest bardzo ważny.
                W swojej codziennej postawie powinniśmy stale pomnazać wszelkie dobra, które stworzyły poprzednie
                pokolenia!
            </p>
        </div>

        <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.1" id="sec5_object1">
            <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec5_object1.png" alt="">
        </div>

        <div class="absolute-pos hidden-sm hidden-xs" id="sec5_object2">
            <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec5_object2.png"
                 alt="">
        </div>

        <div class="absolute-pos animated-element" data-animation-speed="0.2" id="circles_color2">
            <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_color2.png"
                 alt="">
        </div>
    </div>
</section>


<!-- section 6 -->
<section id="section06" class="section">
    <div class="main-container container relative" id="container6">
        <div class="text-section text-center makeVisible" id="text6">
            <h3 class="text-blue">Skąd pochodzi<br/> nazwa naszej ojczyzny?</h3>
            <h4>Nazwa naszej Ojczyzny pochodzi od nazwy plemienia Polan.</h4>
            <p>Polanie zaczerpnęli swoją nazwę od słowa pole. Pole to nic innego jak ziemia uprawiania przez ludzi na
                określonym terenie. Polanie byli potężnym ludem, o którym dowiesz się wiecej na następnych lekcjach.
            </p>
        </div>

        <div class="absolute-pos hidden-sm hidden-xs" id="sec6_object1">
            <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec6_object1.png"
                 alt="">
        </div>

        <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec6_object0">
            <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec6_object0.png" alt="">
        </div>

        <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec6_object2">
            <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s1/sec6_object2.png" alt="">
        </div>

    </div>
</section>


<!-- Modals -->
<div class="modal fade" id="pin3Modal" tabindex="-1" role="dialog" aria-labelledby="pin3ModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe width="560" height="315"
                        src="https://www.youtube.com/embed/xQk8p7XY23A?start=20&end=57&version=3" frameborder="0"
                        allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <p>Film: <a rel="nofollow" href="https://www.youtube.com/user/IPNtvPL" target="_blank">IPN TV
                        (YouTube)</a>
                </p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  var infoBoxesText = {
    box1: '<p>Wiesz, że tradycyjna i pełna nazwa naszego kraju to Rzeczpospolita Polska? Słowo „rzeczpospolita” pochodzi od łacińskich słów „res publica” - czyli rzecz publiczna.</p>',
    box2: `
        <p>
            <ol>
                <li><span>1.</span>Pełna nazwa naszego kraju to Rzeczpospolita Polska, to kraj w którym żyjesz, to Twoja ojczyzna.</li>
                <li><span>2.</span>Nazwa naszej ojczyzny pochodzi od nazwy plemienia Polan.</li>
                <li><span>3.</span>Polanie to potężny lud, który uprawiał pole. To właśnie oni dali początek państwu polskiemu.</li>
            </ol>
        </p>`,
    box3: '/template/default/assets/pdf/lessons/s1_color_pdf.pdf'
  }
</script>
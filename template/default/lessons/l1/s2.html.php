<!-- section 1 -->
<section id="section01" class="parallax section section-fixed">
  <div class="fixed-container">
    <!-- pins -->
    <div id="pin1" class="pin image" data-toggle="modal" data-target="#pin4Modal"></div>
    <div id="pin2" class="pin video" data-toggle="modal" data-target="#pin3Modal"></div>

    <div class="frame">
      <ul id="boxParallax" class="parallax-list">
        <li class="layer" data-depth="0.40">
          <img class="parallax-object layer0" name="background" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/parallax/layer0.png">
        </li>
        <li class="layer" data-depth="0.25">
          <img class="parallax-object layer1" name="trees" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/parallax/layer1.png">
        </li>
        <li class="layer" data-depth="0.20">
          <img class="parallax-object layer2" name="street" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/parallax/layer2.png">
        </li>
        <li class="layer" data-depth="0.20">
          <img class="parallax-object layer3" name="cars" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/parallax/layer3.png">
        </li>
        <li class="layer" data-depth="0.15">
          <img class="parallax-object layer4" name="flag" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/parallax/layer4.png">
        </li>
        <li class="layer" data-depth="0.2">
          <img class="parallax-object layer5" name="boy" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/parallax/layer5.png">
        </li>
        <li class="layer" data-depth="0.0">
          <img class="parallax-object layer6" name="grass" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/parallax/layer6.png">
        </li>
      </ul>
    </div>
  </div>

  <div class="absolute-pos bottom-pos full-width">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>

</section>


<!-- section 2 -->
<section id="section02" class="section">
  <div class="main-container container relative" id="container2">

    <div class="text-section makeVisible" id="text2">
      <h2 class="text-blue">Jaka<br /> jest nasza flaga?</h2>
      <h3>Flaga to jeden z naszych z symboli narodowych.</h3>
      <p>Każde państwo posiada swoje symbole narodowe. Prawdziwy Polak zna je i szanuje. Jednym z nich jest flaga. Polska flaga jest biało - czerwona.</p>
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.4" id="circles_color1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s_elements/circles_color1.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.3" id="sec2_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/sec2_object2.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.1" id="sec2_object0">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/sec2_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="sec2_object1">
      <img class="img-absolute" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/sec2_object1.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_white1.png" alt="">
  </div>

</section>

<!-- section 3 -->
<section id="section03" class="section">
  <div class="main-container container relative" id="container3">
    <div class="row">
      <div class="col-md-6 left text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/sec3_icon1.png" alt="" class="icon">
        <h3>Dlaczego biel?</h3>
        <p>Kolor biały oznacza uczciwość i dobroć. Niech nigdy nie zabraknie tych cech w Twoim sercu.</p>
      </div>
      <div style class="col-md-6 right text-center makeVisible">
        <img src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/sec3_icon2.png" alt="" class="icon">
        <h3>Dlaczego czerwień?</h3>
        <p>Czerwień jest symbolem odwagi oraz krwi, którą nasi przodkowie przelali w obronie naszej ojczyzny.</p>
      </div>
    </div>
  </div>

  <div class="absolute-pos bottom-pos index-5 full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/clouds_red.png" alt="">
  </div>
</section>

<!-- section 4 -->
<section id="section04" class="section">

  <div class="main-container container relative" id="container4">
    <div class="text-section text-center text-white" id="text4">
      <h2>Polska flaga ma swoje święto, </br>które obchodzimy 2 maja!</h2>
      <h3>Warto pamiętać o tym wyjątkowym dniu</h3>

      <p>
        Podczas uroczystości państwowych flagi wywieszane są na ulicach i budynkach. </br>Niech pojawi się także w oknie Twojego domu!
      </p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.03" id="sec4_object0">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/sec4_object0.png" alt="">
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.06" id="sec4_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/sec4_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec4_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/sec4_object2.png" alt="">
    </div>

    <div class="absolute-pos animated-element" data-animation-speed="0.1" id="circles_white">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s_elements/circles_white.png" alt="">
    </div>

  </div>

  <div class="absolute-pos bottom-pos full-width text-center">
    <img class="full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s_elements/clouds_white2.png" alt="">
  </div>
</section>

<!-- section 5 -->
<section id="section05" class="section">
  <div class="main-container container relative" id="container5">
    <div class="text-section text-center makeVisible" id="text5">
      <h3 class="text-blue">Gdzie mogę zobaczyć<br /> naszą flagę?</h3>
      <h4>Biało-czerwone flagi oraz sztandardy towarzyszyły nam od zawsze. </h4>
      <p>Już na pierwszych sztandarach podczas ważnych państwowych bitew widniał biały orzeł na czerwonym tle. Flaga była wszędzie tam, gdzie rozstrzygały się ważne losy naszego kraju. Do dziś podczas ważnych imprez sportowych czy uroczystości państwowych wciąż powiewają te same barwy.</p>
    </div>

    <div class="absolute-pos animated-element hidden-sm hidden-xs" data-animation-speed="0.2" id="sec5_object1">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/sec5_object1.png" alt="">
    </div>

    <div class="absolute-pos hidden-sm hidden-xs" id="sec5_object2">
      <img class="img-absolute full-w" src="<?php echo DIR_TEMPLATE_IMAGES;?>lessons/l1/s2/sec5_object2.png" alt="">
    </div>
  </div>
</section>

<!-- Modals -->
<div class="modal fade" id="pin4Modal" tabindex="-1" role="dialog" aria-labelledby="pin4ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="<?php echo DIR_TEMPLATE_IMAGES; ?>lessons/l1/s2/pins/s2_pin_img1.jpg" alt="">
      </div>
      <div class="modal-footer">
        <p>Zdjęcie: <a href="//commons.wikimedia.org/wiki/User:Aotearoa" title="User:Aotearoa">Aotearoa</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=2278086">Link</a>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pin3Modal" tabindex="-1" role="dialog" aria-labelledby="pin3ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/4V3ME8W-6pU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        <div class="modal-footer">
          <p>Film:
            <a rel="nofollow" href="https://www.youtube.com/channel/UC0-PBNfH_yiYPdsUAyAWT3Q" target="_blank">Pułk Reprezentacyjny Wojska Polskiego (YouTube)</a>
          </p>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    var infoBoxesText = {
      box1: "<p>Czy wiesz, że symbole narodowe są pod szczególną ochroną? Każdy z nich wymaga należytego szacunku i czci. Za ich znieważenie trzeba liczyć się z surową karą!</p>",
      box2: `
        <p>
            <ol>
                <li><span>1.</span>Nasza flaga jest biało-czerwona..</li>
                <li><span>2.</span>Biel na naszej fladze jest odzwierciedleniem dobroci i uczciwości, a czerwień jest wyrażeniem odwagi oraz krwi przelanej za ojczyznę.</li>
                <li><span>3.</span>Święto flagi państwowej obchodzimy 2 maja.</li>
            </ol>
        </p>`,
      box3: '/template/default/assets/pdf/lessons/s2_color_pdf.pdf'
    }
  </script>
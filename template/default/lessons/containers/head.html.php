<!DOCTYPE html>
<html>
<head>
    <title><?php echo $this->pageTitle;?></title>

    <link rel="stylesheet" href="<?php echo HTTP_SERVER.DIR_TEMPLATE.'libs/awesomeicons/css/font-awesome.min.css';?>">
    <link rel="stylesheet" href="<?php echo HTTP_SERVER.DIR_TEMPLATE.'libs/bootstrap/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo HTTP_SERVER.DIR_TEMPLATE.'assets/css/main.css';?>">
    <script type="text/javascript" src="<?php echo HTTP_SERVER.DIR_TEMPLATE.'libs/jquery/jquery-3.2.0.min.js';?>"></script>
    <script type="text/javascript" src="<?php echo HTTP_SERVER.DIR_TEMPLATE.'libs/bootstrap/bootstrap.min.js';?>"></script>


    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="nofollow" />
</head>

<body class="lessonsPage">

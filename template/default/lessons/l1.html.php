<div id="loader"><img src="<?php echo HTTP_SERVER . DIR_TEMPLATE; ?>assets/images/pages/lessons/loader_icon.svg" alt="">
</div>

<div id="site">

    <!-- modal select lesson -->
    <div class="modal fade" id="ModalSelectLesson" tabindex="-1" role="dialog" aria-labelledby="ModalSelectLessonLabel">
        <div class="modal-dialog full-screen" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3>Wybierz swoją lekcję</h3>
                            <h4>Nigdy nie przestawaj w zagłębianiu wiedzy</h4>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12 text-center">

                            <div class="row">

                                <?php foreach ($this->lessons as $lesson): ?>

                                    <?php foreach ($lesson['scenes'] as $scene): ?>

                                        <div class="col-md-4">
                                            <a href="#scene<?php echo $scene['nr']; ?>"
                                               data-scene="<?php echo $scene['nr']; ?>">
                                                <div class="box">
                                                    <div class="image"
                                                         style="background-image: url('<?php echo HTTP_SERVER . DIR_TEMPLATE . 'assets/images/pages/lessons/' . $scene['thumb']; ?>')"></div>
                                                    <div class="title"><?php echo $scene['title']; ?></div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php endforeach; ?>

                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="scene_container" id="null"></div>

    <!-- footer -->
    <footer id="footer">
        <!-- scena -->
        <section id="scene-info">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 box left text-center">
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <i class="fa fa-question text-blue" aria-hidden="true"></i>
                                    <b>Odkrywaj</b><br>ciekawostkę
                                </div>
                                <div class="back">
                                    <div class="image">
                                        <img src="<?php echo HTTP_SERVER . DIR_TEMPLATE; ?>assets/images/pages/lessons/box1_img.png" alt="">
                                    </div>
                                    <div class="title">
                                        Czy wiesz, że...?
                                    </div>
                                    <div class="text"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 box middle text-center">
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <i class="fa fa-graduation-cap text-red" aria-hidden="true"></i>
                                    <b>Co musisz</b><br>wiedzieć?
                                </div>
                                <div class="back">
                                    <div class="image">
                                        <img src="<?php echo HTTP_SERVER . DIR_TEMPLATE; ?>assets/images/pages/lessons/box2_img.png" alt="">
                                    </div>
                                    <div class="title">
                                        Warto<br> zapamiętać:
                                    </div>
                                    <div class="text"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 box right text-center">
                        <div class="flip-container">
                            <div class="flipper w_url">
                                <div class="front">
                                    <a href="#" class="url" target="_blank">
                                        <i class="fa fa-pencil text-white" aria-hidden="true"></i>
                                        <b>pobierz</b><br>kolorowankę
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

            <div class="container-fluid" id="copyright">
                <div class="row">
                    <div class="col-md-5">mojaojczyzna.pl - wszelkie prawa zastrzeżone</div>
                    <div class="col-md-7 text-right">Projekt wykonany w ramach obrony pracy inżynierskiej: Jakub Chachuła, Marcin
                        Tillak
                    </div>
                </div>
        </div>
    </footer>
</div>

<audio id="player" volume="0.3" autoplay loop>
    <source src="" type="audio/mp3">
</audio>